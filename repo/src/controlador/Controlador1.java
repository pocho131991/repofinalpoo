package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
//import java.util.Date;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import modelo.BaseDeDatos;
import modelo.DAOMulta;
import modelo.DAOPeliculasImpl;
import modelo.DAOSocio;
import modelo.DAOUsuario;
import modelo.Exception_VideoClub;
import modelo.Multa;
import modelo.Pelicula;
import modelo.Socio;
import modelo.Usuario;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import tabla.Tabla;
import tabla.TablaModelo;
import vista.Vista1;

public class Controlador1 implements ActionListener, MouseListener, WindowListener, KeyListener {
	private Vista1 vista;
	private Tabla t;
	private Socio socioSeleccionado;
	private BaseDeDatos bd;

/// CONSTRUCTOR

	public Controlador1() {

		this.setVista(new Vista1(this));
		this.getVista().setVisible(true);
		this.setT(new Tabla());

	}

///METODOS	
	@Override
	public void actionPerformed(ActionEvent arg0) {

		JButton btn = (JButton) arg0.getSource();
		DAOPeliculasImpl daop = new DAOPeliculasImpl();
		DAOSocio daos = new DAOSocio();

		if (btn.getText().equals("Agregar Socio")) {

			new ControladorAltaSocio();
		}
		if (btn.getText().equals("Modificar Socio")) {

			Integer rta = JOptionPane.showConfirmDialog(null, "Desea eliminar esta pelicula", "Mensaje",
					JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				daos.modificarPersona(inicializarSocio());
			}
			this.getT().visualizar_TablaSocio(this.getVista().getTable_socio());
		}
		if (btn.getText().equals("Eliminar Socio")) {
			Socio s = new Socio();
			s.setNro_socio(Integer.valueOf(this.getVista().getTxtNumSoc().getText()));
			Integer rta = JOptionPane.showConfirmDialog(null, "Desea eliminar al socio nro " + s.getNro_socio(),
					"Mensaje", JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				daos.eliminarPersona(s);
			}
			this.getT().visualizar_TablaSocio(this.getVista().getTable_socio());
		} else if (btn.getText().equals("Socio")) {
			this.getVista().cambiarPanelSocio();
			this.getVista().getBtnClientes().setEnabled(false);
			this.getVista().getBtnPeliculas().setEnabled(true);
		} else if (btn.getText().equals("Usuarios")) {
			this.getVista().cambiarPanelUsuario();
		} else if (btn.getText().equals("Modificar Usuario")) {

			Integer rta = JOptionPane.showConfirmDialog(null, "Desea modificar al usuario", "Mensaje",
					JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				modificarUsuario();
			}
			this.getT().visualizar_TablaUsuario(this.getVista().getTablaUsuario());

		}

		else if (btn.getText().equals("Eliminar Usuario")) {

			Integer rta = JOptionPane.showConfirmDialog(null, "Desea eliminar al usuario?", "Mensaje",
					JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				DAOUsuario daou = new DAOUsuario();
				daou.eliminarUsuario(Integer.parseInt(this.getVista().getTxtIDUsuario().getText()));
			}
			this.getT().visualizar_TablaUsuario(this.getVista().getTablaUsuario());
		}

		else if (btn.getText().equals("Pelicula")) {
			this.getVista().cambiarPanelPelicula();
			this.getVista().getBtnPeliculas().setEnabled(false);
			this.getVista().getBtnClientes().setEnabled(true);

		} else if (btn.getText().equals("Alquilar")) {
			this.getVista().getBtnPeliculas().setEnabled(true);
			this.getVista().getBtnClientes().setEnabled(true);
			this.getVista().cambiarPanelAlquiler();

		} else if (btn.getText().equals("Busqueda")) {
			new ControladorVistaSeleccionPelicula(this);
			this.getVista().getBtnMostrar().setEnabled(true);

		} else if (btn.getText().equals("Mostrar")) {
			this.getVista().getTxtsocio().setText(this.getSocioSeleccionado().getNro_socio().toString());
			this.getVista().getTxtNomApeSocioAlquiler()
					.setText(this.getSocioSeleccionado().getApellido() + " " + this.getSocioSeleccionado().getNombre());
			this.getVista().getTxtDireccionSocioAlquiler().setText(this.getSocioSeleccionado().getDireccion());
			this.getVista().getTxtTelefonoSocioAlquiler().setText(this.getSocioSeleccionado().getTelefono().toString());
			this.getVista().getBtnMostrar().setEnabled(false);

		} else if (btn.getText().equals("Multas/Sanciones")) {
			this.getVista().cambiarPanelMulta();
		} else if (btn.getText().equals("Modificar Multa")) {
			modificarMulta();
			this.getT().visualizar_TablaMultas(this.getVista().getTablaMultas());
		} else if (btn.getText().equals("Agregar Multa")) {
			new ControladorAltaMulta();
			this.getT().visualizar_TablaMultas(this.getVista().getTablaMultas());
		} else if (btn.getText().equals("Agregar pelicula")) {
			new ControladorAltaPelicula();

		} else if (btn.getText().equals("Actualizar lista")) {
			this.getT().visualizar_TablaPeliculas(this.getVista().getTable());
		} else if (btn.getText().equals("Eliminar pelicula")) {

			Integer indice = this.getVista().getTable().getSelectedRow();
			Pelicula pelicula = new Pelicula();
			pelicula.setCod_pelicula(Integer.valueOf(this.getVista().getTable().getValueAt(indice, 0).toString()));
			Integer rta = JOptionPane.showConfirmDialog(null, "Desea eliminar esta pelicula", "Mensaje",
					JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				daop.eliminarPelicula(pelicula);

			}
			this.getT().visualizar_TablaPeliculas(this.getVista().getTable());
		} else if (btn.getText().equals("Modificar pelicula")) {
			ArrayList<String> lista = new ArrayList<String>();
			Integer indice = this.getVista().getTable().getSelectedRow();
			lista.add(this.getVista().getTable().getValueAt(indice, 1).toString());// obtener titulo
			lista.add(this.getVista().getTable().getValueAt(indice, 2).toString());// obtener titulo latino
			lista.add(this.getVista().getTable().getValueAt(indice, 3).toString());// obtener duracion
			lista.add(this.getVista().getTable().getValueAt(indice, 4).toString());// obtener a�o
			lista.add(this.getVista().getTable().getValueAt(indice, 5).toString());// obtener director
			lista.add(
					daop.obtenerActores(Integer.valueOf(this.getVista().getTable().getValueAt(indice, 0).toString())));// obtener
																														// actores
			lista.add(this.getVista().getTable().getValueAt(indice, 0).toString());// obtener codigo pelicula
			lista.add(this.getVista().getTable().getValueAt(indice, 6).toString());// obtener genero
			lista.add(
					daop.obtenerSinopsis(Integer.valueOf(this.getVista().getTable().getValueAt(indice, 0).toString())));// obtener
																														// sinopsis
			lista.add(this.getVista().getTable().getValueAt(indice, 8).toString());// obtener cantidad
			lista.add(this.getVista().getTable().getValueAt(indice, 9).toString());// obtener precio
			ControladorModElimPelicula cmep = new ControladorModElimPelicula(this);
			cmep.getVmep().completarCampos(lista);
			cmep.getVmep().setVisible(true);
			this.getT().visualizar_TablaPeliculas(this.getVista().getTable());
		} else if (btn.getText().equals("Sinopsis de pelicula")) {
			Integer indice = this.getVista().getTable().getSelectedRow();
			Integer codigo = Integer.parseInt(this.getVista().getTable().getValueAt(indice, 0).toString());
			this.getVista().getTxtASinopsis().setText(daop.obtenerSinopsis(codigo));

		} else if (btn.getText().equals("QUITAR PELICULA")) {
			/// Obtener el modelo de la tabla y castearlo a DefaultTableModel, eliminar la
			/// fila seleccionada, y luego setear el modelo en la tabla

			DefaultTableModel dtm = (DefaultTableModel) this.getVista().getTablaSeleccion().getModel();
			Integer fila = this.getVista().getTablaSeleccion().getSelectedRow();
			Double valor_1 = daop.obtenerPrecio(
					Integer.parseInt(this.getVista().getTablaSeleccion().getValueAt(fila, 0).toString()));
			Double valor = Double.parseDouble(this.getVista().getTxtPagar().getText()) - valor_1;
			dtm.removeRow(fila);
			this.getVista().getTablaSeleccion().setModel(dtm);
			Integer cantidad = Integer.parseInt(this.getVista().getTxtCantipeliculas().getText()) - 1;
			this.getVista().getTxtCantipeliculas().setText(cantidad.toString());

			this.getVista().getTxtPagar().setText(valor.toString());

		} else if (btn.getText().equals("VER LISTADO ALQUILER")) {
			try {
				Connection cn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/BD_TPF_POO", "postgres","astonbirra");
				String path = ("src\\jasper\\listado_alquiler.jasper");
				JasperReport jr = null;
				try {
					String text = getVista().getTextFechaDesde().getText();
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate textFieldAsDate = LocalDate.parse(text, formatter);

					Map parametro = new HashMap();
					Date fecha = Date.valueOf(textFieldAsDate);
					parametro.put("desde_fecha_alquiler", fecha);
					jr = (JasperReport) JRLoader.loadObjectFromFile(path);
					JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
					//JasperViewer jv = new JasperViewer(jp,false);
					//jv.setVisible(true);
					//jv.setTitle(path);
					JasperViewer.viewReport(jp,false);
					
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				}
				cn.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			
		} else if (btn.getText().equals("ALQUILAR")) {
			System.out.println(":(");

			try {
				Connection cn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/BD_TPF_POO", "postgres",
						"astonbirra");

				String path = ("src\\jasper\\Factura_3.jasper");

				JasperReport jr = null;

				try {

					Map parametro = new HashMap();
					parametro.put("numero_socio", getVista().getTxtsocio());
					parametro.put("fecha_creacion_alquiler", getVista().getTxtFecIniAlquiler());

					jr = (JasperReport) JRLoader.loadObjectFromFile(path);
					JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
					JasperViewer jv = new JasperViewer(jp);
					jv.setVisible(true);
					jv.setTitle(path);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				cn.close();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private Socio inicializarSocio() {
		Socio s = new Socio();

		s.setNro_socio(Integer.valueOf(this.getVista().getTxtNumSoc().getText()));
		try {
			s.setDni_socio(Integer.parseInt(this.getVista().getTxtDniSoc().getText()));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception_VideoClub e) {
			e.printStackTrace();
		}

		try {
			s.setApellido(this.getVista().getTxt_apellido_socio().getText());
		} catch (Exception_VideoClub e) {
			e.printStackTrace();
		}
		try {
			s.setNombre(this.getVista().getTxt_nombre_socio().getText());
		} catch (Exception_VideoClub e) {
			e.printStackTrace();
		}
		try {
			s.setDireccion(this.getVista().getTxt_direccion().getText());
		} catch (Exception_VideoClub e) {

			e.printStackTrace();
		}
		try {
			s.setTelefono(Integer.parseInt(this.getVista().getTxt_tel().getText()));
		} catch (NumberFormatException e) {

			e.printStackTrace();
		} catch (Exception_VideoClub e) {

			e.printStackTrace();
		}

		try {

			String[] fecha = this.getVista().getTxt_fecnac().getText().split("-");
			s.setFecha_nac(
					LocalDate.of(Integer.parseInt(fecha[0]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[2])));
		} catch (DateTimeException e) {
			e.printStackTrace();
		}

		return s;
	}

	private void modificarMulta() {
		DAOMulta daom = new DAOMulta();
		Multa m = new Multa();
		m.setCod_multa(Integer.parseInt(this.getVista().getTxtCod_multa().getText()));
		m.setNro_socio(Integer.parseInt(this.getVista().getTxtNroSocioMulta().getText()));
		m.setTipo_multa(Integer.parseInt(this.getVista().getTxtTipo_multa().getText()));
		String[] fecha = this.getVista().getTxtFecha_multa().getText().split("-");
		m.setFecha_multa(
				LocalDate.of(Integer.parseInt(fecha[0]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[2])));

		if (this.getVista().getCmbPagado().getSelectedItem().toString().equals("Pagado")) {// Pagado
			m.setPagado(true);
		} else {
			m.setPagado(false);
		}

		daom.modificarMulta(m);

	}

	private void modificarUsuario() {
		Usuario u = new Usuario();
		DAOUsuario daou = new DAOUsuario();
		boolean correcto = true;

		u.setId_usuario(Integer.valueOf(this.getVista().getTxtIDUsuario().getText()));

		try {
			u.setNombre(this.getVista().getTxtNomUsuario().getText());
		} catch (Exception_VideoClub e) {
			correcto = false;
			this.getVista().getLblExcepcionUsuarioNombre().setText("Nombre Usuario -" + e.getMessage() + "\n");
		}
		try {
			u.setApellido(this.getVista().getTxtApeUsuario().getText());
		} catch (Exception_VideoClub e) {
			correcto = false;

			this.getVista().getLblExcepcionUsuarioApellido().setText("Apellido Usuario -" + e.getMessage() + "\n");

		}
		try {
			u.setEmail(this.getVista().getTxtEmailUsuario().getText());
		} catch (Exception_VideoClub e) {
			correcto = false;
			this.getVista().getLblExcepcionUsuarioEmail().setText("Email Usuario -" + e.getMessage() + "\n");

		}

		try {
			String[] fecha = this.getVista().getTxtFecNacUsuario().getText().split("-");
			u.setFec_nac(
					LocalDate.of(Integer.parseInt(fecha[0]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[2])));

		} catch (DateTimeException e2) {
			correcto = false;
			this.getVista().getLblExcepcionUsuarioFecNac()
					.setText("Fecha de nacimiento Usuario - Error en fecha" + "\n");
		} catch (NumberFormatException nfe) {
			correcto = false;
			this.getVista().getLblExcepcionUsuarioFecNac()
					.setText("Fecha de nacimiento - Error en fecha - Formato incorrecto" + "\n");
		}

		try {

			u.setContrasena(String.valueOf(this.getVista().getPassfieldContra().getPassword()));

		} catch (Exception_VideoClub e) {
			correcto = false;
			this.getVista().getLblExcepcionUsuarioContrasenia().setText("Contrase�a Usuario " + e.getMessage() + "\n");

		}

		if (correcto) {
			JOptionPane.showMessageDialog(null, "Se ingres� usuario correctamente");
			daou.modificarUsuario(u);
		} else {
			JOptionPane.showMessageDialog(null, "Se ingres� usuario incorrectamente");

		}

	}

	@Override
	public void mouseClicked(java.awt.event.MouseEvent e) {
		Integer fila = 0;

		if (e.getSource() == this.getVista().getTablaMultas()) {
			if (e.getClickCount() == 2) {
				fila = this.getVista().getTablaMultas().rowAtPoint(e.getPoint());
				this.getVista().getTxtCod_multa()
						.setText(this.getVista().getTablaMultas().getValueAt(fila, 0).toString());
				this.getVista().getTxtNroSocioMulta()
						.setText(this.getVista().getTablaMultas().getValueAt(fila, 1).toString());
				this.getVista().getTxtTipo_multa()
						.setText(this.getVista().getTablaMultas().getValueAt(fila, 4).toString());
				this.getVista().getTxtFecha_multa()
						.setText(this.getVista().getTablaMultas().getValueAt(fila, 6).toString());
				this.getVista().getTxtCosto().setText(this.getVista().getTablaMultas().getValueAt(fila, 8).toString());
				this.getVista().getCmbPagado().setEnabled(true);
			}
		}
		if (e.getSource() == this.getVista().getTablaUsuario()) {
			if (e.getClickCount() == 1) {
				fila = this.getVista().getTablaUsuario().rowAtPoint(e.getPoint());
				this.getVista().getTxtIDUsuario()
						.setText(this.getVista().getTablaUsuario().getValueAt(fila, 0).toString());
				this.getVista().getTxtApeUsuario()
						.setText(this.getVista().getTablaUsuario().getValueAt(fila, 2).toString());
				this.getVista().getTxtNomUsuario()
						.setText(this.getVista().getTablaUsuario().getValueAt(fila, 1).toString());
				this.getVista().getTxtEmailUsuario()
						.setText(this.getVista().getTablaUsuario().getValueAt(fila, 3).toString());
				this.getVista().getTxtFecNacUsuario()
						.setText(this.getVista().getTablaUsuario().getValueAt(fila, 5).toString());
				this.getVista().getPassfieldContra()
						.setText(this.getVista().getTablaUsuario().getValueAt(fila, 4).toString());
			}
		}

		if (e.getSource() == this.getVista().getTablaSocioAlquiler()) {
			if (e.getClickCount() == 1) {
				this.getVista().getBtnQuitarPeli().setEnabled(true);
				fila = this.getVista().getTablaSocioAlquiler().rowAtPoint(e.getPoint());
				this.getVista().getTxtsocio()
						.setText(this.getVista().getTablaSocioAlquiler().getValueAt(fila, 0).toString());
				this.getVista().getTxtNomApeSocioAlquiler()
						.setText(this.getVista().getTablaSocioAlquiler().getValueAt(fila, 2).toString() + " "
								+ this.getVista().getTablaSocioAlquiler().getValueAt(fila, 3).toString());
				this.getVista().getTxtDireccionSocioAlquiler()
						.setText(this.getVista().getTablaSocioAlquiler().getValueAt(fila, 4).toString());
				this.getVista().getTxtTelefonoSocioAlquiler()
						.setText(this.getVista().getTablaSocioAlquiler().getValueAt(fila, 5).toString());
			} else {
				this.getVista().getBtnQuitarPeli().setEnabled(true);
			}

		}

		if (e.getSource() == this.getVista().getTablaPeli()) {

			// PASAR A LA TABLA DE PELICULAS QUE VA ALQUILAR EL CLIENTE
			// falta terminar
			if (e.getClickCount() == 2) {
				DAOPeliculasImpl daop = new DAOPeliculasImpl();
				TablaModelo tabm = (TablaModelo) this.getVista().getTablaSeleccion().getModel();
				fila = this.getVista().getTablaPeli().rowAtPoint(e.getPoint());
				Object fila_tabla_Seleccion[] = new Object[5];
				fila_tabla_Seleccion[0] = this.getVista().getTablaPeli().getValueAt(fila, 0);
				fila_tabla_Seleccion[1] = this.getVista().getTablaPeli().getValueAt(fila, 1);
				fila_tabla_Seleccion[2] = this.getVista().getTablaPeli().getValueAt(fila, 2);
				fila_tabla_Seleccion[3] = this.getVista().getTablaPeli().getValueAt(fila, 4);
				fila_tabla_Seleccion[4] = 1;

				tabm.addRow(fila_tabla_Seleccion);
				this.getVista().getTablaSeleccion().setModel(tabm);
				this.getVista().getTablaSeleccion().getColumnModel().getColumn(0).setPreferredWidth(80);
				this.getVista().getTablaSeleccion().getColumnModel().getColumn(1).setPreferredWidth(80);
				this.getVista().getTablaSeleccion().getColumnModel().getColumn(2).setPreferredWidth(80);
				this.getVista().getTablaSeleccion().getColumnModel().getColumn(3).setPreferredWidth(80);
				this.getVista().getTablaSeleccion().getColumnModel().getColumn(4).setPreferredWidth(80);
				Integer cantidad = Integer.parseInt(this.getVista().getTxtCantipeliculas().getText()) + 1;
				Double total = Double.parseDouble(this.getVista().getTxtPagar().getText()) + daop
						.obtenerPrecio(Integer.valueOf(this.getVista().getTablaPeli().getValueAt(fila, 0).toString()));

				this.getVista().getTxtPagar().setText(total.toString());
				this.getVista().getTxtCantipeliculas().setText(cantidad.toString());

			}
		}

		if (e.getSource() == this.getVista().getTable()) {
			if (e.getClickCount() == 1) {
				this.getVista().getBtnModificarPelicula().setEnabled(true);
				this.getVista().getBtnEliminarPelicula().setEnabled(true);
			}
			DAOPeliculasImpl daop = new DAOPeliculasImpl();
			Integer indice = this.getVista().getTable().getSelectedRow();

			try {
				byte[] bi = daop
						.obtenerImagen(Integer.valueOf(this.getVista().getTable().getValueAt(indice, 0).toString()));
				InputStream in = new ByteArrayInputStream(bi);
				BufferedImage image = ImageIO.read(in);
				ImageIcon imgi = new ImageIcon(image.getScaledInstance(236, 275, 0));
				this.getVista().getLblPortada().setIcon(imgi);
			} catch (IOException e1) {

				e1.printStackTrace();
			} catch (NullPointerException e2) {
				this.getVista().getLblPortada();
			}
			Integer codigo = Integer.parseInt(this.getVista().getTable().getValueAt(indice, 0).toString());
			this.getVista().getTxtASinopsis().setText(daop.obtenerSinopsis(codigo));
			this.getVista().getTxtAActores().setText(daop.obtenerActores(codigo));
		}
		if (e.getSource() == this.getVista().getTable_socio()) {
			Integer rta = JOptionPane.showConfirmDialog(null, "Desea modificar/eliminar al socio", "Mensaje",
					JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				Integer fila_s = this.getVista().getTable_socio().rowAtPoint(e.getPoint());
				this.getVista().getTxtNumSoc()
						.setText("" + this.getVista().getTable_socio().getValueAt(fila_s, 0).toString());
				this.getVista().getTxtDniSoc()
						.setText("" + this.getVista().getTable_socio().getValueAt(fila_s, 1).toString());
				this.getVista().getTxt_apellido_socio()
						.setText("" + this.getVista().getTable_socio().getValueAt(fila_s, 2).toString());
				this.getVista().getTxt_nombre_socio()
						.setText("" + this.getVista().getTable_socio().getValueAt(fila_s, 3).toString());
				this.getVista().getTxt_direccion()
						.setText("" + this.getVista().getTable_socio().getValueAt(fila_s, 4).toString());
				this.getVista().getTxt_tel()
						.setText("" + this.getVista().getTable_socio().getValueAt(fila_s, 5).toString());
				this.getVista().getTxt_fecnac()
						.setText("" + this.getVista().getTable_socio().getValueAt(fila_s, 6).toString());
			}
		}

	}

	@Override
	public void mouseEntered(java.awt.event.MouseEvent e) {

	}

	@Override
	public void mouseExited(java.awt.event.MouseEvent e) {

	}

	@Override
	public void mousePressed(java.awt.event.MouseEvent e) {
	}

	@Override
	public void mouseReleased(java.awt.event.MouseEvent e) {

	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		this.getT().visualizar_TablaPeliculas(this.getVista().getTable());

	}

	@Override
	public void windowClosed(WindowEvent arg0) {

	}

	@Override
	public void windowClosing(WindowEvent arg0) {

	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		this.getT().visualizar_TablaPeliculas(this.getVista().getTable());
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		this.getT().visualizar_TablaPeliculas(this.getVista().getTable());
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		this.getT().visualizar_TablaPeliculas(this.getVista().getTable());
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		this.getT().visualizar_TablaPeliculas(this.getVista().getTable());

	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// nuevo sin terminar aun

		if (e.getSource() == this.getVista().getTxtFiltroAlquilerPelicula()) {
			TablaModelo tabm = (TablaModelo) this.getVista().getTablaPeli().getModel();
			TableRowSorter<DefaultTableModel> trs = new TableRowSorter<DefaultTableModel>(tabm);
			List<RowSorter.SortKey> sortKeys = new ArrayList<>();
			sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
			trs.setSortKeys(sortKeys);
			trs.setRowFilter(
					RowFilter.regexFilter("(?i)" + this.getVista().getTxtFiltroAlquilerPelicula().getText(), 1));
			trs.setRowFilter(
					RowFilter.regexFilter("(?i)" + this.getVista().getTxtFiltroAlquilerPelicula().getText(), 2));
			this.getVista().getTablaPeli().setRowSorter(trs);
			this.getT().visualizar_TablaPeliculaReducida(this.getVista().getTablaPeli());
		}
		if (e.getSource() == this.getVista().getTxtFiltroPelicula()) {
			TablaModelo tabm = (TablaModelo) this.getVista().getTable().getModel();
			TableRowSorter<DefaultTableModel> trs = new TableRowSorter<DefaultTableModel>(tabm);
			List<RowSorter.SortKey> sortKeys = new ArrayList<>();
			sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
			trs.setSortKeys(sortKeys);
			trs.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVista().getTxtFiltroPelicula().getText(), 1));
			trs.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVista().getTxtFiltroPelicula().getText(), 2));
			this.getVista().getTable().setRowSorter(trs);
			this.getT().visualizar_TablaPeliculas(this.getVista().getTable());
		}
		if (e.getSource() == this.getVista().getTxtFiltro()) {
			System.out.println("hola");
			TableRowSorter<DefaultTableModel> trs_socio = new TableRowSorter<DefaultTableModel>(
					this.getT().getDtm_socio());
			/*
			 * List<RowSorter.SortKey> sortKeys = new ArrayList<>(); sortKeys.add(new
			 * RowSorter.SortKey(0, SortOrder.ASCENDING)); trs_socio.setSortKeys(sortKeys);
			 */
			trs_socio.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVista().getTxtFiltro().getText(), 0));
			trs_socio.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVista().getTxtFiltro().getText(), 1));
			trs_socio.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVista().getTxtFiltro().getText(), 2));
			this.getVista().getTable_socio().setRowSorter(trs_socio);
			this.getT().visualizar_TablaSocio(this.getVista().getTable_socio());
		}

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}
//	GETTERS Y SETTERS

	public Vista1 getVista() {
		return vista;
	}

	public void setVista(Vista1 vista) {
		this.vista = vista;
	}

	public Tabla getT() {
		return t;
	}

	public void setT(Tabla t) {
		this.t = t;
	}

	public Socio getSocioSeleccionado() {
		return socioSeleccionado;
	}

	public void setSocioSeleccionado(Socio socioSeleccionado) {
		this.socioSeleccionado = socioSeleccionado;
	}

	public BaseDeDatos getBd() {
		return bd;
	}

	public void setBd(BaseDeDatos bd) {
		this.bd = bd;
	}

}
