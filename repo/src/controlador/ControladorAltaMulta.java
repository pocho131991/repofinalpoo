package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.JOptionPane;

import modelo.DAOMulta;
import modelo.Multa;
import modelo.Socio;
import vista.VistaAltaMulta;

public class ControladorAltaMulta implements ActionListener {
	private VistaAltaMulta vam;
	private Socio s = new Socio();

	public ControladorAltaMulta() {
		this.setVam(new VistaAltaMulta(this));
		this.getVam().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getVam().getBtnAgregarMulta()) {
			boolean correcto = true;
			Multa m = new Multa();
			DAOMulta daom = new DAOMulta();
			m.setNro_socio(Integer.valueOf(this.getVam().getTxtNroSocio().getText()));
			m.setPagado(false);

			if (this.getVam().getCmbTipo().getSelectedItem().toString().equals("TIPO 1")) {
				m.setTipo_multa(1);
			} else if (this.getVam().getCmbTipo().getSelectedItem().toString().equals("TIPO 2")) {
				m.setTipo_multa(2);
			} else {
				m.setTipo_multa(3);
			}

			try {
				String[] fecha = this.getVam().getFtxtFecha().getText().split("/");
				m.setFecha_multa(LocalDate.of(Integer.parseInt(fecha[0]), Integer.parseInt(fecha[1]),
						Integer.parseInt(fecha[2])));
			} catch (Exception e2) {
				correcto = false;

			}

			if (correcto) {
				daom.agregarMulta(m);
				JOptionPane.showMessageDialog(null, "Se ingres� Multa correctamente");
			} else {
				JOptionPane.showMessageDialog(null, "Se ingres� Multa incorrectamente por error en fecha");
			}

		} else {
			new ControladorVistaSeleccionSocio(this);
			this.getVam().getBtnBusqueda().setEnabled(false);
		}

	}

	public VistaAltaMulta getVam() {
		return vam;
	}

	public void setVam(VistaAltaMulta vam) {
		this.vam = vam;
	}

	public Socio getS() {
		return s;
	}

	public void setS(Socio s) {
		this.s = s;
	}

}
