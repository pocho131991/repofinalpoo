package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import modelo.DAOPeliculasImpl;
import modelo.Exception_VideoClub;
import modelo.Pelicula;
import vista.VistaAltaPelicula;

public class ControladorAltaPelicula implements ActionListener {

	private VistaAltaPelicula vap;
	

	public ControladorAltaPelicula() {
		this.setVap(new VistaAltaPelicula(this));
		this.getVap().setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();

		if (btn.getText().equals("GUARDAR")) {
			registrarPelicula();

		} else if (btn.getText().equals("VOLVER")) {
			this.getVap().dispose();

		} else if (btn.getText().equals("IMAGEN")) {
			JFileChooser j = new JFileChooser();
			FileNameExtensionFilter fil = new FileNameExtensionFilter("JPG, PNG & GIF", "jpg", "png", "gif");
			j.setFileFilter(fil);

			int s = j.showOpenDialog(null);
			if (s == JFileChooser.APPROVE_OPTION) {
				String ruta_img = j.getSelectedFile().getAbsolutePath();
				this.getVap().getTxt_ruta().setText(ruta_img);

			}

		} else {
			limpiarCampos();
		}
	}

	private void limpiarCampos() {
		this.getVap().getTxttitulo().setText("");
		this.getVap().getTxttitulolat().setText("");
		this.getVap().getTxtanio().setText("");
		this.getVap().getTxtduracion().setText("");
		this.getVap().getTxtdirector().setText("");
		this.getVap().getTxtAActoresP().setText("");
	}

		
	private void registrarPelicula() {
		DAOPeliculasImpl daop = new DAOPeliculasImpl();
		Boolean peliculacorrecta = true;
		Pelicula pelicula = new Pelicula();
		// Tabla t = new Tabla();
		pelicula.setCod_pelicula(daop.obtenerCodigosPeli() + 1);
		try {
			pelicula.setTitulo(this.getVap().getTxttitulo().getText());
		} catch (Exception_VideoClub e1) {
			peliculacorrecta = false;
			this.getVap().getLblExcepcionTitulo().setText(e1.getMessage());
		}
		try {
			pelicula.setTitulo_alterno(this.getVap().getTxttitulolat().getText());
		} catch (Exception_VideoClub e1) {
			peliculacorrecta = false;
			this.getVap().getLblExcepcionTituloA().setText(e1.getMessage());
		}

		try {
			pelicula.setDuracion(Integer.valueOf(this.getVap().getTxtduracion().getText()));
		} catch (NumberFormatException e1) {
			this.getVap().getLblExcepcionDuracion().setText("Formato incorrecto");
			peliculacorrecta = false;
		}

		try {
			pelicula.setAnio(Integer.valueOf(this.getVap().getTxtanio().getText()));
		} catch (NumberFormatException e1) {
			this.getVap().getLblExcepcionAnio().setText("Formato incorrecto");
			peliculacorrecta = false;
		}
		try {
			pelicula.setDirector(this.getVap().getTxtdirector().getText());
		} catch (Exception_VideoClub e1) {
			this.getVap().getLblExcepcionDirector().setText(e1.getMessage());
			peliculacorrecta = false;
		}
		try {
			pelicula.setActores_principales(this.getVap().getTxtAActoresP().getText());
		} catch (Exception_VideoClub e1) {
			this.getVap().getLblExcepcionActores().setText(e1.getMessage());
			peliculacorrecta = false;
		}

		try {
			pelicula.setSinopsis(this.getVap().getTxtSinopsis().getText());
		} catch (Exception_VideoClub e3) {
			this.getVap().getLblExcepcionSinopsis().setText(e3.getMessage());
			peliculacorrecta = false;
		}
		try {
			pelicula.setGenero(this.getVap().getTxtgenero().getText());
		} catch (Exception_VideoClub e2) {
			this.getVap().getLblExcepcionGenero().setText(e2.getMessage());
			peliculacorrecta = false;

		}
		try {
			pelicula.setFormato(this.getVap().getCmbFormato().getSelectedItem().toString());
		} catch (Exception_VideoClub e1) {
			this.getVap().getLblExcepcionFormato().setText(e1.getMessage());
			peliculacorrecta = false;
		}
		File ruta = new File(this.getVap().getTxt_ruta().getText());
		try {
			byte[] icono = new byte[(int) ruta.length()];
			InputStream input = new FileInputStream(ruta);
			input.read(icono);
			pelicula.setFoto(icono);
		} catch (IOException e) {
			// e.printStackTrace();
			pelicula.setFoto(null);
		}
		try {
			pelicula.setCantidad(Integer.valueOf(this.getVap().getTxtCantidad().getText()));
		} catch (NumberFormatException e1) {
			this.getVap().getLblExcepcionCantidad().setText("Formato incorrecto");
			peliculacorrecta = false;
		}
		try {
			pelicula.setPrecio(Double.valueOf(this.getVap().getTxtPrecio().getText()));
		} catch (NumberFormatException e1) {
			this.getVap().getLblExcepcionPrecio().setText("Formato incorrecto");
			peliculacorrecta = false;
		}
		
		if (peliculacorrecta) {
			daop.agregarPelicula(pelicula);
			
			JOptionPane.showMessageDialog(null, "La Pelicula " + pelicula.getTitulo() + " se ingreso correctamente ");
			this.getVap().dispose();
		} else {
			JOptionPane.showMessageDialog(null, "Se ingreso incorrectamente uno o mas campos");
		}

	}

	public VistaAltaPelicula getVap() {
		return vap;
	}

	public void setVap(VistaAltaPelicula vap) {
		this.vap = vap;
	}


}
