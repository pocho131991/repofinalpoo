package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.DateTimeException;
import java.time.LocalDate;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import modelo.DAOSocio;
import modelo.Exception_VideoClub;
import modelo.Socio;
import vista.VistaAltaSocio;

public class ControladorAltaSocio implements ActionListener {

	private VistaAltaSocio vas;

	public ControladorAltaSocio() {
		this.setVas(new VistaAltaSocio(this));
		this.getVas().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();

		if (btn.getText().equals("GUARDAR")) {
			registrarSocio();
		}
		if (btn.getText().equals("VOLVER")) {
			this.getVas().dispose();
		}
		if (btn.getText().equals("LIMPIAR CAMPOS")) {
			limpiarCampos();
		}
	}

	private void registrarSocio() {
		Boolean socioCorrecto = true;
		DAOSocio daoS = new DAOSocio();
		Socio socio = new Socio();

		try {
			socio.setNombre(this.getVas().getTxt_nombre().getText());
		} catch (Exception_VideoClub e1) {
			socioCorrecto = false;
			this.getVas().getLblIngreseNombreDel().setForeground(Color.red);
			this.getVas().getLblIngreseNombreDel().setText(e1.getMessage());
		}
		try {
			socio.setApellido(this.getVas().getTxt_apellido().getText());
		} catch (Exception_VideoClub e1) {
			socioCorrecto = false;
			this.getVas().getLblIngreseApellidosDel().setForeground(Color.red);
			this.getVas().getLblIngreseApellidosDel().setText(e1.getMessage());
		}
		try {
			socio.setDni_socio(Integer.valueOf(this.getVas().getTxt_dni().getText()));
		} catch (Exception_VideoClub eSo) {
			socioCorrecto = false;
			this.getVas().getLbl_dni().setForeground(Color.red);
			this.getVas().getLbl_dni().setText(eSo.getMessage());
		} catch (NumberFormatException e1) {
			socioCorrecto = false;
			this.getVas().getLbl_dni().setForeground(Color.red);
			this.getVas().getLbl_dni().setText("Formato incorrecto. Ingrese nuevamente el DNI");
		}
		try {
			socio.setTelefono(Integer.valueOf(this.getVas().getTxt_num_tel().getText()));
		} catch (NumberFormatException e1) {
			socioCorrecto = false;
			this.getVas().getLblIngreseNumeroTelefonico().setForeground(Color.red);
			this.getVas().getLblIngreseNumeroTelefonico()
					.setText("Formato incorrecto. Ingrese nuevamente el n�mero telefonico");
		} catch (Exception_VideoClub e2) {
			socioCorrecto = false;
			this.getVas().getLblIngreseNumeroTelefonico().setForeground(Color.red);
			this.getVas().getLblIngreseNumeroTelefonico().setText(e2.getMessage());
		}

		try {
			socio.setFecha_nac(LocalDate.of(Integer.parseInt(this.getVas().getCmb_anio().getSelectedItem().toString()),
					chooseMonth(this.getVas().getCbm_mes().getSelectedItem().toString()),
					Integer.parseInt(this.getVas().getCmb_dia().getSelectedItem().toString())));

		} catch (DateTimeException e2) {
			socioCorrecto = false;
			this.getVas().getLblIngresoDeFecha().setVisible(true);
		}

		try {
			socio.setDireccion(this.getVas().getTxt_calle().getText());
		} catch (Exception_VideoClub e2) {
			socioCorrecto = false;

			this.getVas().getLblCalle().setForeground(Color.red);
			this.getVas().getLblCalle().setText(e2.getMessage());
		}

		socio.setNro_socio(daoS.obtenerUltimoNroSocio()+1);
		if (socioCorrecto) {
			JOptionPane.showMessageDialog(null, "Se ingres� los datos del socio correctamente");
			daoS.agregarPersona(socio);
			this.getVas().dispose();

		} else {
			JOptionPane.showMessageDialog(null, "Se ingres� uno o mas datos incorrectos del socio incorrectamente");

		}

	}

	private void limpiarCampos() {
		this.getVas().getTxt_nombre().setText("");
		this.getVas().getTxt_apellido().setText("");
		this.getVas().getTxt_dni().setText("");
		this.getVas().getTxt_num_tel().setText("");
		this.getVas().getTxt_calle().setText("");

	}

	private Integer chooseMonth(String st) {

		Integer month = 0;
		switch (st) {
		case "Ene":
			month = 1;
			break;
		case "Feb":
			month = 2;
			break;
		case "Mar":
			month = 3;
			break;
		case "Abr":
			month = 4;
			break;
		case "May":
			month = 5;
			break;
		case "Jun":
			month = 6;
			break;
		case "Jul":
			month = 7;
			break;
		case "Ago":
			month = 8;
			break;
		case "Sep":
			month = 9;
			break;
		case "Oct":
			month = 10;
			break;
		case "Nov":
			month = 11;
			break;
		case "Dic":
			month = 12;
			break;
		}

		return month;
	}

	public VistaAltaSocio getVas() {
		return vas;
	}

	public void setVas(VistaAltaSocio vas) {
		this.vas = vas;
	}

}
