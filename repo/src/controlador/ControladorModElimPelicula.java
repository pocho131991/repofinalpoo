package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import modelo.DAOPeliculasImpl;
import modelo.Exception_VideoClub;
import modelo.Pelicula;
import tabla.Tabla;
import vista.VistaModElimPelicula;

public class ControladorModElimPelicula implements ActionListener {

	private VistaModElimPelicula vmep;
	private Controlador1 c;
	public ControladorModElimPelicula() {
		this.setVmep(new VistaModElimPelicula(this));
		
	}
	public ControladorModElimPelicula(Controlador1 c) {
		this.setVmep(new VistaModElimPelicula(this));
		this.setC(c);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JButton btn = (JButton) arg0.getSource();
		if (btn.getText().equals("VOLVER")) {
			this.getVmep().dispose();
		} else if (btn.getText().equals("LIMPIAR")) {
			this.getVmep().getTxttitulolat().setText("");
			this.getVmep().getTxttitulo().setText("");
			this.getVmep().getTxtanio().setText("");
			this.getVmep().getTxtduracion().setText("");
			this.getVmep().getTxtdirector().setText("");
			this.getVmep().getTxtAActoresP().setText("");
		} else if (btn.getText().equals("IMAGEN")) {

			JFileChooser j = new JFileChooser();
			FileNameExtensionFilter fil = new FileNameExtensionFilter("JPG, PNG & GIF", "jpg", "png", "gif");
			j.setFileFilter(fil);

			int s = j.showOpenDialog(null);
			if (s == JFileChooser.APPROVE_OPTION) {
				String ruta_img = j.getSelectedFile().getAbsolutePath();
				this.getVmep().getTxt_ruta().setText(ruta_img);
			}

		} else {
			Integer cod = Integer.valueOf(this.getVmep().getTxtCodPeli().getText());
			String titulo = this.getVmep().getTxttitulo().getText();
			String titulo_a = this.getVmep().getTxttitulolat().getText();
			Integer duracion = Integer.valueOf(this.getVmep().getTxtduracion().getText());
			Integer anio = Integer.valueOf(this.getVmep().getTxtanio().getText());
			String director = this.getVmep().getTxtdirector().getText();
			String genero = this.getVmep().getTxtgenero().getText();
			String actores = this.getVmep().getTxtAActoresP().getText();
			String sinopsis = this.getVmep().getTxtSinopsis().getText();
			File ruta = null;
			Integer cantidad = Integer.parseInt(this.getVmep().getTxtCantidad().getText());
			Double precio = Double.valueOf(this.getVmep().getTxtPrecio().getText());
			if (this.getVmep().getTxt_ruta() != null) {
				ruta = new File(this.getVmep().getTxt_ruta().getText());
			} else {
				ruta = null;
			}

			String formato = this.getVmep().getCmbFormato().getSelectedItem().toString();

			if (ruta != null) {
				modificarPelicula(cod, titulo, titulo_a, duracion, anio, genero, actores, sinopsis, ruta, formato,director,cantidad,precio);
			} else {

			}

			this.getVmep().dispose();
		}

		/*
		 * if (arg0.getSource() == this.getVmep().getBtnVolver()) {
		 * 
		 * } else if (arg0.getSource() == this.getVmep().getBtnLimpiar()) { // PONER EN
		 * BLANCO LOS JTEXTFIELD�S
		 * 
		 * } else if(arg0.getSource() == this.getVmep().getBtnImaPeli()) {
		 * 
		 * JFileChooser j = new JFileChooser(); FileNameExtensionFilter fil = new
		 * FileNameExtensionFilter("JPG, PNG & GIF","jpg","png","gif");
		 * j.setFileFilter(fil);
		 * 
		 * int s = j.showOpenDialog(null); if(s == JFileChooser.APPROVE_OPTION){ String
		 * ruta_img = j.getSelectedFile().getAbsolutePath();
		 * this.getVmep().getTxt_ruta().setText(ruta_img);
		 * 
		 * }
		 * 
		 * 
		 * } else {
		 * 
		 * }
		 */

	}

	private void modificarPelicula(Integer cod, String titulo, String titulo_a, Integer duracion, Integer anio,
			String genero, String actores, String sinopsis, File ruta, String formato,String director, Integer cantidad, Double precio) {
		Tabla t = new Tabla();
		Pelicula p = new Pelicula();
		DAOPeliculasImpl daop = new DAOPeliculasImpl();
		Boolean correcto = true;
		p.setCod_pelicula(cod);
		try {
			p.setTitulo(titulo);
		} catch (Exception_VideoClub e) {
			correcto = false;
			e.printStackTrace();
		}
		try {
			p.setTitulo_alterno(titulo_a);
		} catch (Exception_VideoClub e) {

			e.printStackTrace();
		}

		try {
			p.setDuracion(duracion);
		} catch (NumberFormatException e) {
			correcto = false;
		}

		try {
			p.setAnio(anio);
		} catch (NumberFormatException e) {
			correcto = false;
		}

		try {
			p.setDirector(director);
		} catch (Exception_VideoClub e) {
			correcto = false;
			e.printStackTrace();
		}
		try {
			p.setActores_principales(actores);
		} catch (Exception_VideoClub e) {
			correcto = false;
			e.printStackTrace();
		}
		try {
			p.setGenero(genero);
		} catch (Exception_VideoClub e3) {
			correcto = false;
			e3.printStackTrace();
		}
		try {
			p.setSinopsis(sinopsis);
		} catch (Exception_VideoClub e2) {
			correcto = false;
			e2.printStackTrace();
		}
		try {
			p.setFormato(formato);
		} catch (Exception_VideoClub e1) {
			correcto = false;
			e1.printStackTrace();
		}

		try {
			byte[] icono = new byte[(int) ruta.length()];
			InputStream input = new FileInputStream(ruta);
			input.read(icono);
			p.setFoto(icono);
		} catch (IOException e) {
			p.setFoto(null);
		}
		
		try {
			p.setCantidad(cantidad);
		} catch (NumberFormatException e) {
			this.getVmep().getLblExcepcionCantidad().setText("Formato incorrecto");
		}
		try {
			p.setPrecio(precio);
		} catch (NumberFormatException e) {
			this.getVmep().getLblPrecio().setText("Formato incorrecto");
		}
		if (correcto) {
			daop.modificarPelicula(p);
			//t.visualizar_TablaPeliculas(this.getC().getVista().getTable());	
			JOptionPane.showMessageDialog(null, "La Pelicula " + p.getTitulo() + " se ingreso modifico correctamente ");
		}else{
			JOptionPane.showMessageDialog(null, "Se ingreso dato/s incorrectos para modificar la pelicula ");
		}
		

	}

	public VistaModElimPelicula getVmep() {
		return vmep;
	}

	public void setVmep(VistaModElimPelicula vmep) {
		this.vmep = vmep;
		vmep.getScrollPane().setLocation(545, 230);
	}

	public Controlador1 getC() {
		return c;
	}

	public void setC(Controlador1 c) {
		this.c = c;
	}

}
