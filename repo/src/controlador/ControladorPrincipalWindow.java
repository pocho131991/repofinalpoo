package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import modelo.DAOUsuario;
import modelo.Exception_VideoClub;
import modelo.Usuario;
import vista.PrincipalWindow;

public class ControladorPrincipalWindow implements ActionListener {

	private PrincipalWindow principalwindow;
	private DAOUsuario daos = new DAOUsuario();
	private Usuario usuario = new Usuario();
	private final String pass = "123";// "123sysVideoClub";// contrase�a

	public ControladorPrincipalWindow() {
		this.setPrincipalwindow(new PrincipalWindow(this));
		this.getPrincipalwindow().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getPrincipalwindow().getBtnRegistrarse()) {
			this.getPrincipalwindow().getBtnVolver().setVisible(true);
			this.getPrincipalwindow().getPanelInicioUsuario().setVisible(false);
			this.getPrincipalwindow().getPanelRegistro().setVisible(true);
			this.getPrincipalwindow().getBtnRegistrarse().setEnabled(false);
			this.getPrincipalwindow().getBtnVolver().setEnabled(true);
			this.getPrincipalwindow().getBtnIniciarSesion().setEnabled(false);
		}
		if (e.getSource() == this.getPrincipalwindow().getBtnIniciarSesion()) {
			this.getPrincipalwindow().getPanelInicioUsuario().setVisible(true);
			this.getPrincipalwindow().getPanelRegistro().setVisible(false);
			this.getPrincipalwindow().getBtnVolver().setEnabled(false);

			limpiarCampos(1);

			Boolean usuarioingresado = confirmarUsuario(this.getPrincipalwindow().getTxtIngresoUsuario().getText(),
					String.valueOf(this.getPrincipalwindow().getPassIngresoUsuario().getPassword()));
			if (usuarioingresado == true) {
				JOptionPane.showMessageDialog(null, "Se ingreso correctamente. Bienvenido "+ this.usuario.getNombre()+"!!!");
				new ControladorVistaMenu();
				
				this.getPrincipalwindow().dispose();
			} else {
				JOptionPane.showMessageDialog(null, "Se ingresaron datos incorrecto, intente nuevamente");
			}

		}
		if (e.getSource() == this.getPrincipalwindow().getBtnVolver()) {
			this.getPrincipalwindow().getBtnVolver().setVisible(false);
			this.getPrincipalwindow().getPanelInicioUsuario().setVisible(true);
			this.getPrincipalwindow().getPanelRegistro().setVisible(false);
			this.getPrincipalwindow().getBtnRegistrarse().setEnabled(true);
			this.getPrincipalwindow().getBtnIniciarSesion().setEnabled(true);
			this.getPrincipalwindow().getBtnVolver().setEnabled(false);
			limpiarCampos(0);
			limpiarCampos(1);

		}
		if (e.getSource() == this.getPrincipalwindow().getBtnOk()) {
			if (this.getPrincipalwindow().getTxtvalidarpass().getText().equals(pass)) {
				JOptionPane.showMessageDialog(null, "Se ingreso correctamente. Bienvenido");
				this.getPrincipalwindow().getBtnConfirmar().setEnabled(true);
				this.getPrincipalwindow().getBtnOk().setEnabled(false);
				this.getPrincipalwindow().getTxtvalidarpass().setText("");
				this.getPrincipalwindow().getTxtvalidarpass().setEnabled(false);
				this.getPrincipalwindow().getTxtNombreU().setEnabled(true);
				this.getPrincipalwindow().getTxtApellidoU().setEnabled(true);
				this.getPrincipalwindow().getTxtEmailU().setEnabled(true);
				this.getPrincipalwindow().getPswContra().setEnabled(true);
				this.getPrincipalwindow().getPswContraConf().setEnabled(true);
				this.getPrincipalwindow().getCmbDia().setEnabled(true);
				this.getPrincipalwindow().getCmbMes().setEnabled(true);
				this.getPrincipalwindow().getCmbAnio().setEnabled(true);

			} else {
				JOptionPane.showMessageDialog(null, "Se ingres� contrase�a incorrecta, intente nuevamente");
			}
		}
		if (e.getSource() == this.getPrincipalwindow().getBtnConfirmar()) {
			registrarUsuario();

		}

	}

	private void limpiarCampos(Integer valor) {
		switch (valor) {
		case 0:// setea los campos de login
			this.getPrincipalwindow().getTxtIngresoUsuario().setText("");
			this.getPrincipalwindow().getPassIngresoUsuario().setText("");
			break;
		case 1:// setea los campos de registrarse
			this.getPrincipalwindow().getTxtNombreU().setText("");
			this.getPrincipalwindow().getTxtApellidoU().setText("");
			this.getPrincipalwindow().getTxtEmailU().setText("");
			this.getPrincipalwindow().getPswContra().setText("");
			this.getPrincipalwindow().getPswContraConf().setText("");
			break;

		default:
			break;
		}
	}

	private void registrarUsuario() {
		Usuario usuario = new Usuario();
		boolean correcto = true;

		usuario.setId_usuario(this.getDaos().obtenerUltimoIdUs() + 1);

		try {
			usuario.setNombre(this.getPrincipalwindow().getTxtNombreU().getText());
		} catch (Exception_VideoClub e) {
			correcto = false;
			this.getPrincipalwindow().getLblregistername().setText(e.getMessage());

		}
		try {
			usuario.setApellido(this.getPrincipalwindow().getTxtApellidoU().getText());
		} catch (Exception_VideoClub e) {
			correcto = false;
			this.getPrincipalwindow().getLblregistersurnema().setText(e.getMessage());

		}
		try {
			usuario.setEmail(this.getPrincipalwindow().getTxtEmailU().getText());
		} catch (Exception_VideoClub e) {
			correcto = false;
			this.getPrincipalwindow().getLblregisteremail().setText(e.getMessage());
		}

		try {
			usuario.setFec_nac(
					LocalDate.of(Integer.parseInt(this.getPrincipalwindow().getCmbAnio().getSelectedItem().toString()),
							chooseMonth(this.getPrincipalwindow().getCmbMes().getSelectedItem().toString()),
							Integer.parseInt(this.getPrincipalwindow().getCmbDia().getSelectedItem().toString())));
		} catch (DateTimeException e2) {
			correcto = false;

			this.getPrincipalwindow().getLblregisterdate().setText("Ingreso una fecha invalida");
		} catch (NumberFormatException nfe) {

			this.getPrincipalwindow().getLblregisterdate().setText("Ingrese una fecha de nacimiento");
		}

		try {

			usuario.setContrasena(String.valueOf(this.getPrincipalwindow().getPswContra().getPassword()));

		} catch (Exception_VideoClub e) {
			correcto = false;
			this.getPrincipalwindow().getLblregisterpass().setText(e.getMessage());
		}

		if (this.getPrincipalwindow().getPswContraConf().getPassword() == null
				|| String.valueOf(this.getPrincipalwindow().getPswContraConf().getPassword()).equals("")) {
			this.getPrincipalwindow().getLblregisterpassconf().setText("Ingrese contrase�a para confirmar");
			correcto = false;
		}

		if (!String.valueOf(this.getPrincipalwindow().getPswContra().getPassword()).equals("")
				&& !String.valueOf(this.getPrincipalwindow().getPswContraConf().getPassword()).equals("")) {
			if (String.valueOf(this.getPrincipalwindow().getPswContra().getPassword())
					.equals(String.valueOf(this.getPrincipalwindow().getPswContraConf().getPassword()))) {
				// System.out.println("hay coincidencia");
				this.getPrincipalwindow().getLblregisterpassconf().setText("Hay coincidencia de contrase�as");
			} else {
				// System.out.println("no hay coincidencia");
				this.getPrincipalwindow().getLblregisterpassconf().setText("No hay coincidencia de contrase�as");
				correcto = false;
			}
		} else {
			correcto = false;
		}

		if (correcto) {
			JOptionPane.showMessageDialog(null, "Se ingres� usuario correctamente");
			this.getDaos().agregarPersona(usuario);
			this.getPrincipalwindow().getBtnConfirmar().setEnabled(false);
		} else {
			JOptionPane.showMessageDialog(null, "Se ingres� usuario incorrectamente");

		}

	}

	private boolean confirmarUsuario(String email, String pass) {
		Boolean flag = false;
		ArrayList<Usuario> coleccion = this.getDaos().obtenerTodosLosUsuariosArray();
		for (Usuario usuario : coleccion) {
			
			if (email.contentEquals(usuario.getEmail()) && pass.equals(usuario.getContrasena())) {
				try {
					this.usuario.setNombre(usuario.getNombre());
				} catch (Exception_VideoClub e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				flag = true;
			}
		}

		return flag;
	}

	private Integer chooseMonth(String st) {

		Integer month = 0;
		switch (st) {
		case "Enero":
			month = 1;
			break;
		case "Febrero":
			month = 2;
			break;
		case "Marzo":
			month = 3;
			break;
		case "Abril":
			month = 4;
			break;
		case "Mayo":
			month = 5;
			break;
		case "Junio":
			month = 6;
			break;
		case "Julio":
			month = 7;
			break;
		case "Agosto":
			month = 8;
			break;
		case "Septiembre":
			month = 9;
			break;
		case "Octubre":
			month = 10;
			break;
		case "Noviembre":
			month = 11;
			break;
		case "Diciembre":
			month = 12;
			break;
		}

		return month;
	}

	public PrincipalWindow getPrincipalwindow() {
		return principalwindow;
	}

	public void setPrincipalwindow(PrincipalWindow principalwindow) {
		this.principalwindow = principalwindow;
	}

	public DAOUsuario getDaos() {
		return daos;
	}

	public void setDaos(DAOUsuario daos) {
		this.daos = daos;
	}

}
