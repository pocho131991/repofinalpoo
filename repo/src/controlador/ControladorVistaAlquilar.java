package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.table.DefaultTableModel;

import modelo.Alquiler;
import modelo.DAOAlquiler;

import modelo.DAOPeliculasImpl;
import modelo.Multa;
import modelo.Socio;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import tabla.TablaModelo;
import vista.VistaAlquilar;

public class ControladorVistaAlquilar extends KeyAdapter
		implements ActionListener, MouseListener, InternalFrameListener {

	private VistaAlquilar va;
	private Socio socio = new Socio();
	private Multa multa;
	private Alquiler alquiler = new Alquiler();
	private LinkedList<Integer> codigosPelicula = new LinkedList<Integer>();
	private ControladorVistaMenu cvm;

	public ControladorVistaAlquilar(ControladorVistaMenu cvm) {
		this.setVa(new VistaAlquilar(this));
		this.setCvm(cvm);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getVa().getBtnQuitar()) {
			/// Obtener el modelo de la tabla y castearlo a DefaultTableModel, eliminar la
			/// fila seleccionada, y luego setear el modelo en la tabla
			DAOPeliculasImpl daop = new DAOPeliculasImpl();

			DefaultTableModel dtm = (DefaultTableModel) this.getVa().getTablaSeleccion().getModel();
			Integer fila = this.getVa().getTablaSeleccion().getSelectedRow();
			Double valor_1 = daop
					.obtenerPrecio(Integer.parseInt(this.getVa().getTablaSeleccion().getValueAt(fila, 0).toString()));
			Double valor = Double.parseDouble(this.getVa().getTxtPagar().getText()) - valor_1;
			dtm.removeRow(fila);
			this.getVa().getTablaSeleccion().setModel(dtm);
			Integer cantidad = Integer.parseInt(this.getVa().getTxtCantipeliculas().getText()) - 1;
			this.getVa().getTxtCantipeliculas().setText(cantidad.toString());
			this.getVa().getTxtPagar().setText(valor.toString());
			/// elimina el codigo de pelicula que se quita con el boton quitar
			this.getCodigosPelicula()
					.remove(Integer.parseInt(this.getVa().getTablaSeleccion().getValueAt(fila, 0).toString()));
		} else if (e.getSource() == this.getVa().getBtnAlquilar()) {
			crearAlquiler();
			
			//Llama al reporte
			try {
				Connection cn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/BD_TPF_POO", "postgres","astonbirra");
				String path = ("src\\jasper\\Factura_3.jasper");
				JasperReport jr = null;
				try {
					Map parametro = new HashMap();
					parametro.put("numero_socio", this.getVa().getTxtsocio());
					parametro.put("fecha_creacion_alquiler", this.getVa().getTxtFecIniAlquiler());
					jr = (JasperReport) JRLoader.loadObjectFromFile(path);
					JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
					JasperViewer.viewReport(jp,false);
				} catch (JRException f) {
					// TODO Auto-generated catch block
					f.printStackTrace();
				}
				cn.close();
			} catch (SQLException g) {
				// TODO Auto-generated catch block
				g.printStackTrace();
			}
			//Limpia la tabla, pero no borra los datos de la vieja tabla, junto con los codigos
			DefaultTableModel dtm = (DefaultTableModel) this.getVa().getTablaSeleccion().getModel();
			dtm.setRowCount(0);
			this.getVa().getTablaSeleccion().setModel(dtm);
		} else if (e.getSource() == this.getVa().getBtnBusqueda()) {
			new ControladorVistaSeleccionSocio(this);
		}

	}

	private void crearAlquiler() {
		DAOAlquiler daoa = new DAOAlquiler();
		DAOPeliculasImpl daop = new DAOPeliculasImpl();
		boolean correcto = true;
		getAlquiler().setNro_socio(Integer.valueOf(this.getVa().getTxtsocio().getText()));// nro socio
		String[] fechaInicioAlquiler = this.getVa().getTxtFecIniAlquiler().getText().split("/");
		getAlquiler().setFecha_alquiler(LocalDate.of(Integer.parseInt(fechaInicioAlquiler[2]),
				Integer.parseInt(fechaInicioAlquiler[1]), Integer.parseInt(fechaInicioAlquiler[0])));// fecha inicio de
																										// alquiler
		Date date = this.getVa().getDateChooser().getDate();
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		try {
			String fechaFA = f.format(date);
			String[] fechaFinAlquiler = fechaFA.split("/");
			getAlquiler().setFecha_entrega(LocalDate.of(Integer.parseInt(fechaFinAlquiler[2]),
					Integer.parseInt(fechaFinAlquiler[1]), Integer.parseInt(fechaFinAlquiler[0])));// fecha fin de
																									// alquiler
		} catch (NullPointerException e) {
			correcto = false;
			JOptionPane.showMessageDialog(null, "Error. No se ingreso la fecha de devolucion", "ERROR", JOptionPane.ERROR_MESSAGE);
			this.getVa().getDateChooser().setBorder(new LineBorder(new Color(255, 0, 0), 2));
		}

		if (correcto) {
			for (int i = 0; i < this.getCodigosPelicula().size(); i++) {

				getAlquiler().setCod_pelicula(this.getCodigosPelicula().get(i));// codigo pelicula
				daop.cantidadPeliculamenos(this.getCodigosPelicula().get(i));//restaria uno de la cantidad de stock de peliculas
				daoa.agregarAlquiler(getAlquiler());

				System.out.println(this.getCodigosPelicula().get(i));
			}

		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Integer fila = 0;
		if (e.getSource() == this.getVa().getTablaPelicula()) {
			if (e.getClickCount() == 2) {
				this.getVa().getBtnQuitar().setEnabled(true);
				DAOPeliculasImpl daop = new DAOPeliculasImpl();
				TablaModelo tabm = (TablaModelo) this.getVa().getTablaSeleccion().getModel();
				fila = this.getVa().getTablaPelicula().rowAtPoint(e.getPoint());
				Object fila_tabla_Seleccion[] = new Object[5];
				fila_tabla_Seleccion[0] = this.getVa().getTablaPelicula().getValueAt(fila, 0);// codigo pelicula
				fila_tabla_Seleccion[1] = this.getVa().getTablaPelicula().getValueAt(fila, 1);// titulo
				fila_tabla_Seleccion[2] = this.getVa().getTablaPelicula().getValueAt(fila, 2);// titulo latino
				fila_tabla_Seleccion[3] = this.getVa().getTablaPelicula().getValueAt(fila, 4);// genero
				fila_tabla_Seleccion[4] = 1;// cantidad

				tabm.addRow(fila_tabla_Seleccion);
				this.getVa().getTablaSeleccion().setModel(tabm);
				this.getVa().getTablaSeleccion().getColumnModel().getColumn(0).setPreferredWidth(80);
				this.getVa().getTablaSeleccion().getColumnModel().getColumn(1).setPreferredWidth(80);
				this.getVa().getTablaSeleccion().getColumnModel().getColumn(2).setPreferredWidth(80);
				this.getVa().getTablaSeleccion().getColumnModel().getColumn(3).setPreferredWidth(80);
				this.getVa().getTablaSeleccion().getColumnModel().getColumn(4).setPreferredWidth(80);
				Integer cantidad = Integer.parseInt(this.getVa().getTxtCantipeliculas().getText()) + 1;
				Double total = Double.parseDouble(this.getVa().getTxtPagar().getText()) + daop
						.obtenerPrecio(Integer.valueOf(this.getVa().getTablaPelicula().getValueAt(fila, 0).toString()));

				this.getVa().getTxtPagar().setText(total.toString());
				this.getVa().getTxtCantipeliculas().setText(cantidad.toString());

				////////////////////// Obtener codigo pelicula
				getCodigosPelicula()
						.add(Integer.parseInt(this.getVa().getTablaPelicula().getValueAt(fila, 0).toString()));
			}
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public VistaAlquilar getVa() {
		return va;
	}

	public void setVa(VistaAlquilar va) {
		this.va = va;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	public Multa getMulta() {
		return multa;
	}

	public void setMulta(Multa multa) {
		this.multa = multa;
	}

	public Alquiler getAlquiler() {
		return alquiler;
	}

	public void setAlquiler(Alquiler alquiler) {
		this.alquiler = alquiler;
	}

	public LinkedList<Integer> getCodigosPelicula() {
		return codigosPelicula;
	}

	public void setCodigosPelicula(LinkedList<Integer> codigosPelicula) {
		this.codigosPelicula = codigosPelicula;
	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		// TODO Auto-generated method stub
		this.getCvm().habilitarBotones(true);
	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {
		// TODO Auto-generated method stub
		Integer rta = JOptionPane.showConfirmDialog(null, "Desea salir ?", "Mensaje", JOptionPane.OK_CANCEL_OPTION);
		if (rta == 0) {
			this.getVa().dispose();
		}

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	public ControladorVistaMenu getCvm() {
		return cvm;
	}

	public void setCvm(ControladorVistaMenu cvm) {
		this.cvm = cvm;
	}

}
