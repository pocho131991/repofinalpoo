package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import modelo.DAOPeliculasImpl;
import vista.VistaDevolucion;

public class ControladorVistaDevolucion implements ActionListener, InternalFrameListener {

	private ControladorVistaMenu cvm;
	private VistaDevolucion vd;

	public ControladorVistaDevolucion(ControladorVistaMenu cvm) {
		this.setVd(new VistaDevolucion(this));
		this.setCvm(cvm);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==this.getVd().getBtnAlquileres()) {
			new ControladorVistaSeleccionAlquileres(this);
			this.getVd().getBtnRealizarDevolucion().setVisible(true);
		}else {
			if (!this.getVd().getTxtCodPelicula().getText().equals("")) {
				DAOPeliculasImpl daop = new DAOPeliculasImpl();
				daop.cantidadPeliculamas(Integer.valueOf(this.getVd().getTxtCodPelicula().getText()));//Incrementa la cantidad de pelicula (devuelve la pelicula)
				JOptionPane.showMessageDialog(null, "Se realizo correctamente la devolucion de la pelicula");
			}else {
				JOptionPane.showMessageDialog(null, "Error al devolver pelicula. Primero ingrese un alquiler");
			}
			
		}

	}

	public ControladorVistaMenu getCvm() {
		return cvm;
	}

	public void setCvm(ControladorVistaMenu cvm) {
		this.cvm = cvm;
	}

	public VistaDevolucion getVd() {
		return vd;
	}

	public void setVd(VistaDevolucion vd) {
		this.vd = vd;
	}

	@Override
	public void internalFrameActivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent arg0) {
		this.getCvm().habilitarBotones(true);

	}

	@Override
	public void internalFrameClosing(InternalFrameEvent arg0) {
		Integer rta = JOptionPane.showConfirmDialog(null, "Desea salir ?", "Mensaje",
				JOptionPane.OK_CANCEL_OPTION);
		if (rta == 0) {
			this.getVd().dispose();
		}

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameOpened(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

}
