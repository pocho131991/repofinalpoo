package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import vista.VistaMenu;

public class ControladorVistaMenu implements ActionListener {
	private VistaMenu vm;

	public ControladorVistaMenu() {
		this.setVm(new VistaMenu(this));
		this.getVm().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getVm().getBtnMenu()) {
			Integer posicion = this.getVm().getBtnMenu().getX();
			if (posicion > 5) {
				Animacion.Animacion.mover_izquierda(211, 5, 2, 2, this.getVm().getBtnMenu());// 211
				Animacion.Animacion.mover_izquierda(5, -200, 2, 2, this.getVm().getPanelMenu());
			} else {
				Animacion.Animacion.mover_derecha(5, 240, 2, 2, this.getVm().getBtnMenu());
				Animacion.Animacion.mover_derecha(-200, 5, 2, 2, this.getVm().getPanelMenu());
			}
		} else if (e.getSource() == this.getVm().getBtnPelicula()) {

			mostrarVistaPelicula();
		} else if (e.getSource() == this.getVm().getBtnAlquiler()) {
			mostrarVistaAlquiler();
		} else if (e.getSource() == this.getVm().getBtnSocio()) {
			mostrarVistaSocio();
		} else if (e.getSource() == this.getVm().getBtnMultas()) {
			mostrarVistaMulta();
		}else if (e.getSource() == this.getVm().getBtnReportes()) {
			mostrarVistaReporte();	
		}else if (e.getSource()== this.getVm().getBtnDevolucion()) {
			mostrarVistaDevolucion();
		}else if (e.getSource()== this.getVm().getBtnUsuario()) {
			mostrarVistaUsuario();
		}
		
	}
	private void mostrarVistaUsuario() {
		habilitarBotones(false);
		ControladorVistaUsuario cvu = new ControladorVistaUsuario(this);
		this.getVm().getPanelContenedor().add(cvu.getVu());
		cvu.getVu().show();
		cvu.getVu().setLocation(0, 0);
	}
	private void mostrarVistaDevolucion() {
		habilitarBotones(false);
		ControladorVistaDevolucion cvd = new ControladorVistaDevolucion(this);
		this.getVm().getPanelContenedor().add(cvd.getVd());
		cvd.getVd().show();
		cvd.getVd().setLocation(0, 0);
	}
	private void mostrarVistaReporte() {
		habilitarBotones(false);
		ControladorVistaReporte cvr = new ControladorVistaReporte(this);
		this.getVm().getPanelContenedor().add(cvr.getVr());
		cvr.getVr().show();
		cvr.getVr().setLocation(0, 0);
	}
	private void mostrarVistaPelicula() {
		habilitarBotones(false);
		ControladorVistaPelicula cvp = new ControladorVistaPelicula(this);
		this.getVm().getPanelContenedor().add(cvp.getVp());
		cvp.getVp().show();
		cvp.getVp().setLocation(0, 0);
	}

	private void mostrarVistaAlquiler() {
		habilitarBotones(false);
		ControladorVistaAlquilar cva = new ControladorVistaAlquilar(this);
		this.getVm().getPanelContenedor().add(cva.getVa());
		cva.getVa().show();
		cva.getVa().setLocation(0, 0);

	}

	private void mostrarVistaSocio() {
		habilitarBotones(false);
		ControladorVistaSocio cvs = new ControladorVistaSocio(this);
		this.getVm().getPanelContenedor().add(cvs.getVs());
		cvs.getVs().show();
		cvs.getVs().setLocation(0, 0);
	}

	private void mostrarVistaMulta() {
		habilitarBotones(false);
		ControladorVistaMulta cvm = new ControladorVistaMulta(this);
		this.getVm().getPanelContenedor().add(cvm.getVm());
		cvm.getVm().show();
		cvm.getVm().setLocation(0, 0);
	}

	public void habilitarBotones(boolean aux) {
		this.getVm().getBtnAlquiler().setEnabled(aux);
		this.getVm().getBtnMultas().setEnabled(aux);
		this.getVm().getBtnPelicula().setEnabled(aux);
		this.getVm().getBtnSocio().setEnabled(aux);
		this.getVm().getBtnReportes().setEnabled(aux);
		this.getVm().getBtnUsuario().setEnabled(aux);
		this.getVm().getBtnDevolucion().setEnabled(aux);
	}

	public VistaMenu getVm() {
		return vm;
	}

	public void setVm(VistaMenu vm) {
		this.vm = vm;
	}

}
