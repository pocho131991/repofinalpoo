package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalDate;

import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import modelo.DAOMulta;
import modelo.Multa;
import tabla.Tabla;
import vista.VistaMulta;

public class ControladorVistaMulta extends KeyAdapter implements ActionListener, MouseListener,InternalFrameListener {

	private VistaMulta vm;
	private ControladorVistaMenu cvm;
	private Tabla t = new Tabla();

	public ControladorVistaMulta(ControladorVistaMenu cvm) {
		this.setCvm(cvm);
		this.setVm(new VistaMulta(this));
	}

	@Override
	public void keyReleased(KeyEvent e) {
	if (e.getSource()==this.getVm().getTxtFiltro()) {
		this.getVm().busqueda(this.getVm().getTxtFiltro().getText());
		
	}
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			Integer fila = this.getVm().getTablaMultas().rowAtPoint(e.getPoint());
			this.getVm().getTxtCod_multa()
					.setText(this.getVm().getTablaMultas().getValueAt(fila, 0).toString());
			this.getVm().getTxtNroSocioMulta()
					.setText(this.getVm().getTablaMultas().getValueAt(fila, 1).toString());
			this.getVm().getTxtTipo_multa()
					.setText(this.getVm().getTablaMultas().getValueAt(fila, 4).toString());
			this.getVm().getTxtFecha_multa()
					.setText(this.getVm().getTablaMultas().getValueAt(fila, 6).toString());
			this.getVm().getTxtCosto().setText(this.getVm().getTablaMultas().getValueAt(fila, 8).toString());
			this.getVm().getCmbPagado().setEnabled(true);
			this.getVm().getBtnModificarMulta().setEnabled(true);
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==this.getVm().getBtnAgregarMulta()) {
			new ControladorAltaMulta();
		}else {
			modificarMulta();
			this.getT().visualizar_TablaMultas(this.getVm().getTablaMultas());

		}

	}
	private void modificarMulta() {
		DAOMulta daom = new DAOMulta();
		Multa m = new Multa();
		m.setCod_multa(Integer.parseInt(this.getVm().getTxtCod_multa().getText()));
		m.setNro_socio(Integer.parseInt(this.getVm().getTxtNroSocioMulta().getText()));
		m.setTipo_multa(Integer.parseInt(this.getVm().getTxtTipo_multa().getText()));
		String[] fecha = this.getVm().getTxtFecha_multa().getText().split("-");
		m.setFecha_multa(
				LocalDate.of(Integer.parseInt(fecha[0]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[2])));

		if (this.getVm().getCmbPagado().getSelectedItem().toString().equals("Pagado")) {// Pagado
			m.setPagado(true);
		} else {
			m.setPagado(false);
		}

		daom.modificarMulta(m);

	}

	public VistaMulta getVm() {
		return vm;
	}

	public void setVm(VistaMulta vm) {
		this.vm = vm;
	}

	public ControladorVistaMenu getCvm() {
		return cvm;
	}

	public void setCvm(ControladorVistaMenu cvm) {
		this.cvm = cvm;
	}

	@Override
	public void internalFrameActivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void internalFrameClosed(InternalFrameEvent arg0) {
		this.getCvm().habilitarBotones(true);
		
	}

	@Override
	public void internalFrameClosing(InternalFrameEvent arg0) {
		Integer rta = JOptionPane.showConfirmDialog(null, "Desea salir ?", "Mensaje",
				JOptionPane.OK_CANCEL_OPTION);
		if (rta == 0) {
			this.getVm().dispose();
		}
		
		
	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void internalFrameIconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void internalFrameOpened(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public Tabla getT() {
		return t;
	}

	public void setT(Tabla t) {
		this.t = t;
	}

}
