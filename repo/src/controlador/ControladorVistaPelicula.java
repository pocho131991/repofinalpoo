package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import modelo.DAOPeliculasImpl;
import modelo.Pelicula;
import tabla.Tabla;
import vista.VistaPelicula;

public class ControladorVistaPelicula extends KeyAdapter
		implements ActionListener, MouseListener, InternalFrameListener {

	private VistaPelicula vp;
	private Tabla t = new Tabla();
	static String x;
	private DAOPeliculasImpl daop = new DAOPeliculasImpl();
	private ControladorVistaMenu cvm;

	public ControladorVistaPelicula() {
		this.setVp(new VistaPelicula(this));
		x = "x";
	}

	public ControladorVistaPelicula(ControladorVistaMenu cvm) {
		this.setVp(new VistaPelicula(this));
		this.setCvm(cvm);
	}

	
	@Override
	public void keyReleased(KeyEvent e) {
	
		if (e.getSource()==this.getVp().getTxtFiltroPelicula()) {
			this.getVp().busqueda(this.getVp().getTxtFiltroPelicula().getText());
		}
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() == this.getVp().getTablaPelicula()) {
			if (e.getClickCount() == 1) {
				this.getVp().getBtnModificarPelicula().setEnabled(true);
				this.getVp().getBtnEliminarPelicula().setEnabled(true);
			}
			DAOPeliculasImpl daop = new DAOPeliculasImpl();
			Integer indice = this.getVp().getTablaPelicula().getSelectedRow();

			try {
				byte[] bi = daop.obtenerImagen(
						Integer.valueOf(this.getVp().getTablaPelicula().getValueAt(indice, 0).toString()));
				InputStream in = new ByteArrayInputStream(bi);
				BufferedImage image = ImageIO.read(in);
				ImageIcon imgi = new ImageIcon(image.getScaledInstance(236, 275, 0));
				this.getVp().getLblPortada().setIcon(imgi);
			} catch (IOException e1) {

				e1.printStackTrace();
			}
			Integer codigo = Integer.parseInt(this.getVp().getTablaPelicula().getValueAt(indice, 0).toString());
			this.getVp().getTxtASinopsis().setText(daop.obtenerSinopsis(codigo));
			this.getVp().getTxtAActores().setText(daop.obtenerActores(codigo));
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getVp().getBtnAgregarPelicula()) {
			new ControladorAltaPelicula();
			this.getT().visualizar_TablaReducidaPelicula(this.getVp().getTablaPelicula());
		} else if (e.getSource() == this.getVp().getBtnModificarPelicula()) {
			Integer indice = this.getVp().getTablaPelicula().getSelectedRow();
			ArrayList<String> lista = new ArrayList<String>();
			Pelicula p = this.getDaop().obtenerCamposPelicula(
					Integer.valueOf(this.getVp().getTablaPelicula().getValueAt(indice, 0).toString()));

			lista.add(p.getTitulo());// obtener titulo
			lista.add(p.getTitulo_alterno());// obtener titulo latino
			lista.add(p.getDuracion().toString());// obtener duracion
			lista.add(p.getAnio().toString());// obtener a�o
			lista.add(p.getDirector());// obtener director
			lista.add(p.getActores_principales());// obtener actores
			lista.add(p.getCod_pelicula().toString());// obtener codigo pelicula
			lista.add(p.getGenero());// obtener genero
			lista.add(p.getSinopsis());// obtener sinopsis
			lista.add(p.getCantidad().toString());// obtener cantidad
			lista.add(p.getPrecio().toString());// obtener precio
			ControladorModElimPelicula cmep = new ControladorModElimPelicula();
			cmep.getVmep().completarCampos(lista);
			cmep.getVmep().setVisible(true);
			this.getT().visualizar_TablaReducidaPelicula(this.getVp().getTablaPelicula());
		} else if (e.getSource() == this.getVp().getBtnEliminarPelicula()) {
			Integer indice = this.getVp().getTablaPelicula().getSelectedRow();
			Pelicula pelicula = new Pelicula();
			pelicula.setCod_pelicula(Integer.valueOf(this.getVp().getTablaPelicula().getValueAt(indice, 0).toString()));
			Integer rta = JOptionPane.showConfirmDialog(null, "Desea eliminar esta pelicula", "Mensaje",
					JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				this.getDaop().eliminarPelicula(pelicula);
			}
			this.getT().visualizar_TablaReducidaPelicula(this.getVp().getTablaPelicula());
		}

	}

	public VistaPelicula getVp() {
		return vp;
	}

	public void setVp(VistaPelicula vp) {
		this.vp = vp;
	}

	public Tabla getT() {
		return t;
	}

	public void setT(Tabla t) {
		this.t = t;
	}

	public DAOPeliculasImpl getDaop() {
		return daop;
	}

	public void setDaop(DAOPeliculasImpl daop) {
		this.daop = daop;
	}

	@Override
	public void internalFrameActivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		this.getCvm().habilitarBotones(true);
	}

	@Override
	public void internalFrameClosing(InternalFrameEvent arg0) {
		Integer rta = JOptionPane.showConfirmDialog(null, "Desea salir ?", "Mensaje",
				JOptionPane.OK_CANCEL_OPTION);
		if (rta == 0) {
			this.getVp().dispose();
		}
		

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameOpened(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	public ControladorVistaMenu getCvm() {
		return cvm;
	}

	public void setCvm(ControladorVistaMenu cvm) {
		this.cvm = cvm;
	}

}
