package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import vista.VistaReporte;

public class ControladorVistaReporte implements InternalFrameListener, ActionListener {
	private VistaReporte vr;
	private ControladorVistaMenu cvm;
	private LocalDate fechaDesde;

	public ControladorVistaReporte(ControladorVistaMenu cvm) {
		this.setCvm(cvm);
		this.setVr(new VistaReporte(this));
	}

	@Override
	public void internalFrameActivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent arg0) {
		this.getCvm().habilitarBotones(true);

	}

	@Override
	public void internalFrameClosing(InternalFrameEvent arg0) {
		Integer rta = JOptionPane.showConfirmDialog(null, "Desea salir ?", "Mensaje", JOptionPane.OK_CANCEL_OPTION);
		if (rta == 0) {
			this.getVr().dispose();
		}

	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameOpened(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == this.getVr().getBtnListadoAlquiler()) {
			crearListadoAlquiler();
		} else if (e.getSource() == this.getVr().getBtnListadoSociosMultas()) { 
			try {
				Connection cn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/BD_TPF_POO", "postgres","astonbirra");
				String path = ("src\\jasper\\listado_socios_multas.jasper");
				JasperReport jr = null;
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				LocalDate textFieldAsDate = LocalDate.parse(this.getVr().getFtxtFechaDesde().getText(), formatter);
				Date fecha = Date.valueOf(textFieldAsDate);
				try {
					Map parametro = new HashMap();
					parametro.put("fecha_multa", fecha);
					jr = (JasperReport) JRLoader.loadObjectFromFile(path);
					JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
					JasperViewer.viewReport(jp,false);
				} catch (JRException f) {
					// TODO Auto-generated catch block
					f.printStackTrace();
				}
				cn.close();
			} catch (SQLException g) {
				// TODO Auto-generated catch block
				g.printStackTrace();
			}
		}

	}

	private void crearReporteListadoAlquiler(LocalDate textFieldAsDate) {
		try {
			Connection cn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/BD_TPF_POO", "postgres",
					"astonbirra");
			String path = ("src\\jasper\\listado_alquiler.jasper");
			JasperReport jr = null;

			try {
				// String text = getVr().getTextFechaDesde().getText();

				Map parametro = new HashMap();
				Date fecha = Date.valueOf(textFieldAsDate);
				parametro.put("desde_fecha_alquiler", fecha);
				jr = (JasperReport) JRLoader.loadObjectFromFile(path);
				JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
				// JasperViewer jv = new JasperViewer(jp,false);
				// jv.setVisible(true);
				// jv.setTitle(path);
				JasperViewer.viewReport(jp, false);

			} catch (JRException f) {
				// TODO Auto-generated catch block
				f.printStackTrace();

			}
			cn.close();

		} catch (SQLException g) {
			// TODO Auto-generated catch block
			g.printStackTrace();

		}

	}
//probar si anda correctamente por el ingreso incorrecto de fecha
	private void crearListadoAlquiler() {
		boolean correcto = true;
		try {
			String text = getVr().getFtxtFechaDesde().getText();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

			LocalDate textFieldAsDate = LocalDate.parse(text, formatter);
			this.setFechaDesde(textFieldAsDate);
		} catch (DateTimeException e) {
			correcto = false;
		}

		if (correcto == false) {
			JOptionPane.showMessageDialog(null, "Error en el ingreso de fecha. Intente nuevamente");

		} else {
			this.crearReporteListadoAlquiler(getFechaDesde());
		}
	}

	public VistaReporte getVr() {
		return vr;
	}

	public void setVr(VistaReporte vr) {
		this.vr = vr;
	}

	public ControladorVistaMenu getCvm() {
		return cvm;
	}

	public void setCvm(ControladorVistaMenu cvm) {
		this.cvm = cvm;
	}

	public LocalDate getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(LocalDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

}
