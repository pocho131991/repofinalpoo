package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import modelo.DAOSocio;
import modelo.Socio;
import vista.VistaSeleccionAlquileres;

public class ControladorVistaSeleccionAlquileres extends KeyAdapter implements ActionListener, MouseListener {

	private VistaSeleccionAlquileres vsa;
	private ControladorVistaDevolucion cvd;

	public ControladorVistaSeleccionAlquileres(ControladorVistaDevolucion cvd) {
		this.setCvd(cvd);
		this.setVsa(new VistaSeleccionAlquileres(this));
		this.getVsa().setVisible(true);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	if (e.getClickCount()==1) {
		this.getVsa().getBtnSeleccionPelicula().setEnabled(true);
	}

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == this.getVsa().getBtnSeleccionPelicula()) {
			DAOSocio daos = new DAOSocio();
			
			Integer fila = this.getVsa().getTablaSeleccionAlquileres().getSelectedRow();
			//Socio s = "Cod Pelicula", "Titulo", "Nro Socio", "Fecha Alquiler", "Fecha Entrega",
			Socio s = daos.buscarSocio(this.getVsa().getTablaSeleccionAlquileres().getValueAt(fila, 2).toString());
			ArrayList<String> seleccion = new ArrayList<String>();
			seleccion.add(this.getVsa().getTablaSeleccionAlquileres().getValueAt(fila, 2).toString());//nro socio
			seleccion.add(s.getApellido()+" "+s.getNombre());//apellido y nombre socio
			seleccion.add(this.getVsa().getTablaSeleccionAlquileres().getValueAt(fila, 0).toString());//codigo pelicula
			seleccion.add(this.getVsa().getTablaSeleccionAlquileres().getValueAt(fila, 1).toString());//titulo
			seleccion.add(this.getVsa().getTablaSeleccionAlquileres().getValueAt(fila, 3).toString());//fecha alquiler
			seleccion.add(this.getVsa().getTablaSeleccionAlquileres().getValueAt(fila, 4).toString());// fecha entrega
			this.getCvd().getVd().completarCampos(seleccion);
			
			this.getVsa().dispose();
			
		}

	}
	

	public VistaSeleccionAlquileres getVsa() {
		return vsa;
	}

	public void setVsa(VistaSeleccionAlquileres vsa) {
		this.vsa = vsa;
	}

	public ControladorVistaDevolucion getCvd() {
		return cvd;
	}

	public void setCvd(ControladorVistaDevolucion cvd) {
		this.cvd = cvd;
	}

}
