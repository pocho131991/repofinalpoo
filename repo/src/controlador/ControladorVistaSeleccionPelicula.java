package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import modelo.DAOPeliculasImpl;
import modelo.Pelicula;
import tabla.Tabla;
import tabla.TablaModelo;
import vista.VistaSeleccionPelicula;

public class ControladorVistaSeleccionPelicula extends KeyAdapter implements ActionListener, MouseListener {
	private VistaSeleccionPelicula vsp;
	private Controlador1 c;
	private ControladorVistaAlquilar cva;

	public ControladorVistaSeleccionPelicula(Controlador1 c) {
		this.setC(c);
		this.setVsp(new VistaSeleccionPelicula(this));
		this.getVsp().setVisible(true);
	}

	public ControladorVistaSeleccionPelicula(ControladorVistaAlquilar cva) {
		this.setCva(getCva());
		this.setVsp(new VistaSeleccionPelicula(this));
		this.getVsp().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getVsp().getBtnSeleccionPelicula()) {
			completarTablaSeleccion();
			//this.getC().setSocioSeleccionado(obtenerSocio());
			//this.getVsp().dispose();
		}

	}

	private void completarTablaSeleccion() {
		Integer fila = this.getVsp().getTablaSeleccionPelicula().getSelectedRow();
		DAOPeliculasImpl daop = new DAOPeliculasImpl();
		Pelicula p = daop.obtenerCamposPelicula(Integer.parseInt(this.getVsp().getTablaSeleccionPelicula().getValueAt(fila, 0).toString()));
		TablaModelo tabm = (TablaModelo) this.getCva().getVa().getTablaSeleccion().getModel();
		Object fila_tabla_Seleccion[] = new Object[5];
		fila_tabla_Seleccion[0] = p.getCod_pelicula();// codigo pelicula
		fila_tabla_Seleccion[1] = p.getTitulo();// titulo
		fila_tabla_Seleccion[2] = p.getTitulo_alterno();// titulo latino
		fila_tabla_Seleccion[3] = p.getGenero();// genero
		fila_tabla_Seleccion[4] = 1;// cantidad
		tabm.addRow(fila_tabla_Seleccion);
		this.getCva().getVa().getTablaSeleccion().setModel(tabm);
		this.getCva().getVa().getTablaSeleccion().getColumnModel().getColumn(0).setPreferredWidth(80);
		this.getCva().getVa().getTablaSeleccion().getColumnModel().getColumn(1).setPreferredWidth(80);
		this.getCva().getVa().getTablaSeleccion().getColumnModel().getColumn(2).setPreferredWidth(80);
		this.getCva().getVa().getTablaSeleccion().getColumnModel().getColumn(3).setPreferredWidth(80);
		this.getCva().getVa().getTablaSeleccion().getColumnModel().getColumn(4).setPreferredWidth(80);
		Integer cantidad = Integer.parseInt(this.getCva().getVa().getTxtCantipeliculas().getText()) + 1;
		Double total = Double.parseDouble(this.getCva().getVa().getTxtPagar().getText()) + daop
				.obtenerPrecio(Integer.valueOf(this.getCva().getVa().getTablaPelicula().getValueAt(fila, 0).toString()));

		this.getCva().getVa().getTxtPagar().setText(total.toString());
		this.getCva().getVa().getTxtCantipeliculas().setText(cantidad.toString());

		////////////////////// Obtener codigo pelicula
		this.getCva().getCodigosPelicula()
				.add(Integer.parseInt(this.getCva().getVa().getTablaPelicula().getValueAt(fila, 0).toString()));
	}
	@Override
	public void keyReleased(KeyEvent e) {

		if (e.getSource() == this.getVsp().getTxtFiltro()) {
			TablaModelo tabm = (TablaModelo) this.getVsp().getTablaSeleccionPelicula().getModel();
			Tabla t = new Tabla();
			TableRowSorter<DefaultTableModel> trs = new TableRowSorter<DefaultTableModel>(tabm);
			List<RowSorter.SortKey> sortKeys = new ArrayList<>();
			sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
			trs.setSortKeys(sortKeys);
			trs.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVsp().getTxtFiltro().getText(), 1));
			trs.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVsp().getTxtFiltro().getText(), 2));
			this.getVsp().getTablaSeleccionPelicula().setRowSorter(trs);
			t.visualizar_TablaPeliculaReducida(this.getVsp().getTablaSeleccionPelicula());
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == this.getVsp().getTablaSeleccionPelicula()) {
			this.getVsp().getBtnSeleccionPelicula().setEnabled(true);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}



	public VistaSeleccionPelicula getVsp() {
		return vsp;
	}

	public void setVsp(VistaSeleccionPelicula vsp) {
		this.vsp = vsp;
	}

	public Controlador1 getC() {
		return c;
	}

	public void setC(Controlador1 c) {
		this.c = c;
	}

	public ControladorVistaAlquilar getCva() {
		return cva;
	}

	public void setCva(ControladorVistaAlquilar cva) {
		this.cva = cva;
	}



}
