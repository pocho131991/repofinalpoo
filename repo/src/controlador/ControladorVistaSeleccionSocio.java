package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import modelo.DAOMulta;
import modelo.Exception_VideoClub;
import modelo.Multa;
import modelo.Socio;
import tabla.Tabla;
import tabla.TablaModelo;
import vista.VistaSeleccionSocio;

public class ControladorVistaSeleccionSocio extends KeyAdapter implements ActionListener, MouseListener {
	private VistaSeleccionSocio vss;
	private ControladorAltaMulta cam;
	private ControladorVistaAlquilar cva;
	private Multa multa = new Multa();
	private Integer codigo;

	public ControladorVistaSeleccionSocio(ControladorAltaMulta cam) {
		this.setCam(cam);
		this.setVss(new VistaSeleccionSocio(this));
		this.getVss().setVisible(true);
	}

	public ControladorVistaSeleccionSocio(ControladorVistaAlquilar cva) {
		this.setVss(new VistaSeleccionSocio(this));
		this.getVss().setVisible(true);
		this.setCva(cva);
	}

	public ControladorVistaSeleccionSocio() {
		this.setVss(new VistaSeleccionSocio(this));

	}

	@Override
	public void keyReleased(KeyEvent e) {

		if (e.getSource() == this.getVss().getTxtFiltro()) {
			TablaModelo tabm = (TablaModelo) this.getVss().getTablaSeleccionSocio().getModel();
			Tabla t = new Tabla();
			TableRowSorter<DefaultTableModel> trs = new TableRowSorter<DefaultTableModel>(tabm);
			List<RowSorter.SortKey> sortKeys = new ArrayList<>();
			sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
			trs.setSortKeys(sortKeys);
			trs.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVss().getTxtFiltro().getText(), 1));
			trs.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVss().getTxtFiltro().getText(), 2));
			this.getVss().getTablaSeleccionSocio().setRowSorter(trs);
			t.visualizar_TablaSocio(this.getVss().getTablaSeleccionSocio());
		}
	}

	public VistaSeleccionSocio getVss() {
		return vss;
	}

	public void setVss(VistaSeleccionSocio vss) {
		this.vss = vss;
	}

	private void agregarSocio() {

		Integer fila = this.getVss().getTablaSeleccionSocio().getSelectedRow();
		this.setCodigo(Integer.parseInt(this.getVss().getTablaSeleccionSocio().getValueAt(fila, 0).toString()));
		String nomApe = this.getVss().getTablaSeleccionSocio().getValueAt(fila, 2).toString() + " "
				+ this.getVss().getTablaSeleccionSocio().getValueAt(fila, 3).toString();
		String direccion = this.getVss().getTablaSeleccionSocio().getValueAt(fila, 4).toString();
		String telefono = this.getVss().getTablaSeleccionSocio().getValueAt(fila, 5).toString();
		this.getCva().getVa().completarCamposSocio(
				this.getVss().getTablaSeleccionSocio().getValueAt(fila, 0).toString(), nomApe, direccion, telefono);
		DAOMulta daom = new DAOMulta();
		this.setMulta(daom.buscarSocioMulta(this.getVss().getTablaSeleccionSocio().getValueAt(fila, 0).toString()));
		if (this.getMulta().getNro_socio() == null) {
			JOptionPane.showMessageDialog(null, "El Nro de socio "
					+ this.getVss().getTablaSeleccionSocio().getValueAt(fila, 0) + " no posee multas sin pagar");
			this.getCva().getVa().getLblSocioMulta().setText("El Nro de socio "
					+ this.getVss().getTablaSeleccionSocio().getValueAt(fila, 0) + " no posee multas sin pagar");
			this.getCva().getVa().getLblSocioMulta().setForeground(new Color(0, 153, 51));
			habilitar(true);
			this.getCva().getVa().getBtnAlquilar().setEnabled(true);
		} else {
			JOptionPane.showMessageDialog(null,
					"El Nro de socio " + this.getVss().getTablaSeleccionSocio().getValueAt(fila, 0)
							+ " posee multas sin pagar. No puede alquilar peliculas. ");
			this.getCva().getVa().getLblSocioMulta().setText("El Nro de socio "
					+ this.getVss().getTablaSeleccionSocio().getValueAt(fila, 0) + " posee multas sin pagar");
			this.getCva().getVa().getLblSocioMulta().setForeground(Color.red);
			habilitar(false);
			this.getCva().getVa().getBtnAlquilar().setEnabled(false);
		}
		this.getVss().dispose();
		
	}

	private void agregarSocioMulta() {

		Integer fila = this.getVss().getTablaSeleccionSocio().getSelectedRow();
		this.getCam().getVam().getTxtNroSocio().setText(this.getVss().getTablaSeleccionSocio().getValueAt(fila, 0).toString());
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.getVss().getBtnSeleccionSocio()) {
			if (this.getCam() != null) {
				agregarSocioMulta();
				this.getVss().dispose();
			} else {
				agregarSocio();
			}
		}

	}

	private void habilitar(boolean aux) {
		this.getCva().getVa().getTablaPelicula().setEnabled(aux);
		this.getCva().getVa().getTablaSeleccion().setEnabled(aux);
		this.getCva().getVa().getBtnAlquilar().setEnabled(aux);
		this.getCva().getVa().getDateChooser().setEnabled(aux);
		this.getCva().getVa().getTxtFiltroAlquilerPelicula().setEnabled(aux);

	}

	public Socio obtenerSocio() {
		Socio s = new Socio();
		Integer fila = this.getVss().getTablaSeleccionSocio().getSelectedRow();
		s.setNro_socio(Integer.parseInt(this.getVss().getTablaSeleccionSocio().getValueAt(fila, 0).toString()));// Nro
																												// socio
		try {
			s.setDni_socio(Integer.parseInt(this.getVss().getTablaSeleccionSocio().getValueAt(fila, 1).toString()));
		} catch (NumberFormatException | Exception_VideoClub e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			s.setApellido(this.getVss().getTablaSeleccionSocio().getValueAt(fila, 2).toString());
		} catch (Exception_VideoClub e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			s.setNombre(this.getVss().getTablaSeleccionSocio().getValueAt(fila, 3).toString());
		} catch (Exception_VideoClub e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			s.setDireccion(this.getVss().getTablaSeleccionSocio().getValueAt(fila, 4).toString());
		} catch (Exception_VideoClub e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			s.setTelefono(Integer.parseInt(this.getVss().getTablaSeleccionSocio().getValueAt(fila, 5).toString()));
		} catch (NumberFormatException | Exception_VideoClub e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String fec = this.getVss().getTablaSeleccionSocio().getValueAt(fila, 6).toString();
		String[] fecha = fec.split("-");
		s.setFecha_nac(
				LocalDate.of(Integer.parseInt(fecha[0]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[2])));

		return s;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() == this.getVss().getTablaSeleccionSocio()) {
			this.getVss().getBtnSeleccionSocio().setEnabled(true);
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public ControladorAltaMulta getCam() {
		return cam;
	}

	public void setCam(ControladorAltaMulta cam) {
		this.cam = cam;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public ControladorVistaAlquilar getCva() {
		return cva;
	}

	public void setCva(ControladorVistaAlquilar cva) {
		this.cva = cva;
	}

	public Multa getMulta() {
		return multa;
	}

	public void setMulta(Multa multa) {
		this.multa = multa;
	}

}
