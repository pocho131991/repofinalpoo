package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.DateTimeException;
import java.time.LocalDate;

import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import modelo.DAOSocio;
import modelo.Exception_VideoClub;
import modelo.Socio;
import tabla.Tabla;
import vista.VistaSocio;

public class ControladorVistaSocio extends KeyAdapter implements ActionListener, MouseListener, InternalFrameListener {
	private VistaSocio vs;
	private ControladorVistaMenu cvm;
	private Tabla t = new Tabla();
	private DAOSocio daos = new DAOSocio();

	public ControladorVistaSocio(ControladorVistaMenu cvm) {
		this.setCvm(cvm);
		this.setVs(new VistaSocio(this));
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getSource() == this.getVs().getTxtFiltro()) {
			TableRowSorter<DefaultTableModel> trs_socio = new TableRowSorter<DefaultTableModel>(
					this.getT().getDtm_socio());
			/*
			 * List<RowSorter.SortKey> sortKeys = new ArrayList<>(); sortKeys.add(new
			 * RowSorter.SortKey(0, SortOrder.ASCENDING)); trs_socio.setSortKeys(sortKeys);
			 */
			trs_socio.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVs().getTxtFiltro().getText(), 0));
			trs_socio.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVs().getTxtFiltro().getText(), 1));
			trs_socio.setRowFilter(RowFilter.regexFilter("(?i)" + this.getVs().getTxtFiltro().getText(), 2));
			this.getVs().getTablaSocio().setRowSorter(trs_socio);
			this.getT().visualizar_TablaSocio(this.getVs().getTablaSocio());
		}
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		Integer rta = JOptionPane.showConfirmDialog(null, "Desea modificar/eliminar al socio", "Mensaje",
				JOptionPane.OK_CANCEL_OPTION);
		if (rta == 0) {
			Integer fila_s = this.getVs().getTablaSocio().rowAtPoint(e.getPoint());
			this.getVs().getTxtNumSoc()
					.setText("" + this.getVs().getTablaSocio().getValueAt(fila_s, 0).toString());
			this.getVs().getTxtDniSoc()
					.setText("" + this.getVs().getTablaSocio().getValueAt(fila_s, 1).toString());
			this.getVs().getTxt_apellido_socio()
					.setText("" + this.getVs().getTablaSocio().getValueAt(fila_s, 2).toString());
			this.getVs().getTxt_nombre_socio()
					.setText("" + this.getVs().getTablaSocio().getValueAt(fila_s, 3).toString());
			this.getVs().getTxt_direccion()
					.setText("" + this.getVs().getTablaSocio().getValueAt(fila_s, 4).toString());
			this.getVs().getTxt_tel()
					.setText("" + this.getVs().getTablaSocio().getValueAt(fila_s, 5).toString());
			this.getVs().getTxt_fecnac()
					.setText("" + this.getVs().getTablaSocio().getValueAt(fila_s, 6).toString());
		}

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource()==this.getVs().getBtnAgregarSocio()) {
			new ControladorAltaSocio();
			this.getT().visualizar_TablaSocio(this.getVs().getTablaSocio());
		}else if (e.getSource()==this.getVs().getBtnModificarSocio()) {
			Integer rta = JOptionPane.showConfirmDialog(null, "Desea modificar esta pelicula", "Mensaje",
					JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				this.getDaos().modificarPersona(inicializarSocio());
			}
			this.getT().visualizar_TablaSocio(this.getVs().getTablaSocio());
		}else {
			Socio s = new Socio();
			s.setNro_socio(Integer.valueOf(this.getVs().getTxtNumSoc().getText()));
			Integer rta = JOptionPane.showConfirmDialog(null, "Desea eliminar al socio nro " + s.getNro_socio(),
					"Mensaje", JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				this.getDaos().eliminarPersona(s);
			}

			this.getT().visualizar_TablaSocio(this.getVs().getTablaSocio());
		}

	}
	private Socio inicializarSocio() {
		Socio s = new Socio();

		s.setNro_socio(Integer.valueOf(this.getVs().getTxtNumSoc().getText()));
		try {
			s.setDni_socio(Integer.parseInt(this.getVs().getTxtDniSoc().getText()));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception_VideoClub e) {
			e.printStackTrace();
		}

		try {
			s.setApellido(this.getVs().getTxt_apellido_socio().getText());
		} catch (Exception_VideoClub e) {
			e.printStackTrace();
		}
		try {
			s.setNombre(this.getVs().getTxt_nombre_socio().getText());
		} catch (Exception_VideoClub e) {
			e.printStackTrace();
		}
		try {
			s.setDireccion(this.getVs().getTxt_direccion().getText());
		} catch (Exception_VideoClub e) {

			e.printStackTrace();
		}
		try {
			s.setTelefono(Integer.parseInt(this.getVs().getTxt_tel().getText()));
		} catch (NumberFormatException e) {

			e.printStackTrace();
		} catch (Exception_VideoClub e) {

			e.printStackTrace();
		}

		try {

			String[] fecha = this.getVs().getTxt_fecnac().getText().split("-");
			s.setFecha_nac(
					LocalDate.of(Integer.parseInt(fecha[0]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[2])));
		} catch (DateTimeException e) {
			e.printStackTrace();
		}

		return s;
	}

	@Override
	public void internalFrameActivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameClosed(InternalFrameEvent e) {
		
		this.getCvm().habilitarBotones(true);
	}

	@Override
	public void internalFrameClosing(InternalFrameEvent e) {
		Integer rta = JOptionPane.showConfirmDialog(null, "Desea salir ?", "Mensaje",
				JOptionPane.OK_CANCEL_OPTION);
		if (rta == 0) {
			this.getVs().dispose();
		}
		
		//this.getCvm().habilitarBotones(true);
	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameIconified(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalFrameOpened(InternalFrameEvent e) {
		// TODO Auto-generated method stub

	}

	public VistaSocio getVs() {
		return vs;
	}

	public void setVs(VistaSocio vs) {
		this.vs = vs;
	}

	public ControladorVistaMenu getCvm() {
		return cvm;
	}

	public void setCvm(ControladorVistaMenu cvm) {
		this.cvm = cvm;
	}

	public Tabla getT() {
		return t;
	}

	public void setT(Tabla t) {
		this.t = t;
	}

	public DAOSocio getDaos() {
		return daos;
	}

	public void setDaos(DAOSocio daos) {
		this.daos = daos;
	}

}
