package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.DateTimeException;
import java.time.LocalDate;

import javax.swing.JOptionPane;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import modelo.DAOUsuario;
import modelo.Exception_VideoClub;
import modelo.Usuario;
import tabla.Tabla;
import vista.VistaUsuario;

public class ControladorVistaUsuario implements ActionListener,MouseListener,InternalFrameListener{
	private ControladorVistaMenu cvm;
	private VistaUsuario vu;
	private Tabla t = new Tabla();
	
	public ControladorVistaUsuario(ControladorVistaMenu cvm) {
		this.setCvm(cvm);
		this.setVu(new VistaUsuario(this));
	
	}
	private void modificarUsuario() {
		Usuario u = new Usuario();
		DAOUsuario daou = new DAOUsuario();
		boolean correcto = true;

		u.setId_usuario(Integer.valueOf(this.getVu().getTxtIDUsuario().getText()));

		try {
			u.setNombre(this.getVu().getTxtNomUsuario().getText());
		} catch (Exception_VideoClub e) {
			correcto = false;
			this.getVu().getLblExcepcionUsuarioNombre().setText("Nombre Usuario -" + e.getMessage() + "\n");
		}
		try {
			u.setApellido(this.getVu().getTxtApeUsuario().getText());
		} catch (Exception_VideoClub e) {
			correcto = false;

			this.getVu().getLblExcepcionUsuarioApellido().setText("Apellido Usuario -" + e.getMessage() + "\n");

		}
		try {
			u.setEmail(this.getVu().getTxtEmailUsuario().getText());
		} catch (Exception_VideoClub e) {
			correcto = false;
			this.getVu().getLblExcepcionUsuarioEmail().setText("Email Usuario -" + e.getMessage() + "\n");

		}

		try {
			String[] fecha = this.getVu().getTxtFecNacUsuario().getText().split("-");
			u.setFec_nac(
					LocalDate.of(Integer.parseInt(fecha[0]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[2])));

		} catch (DateTimeException e2) {
			correcto = false;
			this.getVu().getLblExcepcionUsuarioFecNac()
					.setText("Fecha de nacimiento Usuario - Error en fecha" + "\n");
		} catch (NumberFormatException nfe) {
			correcto = false;
			this.getVu().getLblExcepcionUsuarioFecNac()
					.setText("Fecha de nacimiento - Error en fecha - Formato incorrecto" + "\n");
		}

		try {

			u.setContrasena(String.valueOf(this.getVu().getPassfieldContra().getPassword()));

		} catch (Exception_VideoClub e) {
			correcto = false;
			this.getVu().getLblExcepcionUsuarioContrasenia().setText("Contrase�a Usuario " + e.getMessage() + "\n");

		}

		if (correcto) {
			JOptionPane.showMessageDialog(null, "Se ingres� usuario correctamente");
			daou.modificarUsuario(u);
		} else {
			JOptionPane.showMessageDialog(null, "Se ingres� usuario incorrectamente");

		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == this.getVu().getTablaUsuario()) {
			if (e.getClickCount() == 1) {
				Integer fila = this.getVu().getTablaUsuario().rowAtPoint(e.getPoint());
				this.getVu().getTxtIDUsuario()
						.setText(this.getVu().getTablaUsuario().getValueAt(fila, 0).toString());
				this.getVu().getTxtApeUsuario()
						.setText(this.getVu().getTablaUsuario().getValueAt(fila, 2).toString());
				this.getVu().getTxtNomUsuario()
						.setText(this.getVu().getTablaUsuario().getValueAt(fila, 1).toString());
				this.getVu().getTxtEmailUsuario()
						.setText(this.getVu().getTablaUsuario().getValueAt(fila, 3).toString());
				this.getVu().getTxtFecNacUsuario()
						.setText(this.getVu().getTablaUsuario().getValueAt(fila, 5).toString());
				this.getVu().getPassfieldContra()
						.setText(this.getVu().getTablaUsuario().getValueAt(fila, 4).toString());
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource()==this.getVu().getBtnModificarUsuario()) {
			Integer rta = JOptionPane.showConfirmDialog(null, "Desea modificar al usuario", "Mensaje",
					JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				modificarUsuario();
			}
			this.getT().visualizar_TablaUsuario(this.getVu().getTablaUsuario());

		}else {
			Integer rta = JOptionPane.showConfirmDialog(null, "Desea eliminar al usuario?", "Mensaje",
					JOptionPane.OK_CANCEL_OPTION);
			if (rta == 0) {
				DAOUsuario daou = new DAOUsuario();
				daou.eliminarUsuario(Integer.parseInt(this.getVu().getTxtIDUsuario().getText()));
			}
			this.getT().visualizar_TablaUsuario(this.getVu().getTablaUsuario());
		}
		
	}

	@Override
	public void internalFrameActivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void internalFrameClosed(InternalFrameEvent arg0) {
		
		
	}

	@Override
	public void internalFrameClosing(InternalFrameEvent arg0) {
		this.getCvm().habilitarBotones(true);
		
	}

	@Override
	public void internalFrameDeactivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		Integer rta = JOptionPane.showConfirmDialog(null, "Desea salir ?", "Mensaje",
				JOptionPane.OK_CANCEL_OPTION);
		if (rta == 0) {
			this.getVu().dispose();
		}
	}

	@Override
	public void internalFrameDeiconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void internalFrameIconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void internalFrameOpened(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public ControladorVistaMenu getCvm() {
		return cvm;
	}

	public void setCvm(ControladorVistaMenu cvm) {
		this.cvm = cvm;
	}

	public VistaUsuario getVu() {
		return vu;
	}

	public void setVu(VistaUsuario vu) {
		this.vu = vu;
	}
	public Tabla getT() {
		return t;
	}
	public void setT(Tabla t) {
		this.t = t;
	}
}
