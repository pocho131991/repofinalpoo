package modelo;

import java.time.LocalDate;

public class Alquiler {

	private Integer cod_alquiler;
	private Integer cod_pelicula;
	private String titulo;
	private Integer nro_socio;
	private LocalDate fecha_alquiler;
	private LocalDate fecha_entrega;
	private Float monto;
	
	public Alquiler() {

	}
	
	public Alquiler(Integer cod_alquiler, Integer cod_pelicula, String titulo, Integer nro_socio, LocalDate fecha_alquiler,
			LocalDate fecha_entrega, Float monto) {
		super();
		this.setCod_alquiler(cod_alquiler);
		this.setCod_pelicula(cod_pelicula);;
		this.setTitulo(titulo);
		this.setNro_socio(nro_socio);
		this.setFecha_alquiler(fecha_alquiler);
		this.setFecha_entrega(fecha_entrega);
		this.setMonto(monto);
	}

	//Getters & Setters
	public Integer getCod_alquiler() {
		return cod_alquiler;
	}
	public void setCod_alquiler(Integer cod_alquiler) {
		this.cod_alquiler = cod_alquiler;
	}
	public Integer getCod_pelicula() {
		return cod_pelicula;
	}
	public void setCod_pelicula(Integer cod_pelicula) {
		this.cod_pelicula = cod_pelicula;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Integer getNro_socio() {
		return nro_socio;
	}
	public void setNro_socio(Integer nro_socio) {
		this.nro_socio = nro_socio;
	}
	public LocalDate getFecha_alquiler() {
		return fecha_alquiler;
	}
	public void setFecha_alquiler(LocalDate fecha_alquiler) {
		this.fecha_alquiler = fecha_alquiler;
	}
	public LocalDate getFecha_entrega() {
		return fecha_entrega;
	}
	public void setFecha_entrega(LocalDate fecha_entrega) {
		this.fecha_entrega = fecha_entrega;
	}
	public Float getMonto() {
		return monto;
	}
	public void setMonto(Float monto) {
		this.monto = monto;
	}
	
}
