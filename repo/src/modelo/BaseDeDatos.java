package modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BaseDeDatos {

	private Connection conexion = null;
	private static BaseDeDatos bd = null;

	private BaseDeDatos() {
		estableceConexion();
	}

	private void estableceConexion() {
		if (conexion != null)
			return;

		try {
			
			DriverManager.registerDriver(new org.postgresql.Driver());
			
			conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/BD_TPF_POO", "postgres",
					"astonbirra");
			//conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Database_TP_final_POO", "postgres","astonbirra");
			// conexion = DriverManager.getConnection(
			// "jdbc:mysql://172.17.111.138/ex000377_afip","ex000377_afip","paLEpili01");Database_TP_final_POO
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static BaseDeDatos getInstance() {
		if (bd == null) {
			bd = new BaseDeDatos();
		}
		return bd;
	}

	public boolean altaPelicula(String consulta, Pelicula pelicula) {
		PreparedStatement s = null;
		boolean resultado = false;

		try {
			s = conexion.prepareStatement(consulta);
			s.setInt(1, pelicula.getCod_pelicula());
			s.setString(2, pelicula.getTitulo());
			s.setString(3, pelicula.getTitulo_alterno());
			s.setInt(4, pelicula.getDuracion());
			s.setInt(5, pelicula.getAnio());
			s.setString(6, pelicula.getDirector());
			s.setString(7, pelicula.getActores_principales());
			s.setString(8, pelicula.getSinopsis());
			s.setString(9, pelicula.getGenero());
			s.setBytes(10, pelicula.getFoto());
			s.setString(11, pelicula.getFormato());
			s.setInt(12, pelicula.getCantidad());
			s.setDouble(13, pelicula.getPrecio());
			s.execute();

		} catch (Exception e) {

		}

		return resultado;
	}

	public boolean altaSocio(String consulta, Socio socio) {
		PreparedStatement s = null;
		boolean resultado = false;

		try {
		
			s = conexion.prepareStatement(consulta);
			s.setInt(1, socio.getNro_socio());
			s.setInt(2, socio.getDni_socio());
			s.setString(3, socio.getApellido());
			s.setString(4, socio.getNombre());
			s.setString(5, socio.getDireccion());
			s.setInt(6, socio.getTelefono());
			s.setDate(7, Date.valueOf(socio.getFecha_nac()));

			s.execute();
			// resultado = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}

	public boolean bajaUsuario (String consulta) {
		PreparedStatement s = null;
		boolean resultado = false;
		try {
			s = conexion.prepareStatement(consulta);
			s.executeUpdate();
		
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return resultado;
	}
	


	public boolean bajaSocio(String consulta, Socio socio) {

		PreparedStatement s = null;
		boolean resultado = false;
		try {
			s = conexion.prepareStatement(consulta);
			s.setInt(1, socio.getNro_socio());
			s.executeUpdate();
			resultado = true;
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return resultado;
	}

	public boolean modificarSocio(String consulta, Socio socio) {
		PreparedStatement s = null;
		boolean resultado = false;
		try {
			String nombre = socio.getNombre();
			String apellido = socio.getApellido();
			Integer dni = socio.getDni_socio();
			Integer tel = socio.getTelefono();
			String direccion = socio.getDireccion();
			Date fecha = Date.valueOf(socio.getFecha_nac());
			s = conexion.prepareStatement(consulta);

			s.setInt(1, dni);
			s.setString(2, apellido);
			s.setString(3, nombre);
			s.setString(4, direccion);
			s.setInt(5, tel);
			s.setDate(6, fecha);
			s.setInt(7, socio.getNro_socio());
			s.execute();
			resultado = true;
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return resultado;

	}

	public boolean bajaPelicula(String consulta, Pelicula pelicula) {
		PreparedStatement s = null;
		boolean resultado = false;
		try {
			s = conexion.prepareStatement(consulta);
			s.setInt(1, pelicula.getCod_pelicula());
			s.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return resultado;
	}

	public boolean modificarPelicula(String consulta, Pelicula pelicula) {
		PreparedStatement s = null;
		boolean resultado = false;
		try {
			s = conexion.prepareStatement(consulta);
			s.setInt(13, pelicula.getCod_pelicula());
			s.setString(1, pelicula.getTitulo());
			s.setString(2, pelicula.getTitulo_alterno());
			s.setInt(3, pelicula.getDuracion());
			s.setInt(4, pelicula.getAnio());
			s.setString(5, pelicula.getDirector());
			s.setString(6, pelicula.getActores_principales());
			s.setString(7, pelicula.getSinopsis());
			s.setString(8, pelicula.getGenero());
			s.setBytes(9, pelicula.getFoto());
			s.setString(10, pelicula.getFormato());
			s.setInt(11, pelicula.getCantidad());
			s.setDouble(12, pelicula.getPrecio());

			s.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return resultado;
	}

	public boolean modificarPeliculanotImage(String consulta, Pelicula pelicula) {
		PreparedStatement s = null;
		boolean resultado = false;
		try {
			s = conexion.prepareStatement(consulta);
			s.setInt(10, pelicula.getCod_pelicula());
			s.setString(1, pelicula.getTitulo());
			s.setString(2, pelicula.getTitulo_alterno());
			s.setInt(3, pelicula.getDuracion());
			s.setInt(4, pelicula.getAnio());
			s.setString(5, pelicula.getDirector());
			s.setString(6, pelicula.getActores_principales());
			s.setString(7, pelicula.getSinopsis());
			s.setString(8, pelicula.getGenero());
			s.setString(9, pelicula.getFormato());
			s.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return resultado;
	}

	public boolean altaMulta(String consulta, Multa multa) {
		PreparedStatement s = null;
		boolean resultado = false;

		try {
			s = conexion.prepareStatement(consulta);
			
			s.setInt(1, multa.getNro_socio());
			s.setInt(2, multa.getTipo_multa());
			s.setDate(3, Date.valueOf(multa.getFecha_multa()));
			s.setBoolean(4, multa.getPagado());
			s.execute();

		} catch (Exception e) {

		}

		return resultado;
	}

	public boolean modificarMulta(String consulta, Multa multa) {
		PreparedStatement s = null;
		boolean resultado = false;
		try {
			Integer cod_multa = multa.getCod_multa();
			Integer nro_socio = multa.getNro_socio();
			Integer tipo_multa = multa.getTipo_multa();
			Date fecha_multa = Date.valueOf(multa.getFecha_multa());
			Boolean pagado = multa.getPagado();
			
			s = conexion.prepareStatement(consulta);

			
			s.setInt(1, nro_socio);
			s.setInt(2, tipo_multa);
			s.setDate(3, fecha_multa);
			s.setBoolean(4, pagado);
			s.setInt(5, cod_multa);
			
			
			s.execute();
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return resultado;

	}
	
	public boolean altaAlquiler(String consulta, Alquiler alquiler) {
		PreparedStatement s = null;
		boolean resultado = false;

		try {
			s = conexion.prepareStatement(consulta);
			s.setInt(1, alquiler.getCod_pelicula());
			s.setInt(2, alquiler.getNro_socio());
			s.setDate(3, Date.valueOf(alquiler.getFecha_alquiler()));
			s.setDate(4, Date.valueOf(alquiler.getFecha_entrega()));
			//s.setFloat(7, alquiler.getMonto());
			s.execute();

		} catch (Exception e) {

		}

		return resultado;
	}

	public boolean modificarAlquiler(String consulta, Alquiler alquiler) {
		PreparedStatement s = null;
		boolean resultado = false;
		try {
			Integer cod_alquiler = alquiler.getCod_alquiler();
			Integer cod_pelicula = alquiler.getCod_pelicula();
			String titulo = alquiler.getTitulo();
			Integer nro_socio = alquiler.getNro_socio();
			Date fecha_alquiler = Date.valueOf(alquiler.getFecha_alquiler());
			Date fecha_entrega = Date.valueOf(alquiler.getFecha_entrega());
			Float monto = alquiler.getMonto();
			s = conexion.prepareStatement(consulta);

			s.setInt(1, cod_alquiler);
			s.setInt(2, cod_pelicula);
			s.setString(3, titulo);
			s.setInt(4, nro_socio);
			s.setDate(5, fecha_alquiler);
			s.setDate(6, fecha_entrega);
			s.setFloat(7, monto);
			resultado = true;
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return resultado;

	}
	
	public ResultSet obtenerActores(String consulta) {
		ResultSet rs = null;
		try {

			Statement s = conexion.createStatement();
			rs = s.executeQuery(consulta);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;

	}

	public ResultSet obtenerSinopsis(String consulta) {
		ResultSet rs = null;
		try {

			Statement s = conexion.createStatement();
			rs = s.executeQuery(consulta);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;

	}
	public boolean usuarioAlta(String consulta, Usuario usuario) {
		PreparedStatement s = null;
		boolean resultado = false;
		try {
			// Se crea un Statement, para realizar la consulta

			// Se realiza la consulta parametrizada
			String nombre = usuario.getNombre();
			String apellido = usuario.getApellido();
			String email = usuario.getEmail();
			String contrasena = usuario.getContrasena();
			Date fecha = Date.valueOf(usuario.getFec_nac());

			// String consulta="insert into clientes (razonSocial, cuit, domicilio,
			// telefono)" + " values (?,?,?,?)";
			// System.out.println(consulta);
			s = conexion.prepareStatement(consulta);
			s.setInt(1, usuario.getId_usuario());
			s.setString(2, nombre);
			s.setString(3, apellido);
			s.setString(4, email);
			s.setString(5, contrasena);
			s.setDate(6, fecha);

			s.execute();
			resultado = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}
	
	public boolean usuarioModificar(String consulta, Usuario usuario) {
		PreparedStatement s = null;
		boolean resultado = false;
		try {

			s = conexion.prepareStatement(consulta);

			s.setString(1, usuario.getNombre());
			s.setString(2, usuario.getApellido());
			s.setString(3, usuario.getEmail());
			s.setString(4, usuario.getContrasena());
			s.setDate(5, Date.valueOf(usuario.getFec_nac()));
			s.setInt(6, usuario.getId_usuario());

			s.execute();
			//resultado = true;
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return resultado;
	}

	public ResultSet damePrecio(String consulta) {
		ResultSet rs = null;
		try {
			// Se crea un Statement, para realizar la consulta
			Statement s = conexion.createStatement();

			// Se realiza la consulta. Los resultados se guardan en el
			// ResultSet rs
			rs = s.executeQuery(consulta);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;		
	}
	
	public ResultSet obtenerCamposPelicula(String consulta) {
		ResultSet rs = null;
		try {
			Statement s = conexion.createStatement();
			rs = s.executeQuery(consulta);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;		
	}
	public boolean updateCantidadPelicula(String consulta) {
		PreparedStatement s = null;
		boolean resultado = false;
		try {

			s = conexion.prepareStatement(consulta);

			s.execute();
			//resultado = true;
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return resultado;
	}
	public ResultSet dameLista(String consulta) {
		ResultSet rs = null;
		try {
			// Se crea un Statement, para realizar la consulta
			Statement s = conexion.createStatement();

			// Se realiza la consulta. Los resultados se guardan en el
			// ResultSet rs
			rs = s.executeQuery(consulta);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	public void cierraConexion() {
		try {
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



}
