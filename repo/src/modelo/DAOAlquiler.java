package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAOAlquiler {

	public boolean agregarAlquiler(Alquiler alquiler) {
		String consulta = "INSERT INTO public.alquiler(cod_pelicula, nro_socio, fecha_alquiler, fecha_entrega)" + 
				"VALUES (?,?,?,?);";

		return BaseDeDatos.getInstance().altaAlquiler(consulta, alquiler);
	}

	public boolean modificarAlquiler(Alquiler alquiler) {

		String consulta = "UPDATE public.alquiler"
				+ "	SET cod_pelicula=?, titulo=?, nro_socio=?, fecha_alquiler=?, fecha_entrega=?"
				+ "	WHERE cod_alquiler=?,cod_pelicula=?,nro_socio=?";
		return BaseDeDatos.getInstance().modificarAlquiler(consulta, alquiler);
	}

	public ArrayList<Alquiler> obtenerTodosLosAlquileresArray() {
		ArrayList<Alquiler> coleccion = new ArrayList<Alquiler>();
		String consulta = "SELECT p.cod_pelicula, p.titulo, s.nro_socio, a.fecha_alquiler, a.fecha_entrega, p.precio FROM public.alquiler a\r\n" + 
				"INNER JOIN public.socio s ON s.nro_socio = a.nro_socio\r\n" + 
				"INNER JOIN public.pelicula p ON p.cod_pelicula = a.cod_pelicula\r\n" + 
				"ORDER BY a.cod_alquiler ASC ";
		ResultSet rs = BaseDeDatos.getInstance().dameLista(consulta);
		try {
			while (rs.next()) {
				Alquiler a = new Alquiler();
				
				//a.setCod_alquiler(rs.getInt("cod_alquiler"));
				a.setCod_pelicula(rs.getInt("cod_pelicula"));
				a.setTitulo(rs.getString("titulo"));
				a.setNro_socio(rs.getInt("nro_socio"));
				a.setFecha_alquiler(rs.getDate("fecha_alquiler").toLocalDate());
				a.setFecha_entrega(rs.getDate("fecha_entrega").toLocalDate());
				a.setMonto(rs.getFloat("precio"));
				coleccion.add(a);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return coleccion;
	}
	
}
