package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAOMulta {

	public Multa buscarSocioMulta(String nroSocio) {
		Multa m = new Multa();
		String consulta = "SELECT * FROM public.multa   where nro_socio="+nroSocio+  " and pagado=false";
		ResultSet rs = BaseDeDatos.getInstance().dameLista(consulta);
		try {
			while (rs.next()) {
				
				m.setCod_multa(rs.getInt("cod_multa"));
				m.setNro_socio(rs.getInt("nro_socio"));
				m.setTipo_multa(rs.getInt("tipo_multa"));
				m.setFecha_multa(rs.getDate("fecha_multa").toLocalDate());
				m.setPagado(rs.getBoolean("pagado"));
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return m;
	} 
	
	public boolean agregarMulta(Multa multa) {
		String consulta = "INSERT INTO public.multa (nro_socio, tipo_multa, fecha_multa, pagado)" + 
				"	VALUES (?, ?, ?, ?);";
		
		return BaseDeDatos.getInstance().altaMulta(consulta, multa);
	}

	public boolean modificarMulta(Multa multa) {

		String consulta = "UPDATE public.multa SET nro_socio=?, tipo_multa=?, fecha_multa=?, pagado=? WHERE cod_multa=?";
		
		return BaseDeDatos.getInstance().modificarMulta(consulta, multa);
	}

	public ArrayList<Multa> obtenerTodasLasMultasArray() {
		ArrayList<Multa> coleccion = new ArrayList<Multa>();
		String consulta = "SELECT m.cod_multa,m.nro_socio, s.apellido_socio, s.nombre_socio, m.tipo_multa, dt.detalle, m.fecha_multa, m.pagado, dt.costo FROM public.multa m " + 
				"INNER JOIN public.socio s ON s.nro_socio = m.nro_socio\r\n" + 
				"INNER JOIN public.detalle_multa dt ON dt.id_tipo_multa = m.tipo_multa\r\n" + 
				"ORDER BY cod_multa ASC ";
		ResultSet rs = BaseDeDatos.getInstance().dameLista(consulta);
		try {
			while (rs.next()) {
				Multa s = new Multa();
				
				s.setCod_multa(rs.getInt("cod_multa"));
				s.setNro_socio(rs.getInt("nro_socio"));
				s.setApellido(rs.getString("apellido_socio"));
				s.setNombre(rs.getString("nombre_socio"));
				s.setTipo_multa(rs.getInt("tipo_multa"));
				s.setDetalle(rs.getString("detalle"));
				s.setFecha_multa(rs.getDate("fecha_multa").toLocalDate());
				s.setPagado(rs.getBoolean("pagado"));
				s.setCosto(rs.getFloat("Costo"));
				coleccion.add(s);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return coleccion;
	}
	
	
	/*
	 * public boolean eliminarPersona(Socio socio) {
		String consulta = "DELETE FROM public.socio" + "	WHERE nro_socio=?;";
		return BaseDeDatos.getInstance().bajaSocio(consulta, socio);
	}
	 */
	
	
	
}
