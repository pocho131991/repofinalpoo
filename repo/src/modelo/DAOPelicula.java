package modelo;

import java.util.ArrayList;

public interface DAOPelicula {
	Boolean agregarPelicula(Pelicula pelicula);
	Boolean eliminarPelicula(Pelicula pelicula);
	Boolean modificarPelicula(Pelicula pelicula);
	Double obtenerPrecio(Integer codigo);
	Pelicula obtenerCamposPelicula(Integer codigo);
	String obtenerSinopsis(Integer codigo);
	String obtenerActores(Integer codigo);
	Boolean modificarPeliculanotImage(Pelicula pelicula);
	ArrayList<Pelicula> buscarPeliculas();
	
}
