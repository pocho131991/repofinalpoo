package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class DAOPeliculasImpl implements DAOPelicula {

	public boolean cantidadPeliculamenos(Integer codigo) {
		String consulta = "UPDATE public.pelicula SET cantidad=((select cantidad from public.pelicula where cod_pelicula="
				+ codigo + ")-1) WHERE cod_pelicula=" + codigo;

		return BaseDeDatos.getInstance().updateCantidadPelicula(consulta);
	}
	public boolean cantidadPeliculamas(Integer codigo) {
		String consulta = "UPDATE public.pelicula SET cantidad=((select cantidad from public.pelicula where cod_pelicula="
				+ codigo + ")+1) WHERE cod_pelicula=" + codigo;

		return BaseDeDatos.getInstance().updateCantidadPelicula(consulta);
	}
	@Override
	public String obtenerActores(Integer codigo) {
		String actores = "";
		String consulta = "Select actores_principales from public.pelicula where cod_pelicula=" + codigo;
		ResultSet rs = BaseDeDatos.getInstance().obtenerActores(consulta);
		try {
			while (rs.next()) {
				actores = actores + rs.getString("actores_principales");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return actores;
	}

	@Override
	public String obtenerSinopsis(Integer codigo) {
		String sinopsis = "";
		String consulta = "Select sinopsis from public.pelicula where cod_pelicula=" + codigo;
		ResultSet rs = BaseDeDatos.getInstance().obtenerSinopsis(consulta);
		try {
			while (rs.next()) {
				sinopsis = sinopsis + rs.getString("sinopsis");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sinopsis;
	}

	@Override
	public Double obtenerPrecio(Integer codigo) {
		Double valor = 0.0;
		String consulta = "Select precio from public.pelicula where cod_pelicula=" + codigo;
		ResultSet rs = BaseDeDatos.getInstance().damePrecio(consulta);

		try {
			while (rs.next()) {
				valor = rs.getDouble("precio");
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return valor;
	}

	@Override
	public Pelicula obtenerCamposPelicula(Integer codigo) {
		Pelicula p = new Pelicula();
		System.out.println("hola");
		String consulta = "SELECT * FROM public.pelicula where cod_pelicula=" + codigo;
		ResultSet rs = BaseDeDatos.getInstance().dameLista(consulta);
		try {
			while (rs.next()) {

				p.setCod_pelicula(rs.getInt("cod_pelicula"));
				p.setTitulo(rs.getString("titulo"));
				p.setTitulo_alterno(rs.getString("titulo_alterno"));
				p.setDuracion(rs.getInt("duracion"));
				p.setAnio(rs.getInt("anio"));
				p.setDirector(rs.getString("director"));
				p.setActores_principales(rs.getString("actores_principales"));
				p.setGenero(rs.getString("genero"));
				p.setFoto(rs.getBytes("imagen"));
				p.setFormato(rs.getString("formato"));
				p.setSinopsis(rs.getString("sinopsis"));
				p.setCantidad(rs.getInt("cantidad"));
				p.setPrecio(rs.getDouble("precio"));

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return p;

	}

	@Override
	public ArrayList<Pelicula> buscarPeliculas() {
		ArrayList<Pelicula> coleccion = new ArrayList<Pelicula>();
		String consulta = "SELECT * FROM public.pelicula ORDER BY cod_pelicula ASC ";
		ResultSet rs = BaseDeDatos.getInstance().dameLista(consulta);
		try {
			while (rs.next()) {
				Pelicula pelicula = new Pelicula();
				pelicula.setCod_pelicula(rs.getInt("cod_pelicula"));
				pelicula.setTitulo(rs.getString("titulo"));
				pelicula.setTitulo_alterno(rs.getString("titulo_alterno"));
				pelicula.setDuracion(rs.getInt("duracion"));
				pelicula.setAnio(rs.getInt("anio"));
				pelicula.setDirector(rs.getString("director"));
				pelicula.setActores_principales(rs.getString("actores_principales"));
				pelicula.setGenero(rs.getString("genero"));
				pelicula.setFoto(rs.getBytes("imagen"));
				pelicula.setFormato(rs.getString("formato"));
				pelicula.setSinopsis(rs.getString("sinopsis"));
				pelicula.setCantidad(rs.getInt("cantidad"));
				pelicula.setPrecio(rs.getDouble("precio"));
				coleccion.add(pelicula);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return coleccion;
	}

	@Override
	public Boolean agregarPelicula(Pelicula pelicula) {
		String consulta = "INSERT INTO public.pelicula(cod_pelicula, titulo, titulo_alterno, duracion, anio, director, actores_principales, sinopsis, genero, imagen,formato,cantidad,precio)"
				+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		return BaseDeDatos.getInstance().altaPelicula(consulta, pelicula);
	}

	public Integer obtenerCodigosPeli() {
		ArrayList<Integer> codigos = new ArrayList<Integer>();

		for (Pelicula pelicula : this.buscarPeliculas()) {
			codigos.add(pelicula.getCod_pelicula());

		}

		return Collections.max(codigos);
	}

	public byte[] obtenerImagen(Integer cod) {

		byte[] bi = null;

		for (Pelicula pelicula : this.buscarPeliculas()) {
			if (pelicula.getCod_pelicula().equals(cod)) {
				bi = pelicula.getFoto();
			}

		}

		return bi;
	}

	public String obtenerSinopsisP(Integer cod) {
		String sinopsis = "";
		for (Pelicula pelicula : this.buscarPeliculas()) {
			if (pelicula.getCod_pelicula().equals(cod)) {
				sinopsis = sinopsis + pelicula.getSinopsis();
			}
		}
		return sinopsis;
	}

	@Override
	public Boolean eliminarPelicula(Pelicula pelicula) {
		String consulta = "DELETE FROM public.pelicula" + "	WHERE cod_pelicula= ?";
		return BaseDeDatos.getInstance().bajaPelicula(consulta, pelicula);
	}

	@Override
	public Boolean modificarPelicula(Pelicula pelicula) {
		String consulta = "UPDATE public.pelicula"
				+ " SET titulo=?, titulo_alterno=?, duracion=?, anio=?, director=?, actores_principales=?, sinopsis=?, genero=?, imagen=?,formato=?,cantidad=?,precio=?"
				+ "		WHERE cod_pelicula=?";
		return BaseDeDatos.getInstance().modificarPelicula(consulta, pelicula);
	}

	@Override
	public Boolean modificarPeliculanotImage(Pelicula pelicula) {
		String consulta = "UPDATE public.pelicula"
				+ " SET titulo=?, titulo_alterno=?, duracion=?, anio=?, director=?, actores_principales=?, sinopsis=?, genero=? ,formato=?,precio=?"
				+ "		WHERE cod_pelicula=?";
		return BaseDeDatos.getInstance().modificarPeliculanotImage(consulta, pelicula);
	}

}
