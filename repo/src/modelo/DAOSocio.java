package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class DAOSocio {
	public boolean agregarPersona(Socio socio) {
		String consulta = "INSERT INTO public.socio(nro_socio, dni_socio, apellido_socio, nombre_socio, direccion_socio, telefono_socio, fecha_nacimiento)" + 
				"	VALUES (?, ?, ?, ?, ?, ?, ?);";

		return BaseDeDatos.getInstance().altaSocio(consulta, socio);
	}

	public boolean modificarPersona(Socio socio) {

		String consulta = "UPDATE public.socio"
				+ "	SET dni_socio=?, apellido_socio=?, nombre_socio=?, direccion_socio=?, telefono_socio=?, fecha_nacimiento=?"
				+ "	WHERE nro_socio=?";
		return BaseDeDatos.getInstance().modificarSocio(consulta, socio);
	}

	public boolean eliminarPersona(Socio socio) {
		String consulta = "DELETE FROM public.socio" + "	WHERE nro_socio=?;";
		return BaseDeDatos.getInstance().bajaSocio(consulta, socio);
	}
	public Socio buscarSocio(String nroSocio) {
		Socio s = new Socio();
		String consulta = "SELECT * FROM public.socio WHERE nro_socio="+ nroSocio;
		ResultSet rs = BaseDeDatos.getInstance().dameLista(consulta);
		try {
			while (rs.next()) {
				s.setNro_socio(rs.getInt("nro_socio"));
				s.setDni_socio(rs.getInt("dni_socio"));
				s.setApellido(rs.getString("apellido_socio"));
				s.setNombre(rs.getString("nombre_socio"));
				s.setDireccion(rs.getString("direccion_socio"));
				s.setTelefono(rs.getInt("telefono_socio"));
				s.setFecha_nac(rs.getDate("fecha_nacimiento").toLocalDate());
				

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception_VideoClub e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;

	}
	

	public ArrayList<Socio> obtenerTodosLosClientesArray() {
		ArrayList<Socio> coleccion = new ArrayList<Socio>();
		String consulta = "SELECT * FROM public.socio ORDER BY nro_socio ASC ";
		ResultSet rs = BaseDeDatos.getInstance().dameLista(consulta);
		try {
			while (rs.next()) {
				Socio s = new Socio();
				s.setNro_socio(rs.getInt("nro_socio"));
				s.setDni_socio(rs.getInt("dni_socio"));
				s.setApellido(rs.getString("apellido_socio"));
				s.setNombre(rs.getString("nombre_socio"));
				s.setDireccion(rs.getString("direccion_socio"));
				s.setTelefono(rs.getInt("telefono_socio"));
				s.setFecha_nac(rs.getDate("fecha_nacimiento").toLocalDate());
				coleccion.add(s);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception_VideoClub e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return coleccion;

	}

	public Integer obtenerUltimoNroSocio() {
		ArrayList<Integer> codigos = new ArrayList<Integer>();
		for (Socio socio : this.obtenerTodosLosClientesArray()) {
			codigos.add(socio.getNro_socio());

		}
		return Collections.max(codigos);

	}


}
