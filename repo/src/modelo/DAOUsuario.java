package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class DAOUsuario {

	public DAOUsuario() {
		// TODO Auto-generated constructor stub
	}

	public boolean agregarPersona(Usuario u) {
		String consulta = "INSERT INTO public.usuario(id_usuario,nombre_usuario, apellido_usuario, email, contrasenia, fecha_nacimiento)"
				+ "	VALUES (?, ?, ?, ?, ?,?)";
		return BaseDeDatos.getInstance().usuarioAlta(consulta, u);

	}
	public boolean eliminarUsuario(Integer codigo) {
		String consulta = "DELETE FROM public.usuario WHERE id_usuario="+codigo;
		return BaseDeDatos.getInstance().bajaUsuario(consulta);
	}

	public boolean modificarUsuario(Usuario u) {
		String consulta ="UPDATE public.usuario SET nombre_usuario=?, apellido_usuario=?, email=?, contrasenia=?, fecha_nacimiento=?" + 
							"	WHERE id_usuario=?";
		
		return BaseDeDatos.getInstance().usuarioModificar(consulta, u);
	}
	
	public ArrayList<Usuario> obtenerTodosLosUsuariosArray() {
		ArrayList<Usuario> coleccion = new ArrayList<>();
		String consulta = "select * from usuario";
		ResultSet rs = BaseDeDatos.getInstance().dameLista(consulta);

		try {
			while (rs.next()) {
				Usuario usuario = new Usuario();
				usuario.setId_usuario(rs.getInt("id_usuario"));
				usuario.setNombre(rs.getString("nombre_usuario"));
				usuario.setApellido(rs.getString("apellido_usuario"));
				usuario.setEmail(rs.getString("email"));
				usuario.setContrasena(rs.getString("contrasenia"));
				usuario.setFec_nac(rs.getDate("fecha_nacimiento").toLocalDate());
				coleccion.add(usuario);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception_VideoClub e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return coleccion;
	}

	public Integer obtenerUltimoIdUs() {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Usuario u : this.obtenerTodosLosUsuariosArray()) {
			ids.add(u.getId_usuario());
		}

		return Collections.max(ids);
	}

}
