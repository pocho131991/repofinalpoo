package modelo;

import java.time.LocalDate;

public class Multa {
	
	private Integer cod_multa;
	private Integer nro_socio;
	private String nombre;
	private String apellido;
	private Integer tipo_multa;
	private String detalle;
	private LocalDate fecha_multa;
	private Boolean pagado;
	private Float costo;
	
	public Multa() {

	}
	
	public Multa(Integer cod_multa, Integer nro_socio, String apellido, String nombre, Integer tipo_multa, String detalle, 
			LocalDate fecha_multa, Boolean pagado, Float costo) {
		super();
		this.setCod_multa(cod_multa);
		this.setNro_socio(nro_socio);
		this.setApellido(apellido);
		this.setNombre(nombre);
		this.setTipo_multa(tipo_multa);
		this.setDetalle(detalle);
		this.setFecha_multa(fecha_multa);
		this.setPagado(pagado);
		this.setCosto(costo);
	}



	//Getters & Setters
	public Integer getCod_multa() {
		return cod_multa;
	}
	public void setCod_multa(Integer cod_multa) {
		this.cod_multa = cod_multa;
	}
	public Integer getNro_socio() {
		return nro_socio;
	}
	public void setNro_socio(Integer nro_socio) {
		this.nro_socio = nro_socio;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Integer getTipo_multa() {
		return tipo_multa;
	}
	public void setTipo_multa(Integer tipo_multa) {
		this.tipo_multa = tipo_multa;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public LocalDate getFecha_multa() {
		return fecha_multa;
	}
	public void setFecha_multa(LocalDate fecha_multa) {
		this.fecha_multa = fecha_multa;
	}
	public Boolean getPagado() {
		return pagado;
	}
	public void setPagado(Boolean pagado) {
		this.pagado = pagado;
	}
	public Float getCosto() {
		return costo;
	}
	public void setCosto(Float costo) {
		this.costo = costo;
	}
	
}
