package modelo;

public class Pelicula {

	private Integer cod_pelicula;
	private Integer anio;
	private Integer duracion;
	private String titulo;
	private String titulo_alterno;
	private String director;
	private String actores_principales;
	private String sinopsis;
	private String genero;
	private byte[] foto;
	private String formato;
	private Integer cantidad;
	private Double precio;

	public Integer getCod_pelicula() {
		return cod_pelicula;
	}

	public void setCod_pelicula(Integer cod_pelicula) {
		this.cod_pelicula = cod_pelicula;
	}

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public Integer getDuracion() {
		return duracion;
	}

	public void setDuracion(Integer duracion) {
		this.duracion = duracion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) throws Exception_VideoClub {
		if (!titulo.equals("")) {
			this.titulo = titulo;
		} else {
			throw new Exception_VideoClub("Ingres� el titulo de la pelicula");
		}

	}

	public String getTitulo_alterno() {
		return titulo_alterno;
	}

	public void setTitulo_alterno(String titulo_alterno) throws Exception_VideoClub {
		if (!titulo_alterno.equals("")) {
			this.titulo_alterno = titulo_alterno;
		} else {
			throw new Exception_VideoClub("Ingres� el titulo latino de la pelicula");
		}

	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) throws Exception_VideoClub {
		if (!director.equals("")) {
			this.director = director;
		} else {
			throw new Exception_VideoClub("Ingres� el director de la pelicula");
		}

	}

	public String getActores_principales() {
		return actores_principales;
	}

	public void setActores_principales(String actores_principales) throws Exception_VideoClub {
		if (!actores_principales.equals("")) {
			this.actores_principales = actores_principales;
		} else {
			throw new Exception_VideoClub("Ingres� actor/es principal/es de la pelicula");
		}

	}


	public String getSinopsis() {
		return sinopsis;
	}

	public void setSinopsis(String sinopsis) throws Exception_VideoClub {
		if (!sinopsis.equals("")) {
			this.sinopsis = sinopsis;
		} else {
			throw new Exception_VideoClub("Ingres� la sinopsis de la pelicula");
		}
		
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) throws Exception_VideoClub {
		if (!genero.equals("")) {
			this.genero = genero;
		} else {
			throw new Exception_VideoClub("Ingres� genero/s de la pelicula");
		}
		
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) throws Exception_VideoClub {
		if (!formato.equals("")) {
			this.formato = formato;
		} else {
			throw new Exception_VideoClub("Ingrese formato de la pelicula");
		}

	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

}
