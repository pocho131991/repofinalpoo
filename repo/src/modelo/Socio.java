package modelo;

import java.time.LocalDate;

public class Socio {

	private Integer nro_socio;
	private Integer dni_socio;
	private String nombre;
	private String apellido;
	private String direccion;
	private Integer telefono;
	private LocalDate fecha_nac;

	public Socio() {

	}

	public Socio(Integer dni_socio, String nombre, String apellido, String direccion, Integer telefono,
			LocalDate fecha_nac) throws Exception_VideoClub {

		this.setDni_socio(dni_socio);
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setDireccion(direccion);
		this.setTelefono(telefono);
		this.setFecha_nac(fecha_nac);

	}

	public Integer getDni_socio() {
		return dni_socio;
	}

	public void setDni_socio(Integer dni_socio) throws Exception_VideoClub {

		String dS = dni_socio.toString();
		Boolean flag = true;
		for (int i = 0; i < dS.length(); i++) {
			Character car = dS.charAt(i);
			if (!Character.isDigit(car)) {
				flag = false;
			}
		}

		if (!dS.equals("")) {
			if (dS.length() >= 7 && dS.length() <= 8 && flag) {
				this.dni_socio = dni_socio;
			} else {
				throw new Exception_VideoClub("Ha ingresado un DNI incorrecto. Ingrese nuevamente el DNI");
			}
		} else {

			throw new Exception_VideoClub("Ingrese nuevamente el DNI");
		}

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) throws Exception_VideoClub {
		Boolean flag = true;
		for (int i = 0; i < nombre.length(); i++) {
			Character car = nombre.charAt(i);
			if (Character.isDigit(car)) {
				flag = false;
			}
		}
		if (!nombre.equals("")) {
			if (flag) {
				this.nombre = nombre;
			} else {
				throw new Exception_VideoClub("Ingrese solo caracteres alfab�ticos");
			}

		} else {
			throw new Exception_VideoClub("Ingres� el nombre del socio");
		}

	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) throws Exception_VideoClub {
		Boolean flag = true;
		for (int i = 0; i < apellido.length(); i++) {
			Character car = apellido.charAt(i);
			if (Character.isDigit(car)) {
				flag = false;
			}
		}
		if (!apellido.equals("")) {
			if (flag) {
				this.apellido = apellido;
			} else {
				throw new Exception_VideoClub("Ingrese solo caracteres alfab�ticos");
			}

		} else {
			throw new Exception_VideoClub("Ingres� el apellido del socio");
		}

	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) throws Exception_VideoClub {
		if (!direccion.equals("")) {
			this.direccion = direccion;
		} else {
			throw new Exception_VideoClub("No se ingres� la direccion del socio");
		}

	}

	public Integer getTelefono() {
		return telefono;
	}

	public void setTelefono(Integer telefono) throws Exception_VideoClub {
		String dS = telefono.toString();
		if (!dS.equals("")) {
			this.telefono = telefono;
		} else {
			throw new Exception_VideoClub("No se ingres� el n�mero telefonico del socio");
		}

	}

	public LocalDate getFecha_nac() {
		return fecha_nac;
	}

	public void setFecha_nac(LocalDate fecha_nac) {
		this.fecha_nac = fecha_nac;
	}

	public Integer getNro_socio() {
		return nro_socio;
	}

	public void setNro_socio(Integer nro_socio) {
		this.nro_socio = nro_socio;
	}

	@Override
	public String toString() {
		return "Socio [getDni_socio()=" + getDni_socio() + ", getNombre()=" + getNombre() + ", getApellido()="
				+ getApellido() + ", getDireccion()=" + getDireccion() + ", getTelefono()=" + getTelefono()
				+ ", getFecha_nac()=" + getFecha_nac() + ", getNro_socio()=" + getNro_socio() + "] \n";
	}

}
