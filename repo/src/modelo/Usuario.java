package modelo;

import java.time.LocalDate;

public class Usuario {

	private String nombre;
	private String apellido;
	private String email;
	private String contrasena;
	private LocalDate fec_nac;
	private Integer id_usuario;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) throws Exception_VideoClub {

		Boolean flag = false;
		for (int i = 0; i < nombre.length(); i++) {
			Character car = nombre.charAt(i);
			if (Character.isDigit(car)) {
				flag = true;
			}
		}

		if (nombre.equals("")) {
			throw new Exception_VideoClub("Ingres� su nombre");
		} else {
			if (flag==false) {
				this.nombre = nombre;
			} else {
				throw new Exception_VideoClub("Ingres� caracteres alfabeticos");
			}

		}

	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) throws Exception_VideoClub {

		Boolean flag = false;
		for (int i = 0; i < apellido.length(); i++) {
			Character car = apellido.charAt(i);
			if (Character.isDigit(car)) {
				flag = true;
			}
		}

		if (apellido.equals("")) {
			throw new Exception_VideoClub("Ingres� su apellido");
		} else {
			if (flag==false) {
				this.apellido = apellido;
			} else {
				throw new Exception_VideoClub("Ingres� caracteres alfabeticos");
			}

		}

	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) throws Exception_VideoClub {

		if (email.equals("")) {
			throw new Exception_VideoClub("Ingres� su correo electronico");
		} else {
			this.email = email;
		}
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) throws Exception_VideoClub {
		if (contrasena.equals("")) {
			throw new Exception_VideoClub("Ingres� su contrase�a");
		} else {
			this.contrasena = contrasena;
		}
		
	}

	public LocalDate getFec_nac() {
		return fec_nac;
	}

	public void setFec_nac(LocalDate fec_nac) {
		this.fec_nac = fec_nac;
	}

	public Integer getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}

	@Override
	public String toString() {
		return "Usuario [nombre=" + nombre + ", apellido=" + apellido + ", email=" + email + ", contrasena="
				+ contrasena + ", fec_nac=" + fec_nac + ", id_usuario=" + id_usuario + "]";
	}

	
}
