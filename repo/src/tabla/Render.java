package tabla;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class Render extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 1L;
private String tipo;
	
	public Render(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		   Color colorFondo = null;
	        Color colorFondoPorDefecto=new Color( 192, 192, 192);
	        Color colorFondoSeleccion=new Color( 140, 140 , 140);
		if (value instanceof JLabel) {
			JLabel lbl = (JLabel) value;
			return lbl;
		}
		if (isSelected) {
			this.setBackground(new Color(192, 192, 192));
		} else {
			// Para las que no est�n seleccionadas se pinta el fondo de las celdas de blanco
			this.setBackground(Color.white);
		}
		if (tipo.equals("texto")) {
			// si es tipo texto define el color de fondo del texto y de la celda as� como la
			// alineaci�n
			if (hasFocus) {
				colorFondo = colorFondoSeleccion;
			} else {
				colorFondo = colorFondoPorDefecto;
			}
			this.setHorizontalAlignment(JLabel.LEFT);
			this.setText((String) value);
			// this.setForeground( (selected)? new Color(255,255,255) :new Color(0,0,0) );
			// this.setForeground( (selected)? new Color(255,255,255) :new Color(32,117,32)
			// );
			this.setBackground((isSelected) ? colorFondo : Color.WHITE);
			//this.setFont(normal);
			//this.setFont(bold);
			return this;
		}
		  if( tipo.equals("numerico"))
	        {           
	         if (hasFocus) {
	        colorFondo=colorFondoSeleccion;
	       }else{
	        colorFondo=colorFondoPorDefecto;
	       }
	         // System.out.println(value);
	            this.setHorizontalAlignment( JLabel.CENTER );
	            this.setText( (String) value );            
	            this.setForeground( (isSelected)? new Color(255,255,255) :new Color(32,117,32) );    
	            this.setBackground( (isSelected)? colorFondo :Color.WHITE);
	           // this.setBackground( (selected)? colorFondo :Color.MAGENTA);
	            //this.setFont(Bold);            
	            return this;   
	        }
		  return this;
		//return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	}
}
