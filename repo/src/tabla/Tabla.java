package tabla;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import modelo.Alquiler;
import modelo.DAOAlquiler;
import modelo.DAOMulta;
import modelo.DAOPeliculasImpl;
import modelo.DAOSocio;
import modelo.DAOUsuario;
import modelo.Multa;
import modelo.Pelicula;
import modelo.Socio;
import modelo.Usuario;

public class Tabla {

	private DefaultTableModel dtm_socio = new DefaultTableModel();
	private DefaultTableModel dtm_peliculas = new DefaultTableModel();
	private DefaultTableModel dtm_multas = new DefaultTableModel();
	private DefaultTableModel dtm_alquiler = new DefaultTableModel();

	public void visualizar_TablaUsuario(JTable tabla) {
		DAOUsuario daou = new DAOUsuario();
		TablaModelo tabm = new TablaModelo();
		String[] columnas = { "ID", "Nombre", "Apellido", "Email", "Contrase�a", "Fecha nacimiento" };
		tabm.setColumnIdentifiers(columnas);
		for (Usuario u : daou.obtenerTodosLosUsuariosArray()) {
			Object fila[] = { u.getId_usuario(), u.getNombre(), u.getApellido(), u.getEmail(), u.getContrasena(),
					u.getFec_nac().toString() };
			tabm.addRow(fila);
		}

		tabla.setModel(tabm);
		JTableHeader jtableHeader = tabla.getTableHeader();
		jtableHeader.setDefaultRenderer(new PersonalizarEncabezado());
		tabla.setTableHeader(jtableHeader);
		tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tabla.doLayout();

		tabla.getColumnModel().getColumn(0).setPreferredWidth(80);

		tabla.getColumnModel().getColumn(1).setPreferredWidth(120);

		tabla.getColumnModel().getColumn(2).setPreferredWidth(120);

		tabla.getColumnModel().getColumn(3).setPreferredWidth(150);
		tabla.getColumnModel().getColumn(4).setPreferredWidth(80);

		tabla.getColumnModel().getColumn(5).setPreferredWidth(100);
	}

	public void visualizar_TablaMultas(JTable tabla) {
		DAOMulta daom = new DAOMulta();
		dtm_multas = new TablaModelo();

		/*
		 * String[] columnas = {"Nro Socio", "Apellido", "Nombre", "Tipo Multa",
		 * "Detalle", "Fecha Multa", "Pagado", "Costo"};
		 * tabm.setColumnIdentifiers(columnas); for (Multa m :
		 * daom.obtenerTodasLasMultasArray()) {
		 * 
		 * Object fila[] = {m.getNro_socio(), m.getApellido(), m.getNombre(),
		 * m.getTipo_multa(), m.getDetalle(), m.getFecha_multa(), m.getPagado(),
		 * m.getCosto()}; tabm.addRow(fila); }
		 * 
		 * tabla.setModel(tabm); JTableHeader jtableHeader = tabla.getTableHeader();
		 * jtableHeader.setDefaultRenderer(new PersonalizarEncabezado());
		 * tabla.setTableHeader(jtableHeader);
		 */

		Object fila[] = new Object[9];

		String[] columnas = { "Cod multa", "Nro Socio", "Apellido", "Nombre", "Tipo Multa", "Detalle", "Fecha Multa",
				"Pagado", "Costo" };
		dtm_multas.setColumnIdentifiers(columnas);

		for (Multa m : daom.obtenerTodasLasMultasArray()) {
			String bool = "";
			fila[0] = m.getCod_multa();
			fila[1] = m.getNro_socio();
			fila[2] = m.getApellido();
			fila[3] = m.getNombre();
			fila[4] = m.getTipo_multa();
			fila[5] = m.getDetalle();
			fila[6] = m.getFecha_multa();
			if (m.getPagado() == true) {
				bool = bool + "Pagado";
			} else {
				bool = bool + "No Pagado";
			}

			fila[7] = bool;
			fila[8] = m.getCosto();

			dtm_multas.addRow(fila);
		}

		tabla.setModel(dtm_multas);
		JTableHeader jtableHeader = tabla.getTableHeader();
		jtableHeader.setDefaultRenderer(new PersonalizarEncabezado());
		tabla.setTableHeader(jtableHeader);
		tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tabla.doLayout();
		tabla.setRowSelectionAllowed(true);
		// tabla.getColumnModel().getColumn(0).setCellRenderer(new Render("numerico"));
		tabla.getColumnModel().getColumn(0).setPreferredWidth(80);
		// tabla.getColumnModel().getColumn(1).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(1).setPreferredWidth(80);
		// tabla.getColumnModel().getColumn(2).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(2).setPreferredWidth(150);
		// tabla.getColumnModel().getColumn(3).setCellRenderer(new Render("numerico"));
		tabla.getColumnModel().getColumn(3).setPreferredWidth(150);
		// tabla.getColumnModel().getColumn(4).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(4).setPreferredWidth(80);
		// tabla.getColumnModel().getColumn(5).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(5).setPreferredWidth(150);
		// tabla.getColumnModel().getColumn(6).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(6).setPreferredWidth(90);
		// tabla.getColumnModel().getColumn(7).setCellRenderer(new Render("numerico"));
		tabla.getColumnModel().getColumn(7).setPreferredWidth(100);

	}

	public void visualizar_TablaAlquiler(JTable tabla) {
		DAOAlquiler daoa = new DAOAlquiler();
		dtm_alquiler = new TablaModelo();

		Object fila[] = new Object[6];
		String[] columnas = { "Cod Pelicula", "Titulo", "Nro Socio", "Fecha Alquiler", "Fecha Entrega", "Precio" };
		dtm_alquiler.setColumnIdentifiers(columnas);

		for (Alquiler a : daoa.obtenerTodosLosAlquileresArray()) {
			fila[0] = a.getCod_pelicula();
			fila[1] = a.getTitulo();
			fila[2] = a.getNro_socio();
			fila[3] = a.getFecha_alquiler();
			fila[4] = a.getFecha_entrega();
			fila[5] = a.getMonto();

			dtm_alquiler.addRow(fila);
		}

		tabla.setModel(dtm_alquiler);
		JTableHeader jtableHeader = tabla.getTableHeader();
		jtableHeader.setDefaultRenderer(new PersonalizarEncabezado());
		tabla.setTableHeader(jtableHeader);
	}

	public void visualizar_TablaReducidaPelicula(JTable tabla) {
		DAOPeliculasImpl daop = new DAOPeliculasImpl();
		dtm_peliculas = new TablaModelo();

		String[] columnas = { "Codigo", "Titulo", "Titulo latino", "A�o", "Genero" };
		dtm_peliculas.setColumnIdentifiers(columnas);

		for (Pelicula pelicula : daop.buscarPeliculas()) {
			Object fila[] = { pelicula.getCod_pelicula().toString(), pelicula.getTitulo(), pelicula.getTitulo_alterno(),
					pelicula.getAnio().toString(), pelicula.getGenero() };
			dtm_peliculas.addRow(fila);
		}

		tabla.setModel(dtm_peliculas);
		JTableHeader jtableHeader = tabla.getTableHeader();
		jtableHeader.setDefaultRenderer(new PersonalizarEncabezado());
		tabla.setTableHeader(jtableHeader);
		// tabla.getColumnModel().getColumn(0).setCellRenderer(new Render("numerico"));
		tabla.getColumnModel().getColumn(0).setPreferredWidth(80);
		// tabla.getColumnModel().getColumn(1).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(1).setPreferredWidth(250);
		// tabla.getColumnModel().getColumn(2).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(2).setPreferredWidth(250);
		// tabla.getColumnModel().getColumn(3).setCellRenderer(new Render("numerico"));
		tabla.getColumnModel().getColumn(3).setPreferredWidth(70);
		// tabla.getColumnModel().getColumn(4).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(4).setPreferredWidth(180);

	}

	public void visualizar_TablaPeliculaReducida(JTable tabla) {
		DAOPeliculasImpl daop = new DAOPeliculasImpl();
		dtm_peliculas = new TablaModelo();

		String[] columnas = { "Codigo", "Titulo", "Titulo latino", "A�o", "Genero", "Formato", "Cantidad", "Precio" };
		dtm_peliculas.setColumnIdentifiers(columnas);

		for (Pelicula pelicula : daop.buscarPeliculas()) {
			Object fila[] = { pelicula.getCod_pelicula().toString(), pelicula.getTitulo(), pelicula.getTitulo_alterno(),
					pelicula.getAnio().toString(), pelicula.getGenero(), pelicula.getFormato(),
					pelicula.getCantidad().toString(), pelicula.getPrecio().toString() };
			dtm_peliculas.addRow(fila);
		}

		tabla.setModel(dtm_peliculas);
		JTableHeader jtableHeader = tabla.getTableHeader();
		jtableHeader.setDefaultRenderer(new PersonalizarEncabezado());
		tabla.setTableHeader(jtableHeader);
		// tabla.getColumnModel().getColumn(0).setCellRenderer(new Render("numerico"));
		tabla.getColumnModel().getColumn(0).setPreferredWidth(80);
		// tabla.getColumnModel().getColumn(1).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(1).setPreferredWidth(300);
		// tabla.getColumnModel().getColumn(2).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(2).setPreferredWidth(300);
		// tabla.getColumnModel().getColumn(3).setCellRenderer(new Render("numerico"));
		tabla.getColumnModel().getColumn(3).setPreferredWidth(80);
		// tabla.getColumnModel().getColumn(4).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(4).setPreferredWidth(120);
		// tabla.getColumnModel().getColumn(5).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(5).setPreferredWidth(120);
		tabla.getColumnModel().getColumn(6).setPreferredWidth(80);
		tabla.getColumnModel().getColumn(7).setPreferredWidth(80);

	}

	public void visualizar_TablaSocio(JTable tabla) {
		DAOSocio daos = new DAOSocio();
		dtm_socio = new TablaModelo();
		Object fila[] = new Object[7];
		dtm_socio.addColumn("Nro socio");
		dtm_socio.addColumn("DNI");
		dtm_socio.addColumn("Apellido");
		dtm_socio.addColumn("Nombre");
		dtm_socio.addColumn("Direccion");
		dtm_socio.addColumn("Telefono");
		dtm_socio.addColumn("Fecha Nacimiento");
		for (Socio socio : daos.obtenerTodosLosClientesArray()) {
			fila[0] = socio.getNro_socio();
			fila[1] = socio.getDni_socio();
			fila[2] = socio.getApellido();
			fila[3] = socio.getNombre();
			fila[4] = socio.getDireccion();
			fila[5] = socio.getTelefono();
			fila[6] = socio.getFecha_nac().toString();
			dtm_socio.addRow(fila);
		}
		tabla.setModel(dtm_socio);
		JTableHeader jtableHeader = tabla.getTableHeader();
		jtableHeader.setDefaultRenderer(new PersonalizarEncabezado());
		tabla.setTableHeader(jtableHeader);
		tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tabla.doLayout();

		tabla.getColumnModel().getColumn(0).setPreferredWidth(80);
		tabla.getColumnModel().getColumn(1).setPreferredWidth(80);
		tabla.getColumnModel().getColumn(2).setPreferredWidth(120);
		tabla.getColumnModel().getColumn(3).setPreferredWidth(120);
		tabla.getColumnModel().getColumn(4).setPreferredWidth(100);
		tabla.getColumnModel().getColumn(5).setPreferredWidth(80);
		tabla.getColumnModel().getColumn(6).setPreferredWidth(120);

	}

	public TablaModelo visualizar_TablaSelec() {
		TablaModelo dtmSelec = new TablaModelo();
		dtmSelec.addColumn("COD");
		dtmSelec.addColumn("TITULO ");
		dtmSelec.addColumn("TITULO LATINO ");
		dtmSelec.addColumn("GENERO");
		dtmSelec.addColumn("CANTIDAD");

		return dtmSelec;

	}

	public void visualizar_TablaPeliculas(JTable tabla) {
		DAOPeliculasImpl daop = new DAOPeliculasImpl();

		dtm_peliculas = new TablaModelo();

		Object fila[] = new Object[10];
		String[] columnas = { "Codigo", "Titulo", "Titulo latino", "Duracion(min)", "A�o", "Director", "Genero",
				"Formato", "Cantidad", "Precio" };//
		dtm_peliculas.setColumnIdentifiers(columnas);

		for (Pelicula pelicula : daop.buscarPeliculas()) {
			fila[0] = pelicula.getCod_pelicula().toString();
			fila[1] = pelicula.getTitulo();
			fila[2] = pelicula.getTitulo_alterno();
			fila[3] = pelicula.getDuracion().toString();
			fila[4] = pelicula.getAnio().toString();
			fila[5] = pelicula.getDirector();
			fila[6] = pelicula.getGenero();
			fila[7] = pelicula.getFormato();
			fila[8] = pelicula.getCantidad();
			fila[9] = pelicula.getPrecio();
			dtm_peliculas.addRow(fila);
		}

		tabla.setModel(dtm_peliculas);
		JTableHeader jtableHeader = tabla.getTableHeader();
		jtableHeader.setDefaultRenderer(new PersonalizarEncabezado());
		tabla.setTableHeader(jtableHeader);
		tabla.getColumnModel().getColumn(0).setPreferredWidth(80);
		tabla.getColumnModel().getColumn(0).setCellRenderer(new Render("numerico"));
		tabla.getColumnModel().getColumn(1).setPreferredWidth(300);
		tabla.getColumnModel().getColumn(1).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(2).setPreferredWidth(300);
		tabla.getColumnModel().getColumn(2).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(3).setPreferredWidth(100);
		tabla.getColumnModel().getColumn(3).setCellRenderer(new Render("numerico"));
		tabla.getColumnModel().getColumn(4).setPreferredWidth(80);
		tabla.getColumnModel().getColumn(4).setCellRenderer(new Render("numerico"));
		tabla.getColumnModel().getColumn(5).setPreferredWidth(120);
		tabla.getColumnModel().getColumn(5).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(6).setPreferredWidth(400);
		tabla.getColumnModel().getColumn(6).setCellRenderer(new Render("texto"));
		tabla.getColumnModel().getColumn(7).setPreferredWidth(90);
		tabla.getColumnModel().getColumn(7).setCellRenderer(new Render("texto"));

	}

	public DefaultTableModel getDtm_socio() {
		return dtm_socio;
	}

	public void setDtm_socio(DefaultTableModel dtm_socio) {
		this.dtm_socio = dtm_socio;
	}

	public DefaultTableModel getDtm_peliculas() {
		return dtm_peliculas;
	}

	public void setDtm_peliculas(DefaultTableModel dtm_peliculas) {
		this.dtm_peliculas = dtm_peliculas;
	}
}
