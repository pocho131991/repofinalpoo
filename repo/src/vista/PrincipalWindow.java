package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controlador.ControladorPrincipalWindow;

public class PrincipalWindow extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panelInicioUsuario;
	private JPanel panelRegistro;
	private ControladorPrincipalWindow controlador;
	private JButton btnIniciarSesion;
	private JButton btnRegistrarse;
	private JTextField txtIngresoUsuario;
	private JLabel lblNombreDeUsuario;
	private JTextField txtNombreU;
	private JTextField txtApellidoU;
	private JTextField txtEmailU;
	private JLabel lblContrasea_1;
	private JPasswordField pswContra;
	private JPasswordField pswContraConf;
	private JPasswordField passIngresoUsuario;
	private JComboBox cmbMes;
	private JComboBox cmbAnio;
	private JComboBox cmbDia;
	private JLabel lblregistername;
	private JLabel lblregistersurnema;
	private JLabel lblregisteremail;
	private JLabel lblregisterpass;
	private JLabel lblregisterpassconf;
	private JLabel lblregisterdate;
	private JTextField txtvalidarpass;
	private JButton btnOk;
	private JButton btnVolver;
	private JButton btnConfirmar;
	private JLabel lblFondo = new JLabel();

	public PrincipalWindow(ControladorPrincipalWindow controlador) {
		this.setControlador(controlador);
		this.setLocationRelativeTo(null);
		this.setTitle("LOGIN");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(350, 150, 650, 400);
		getContentPane().setLayout(null);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		inicializarComponentes();
	}

	public void cambiarPanel() {

	}

	private void inicializarComponentes() {
		panelRegistro = new JPanel();
		panelRegistro.setBackground(Color.LIGHT_GRAY);
		panelRegistro.setBounds(10, 37, 614, 268);
		panelRegistro.setLayout(null);
		panelRegistro.setVisible(false);
		contentPane.add(panelRegistro);
		panelInicioUsuario = new JPanel();
		panelInicioUsuario.setBackground(Color.WHITE);
		panelInicioUsuario.setBounds(10, 37, 614, 268);
		panelInicioUsuario.setLayout(null);
		panelInicioUsuario.setVisible(true);
		contentPane.add(panelInicioUsuario);

		btnVolver = new JButton("VOLVER");
		btnVolver.setVisible(false);
		btnVolver.setBounds(10, 316, 170, 23);
		btnVolver.addActionListener(this.getControlador());

		JLabel lblTitulo = new JLabel("SYS-VIDEOCLUB");
		lblTitulo.setFont(new Font("Verdana", Font.PLAIN, 28));
		lblTitulo.setBounds(185, 3, 245, 35);
		contentPane.add(lblTitulo);
		contentPane.add(btnVolver);

		btnRegistrarse = new JButton("REGISTRARSE");
		btnRegistrarse.setBounds(454, 316, 170, 23);
		btnRegistrarse.addActionListener(this.getControlador());
		contentPane.add(btnRegistrarse);

		btnIniciarSesion = new JButton("INICIAR SESION");
		btnIniciarSesion.addActionListener(this.getControlador());
		btnIniciarSesion.setBounds(232, 316, 170, 23);
		contentPane.add(btnIniciarSesion);
		inicializarRegistro();
		inicializarLogin();

	}

	private void inicializarRegistro() {
		lblNombreDeUsuario = new JLabel("Nombre:");
		lblNombreDeUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreDeUsuario.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblNombreDeUsuario.setBounds(0, 30, 182, 28);
		panelRegistro.add(lblNombreDeUsuario);

		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setHorizontalAlignment(SwingConstants.CENTER);
		lblApellido.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblApellido.setBounds(0, 75, 182, 28);
		panelRegistro.add(lblApellido);

		txtNombreU = new JTextField();
		txtNombreU.setEnabled(false);
		txtNombreU.setBounds(192, 30, 169, 28);
		panelRegistro.add(txtNombreU);
		txtNombreU.setColumns(10);

		txtApellidoU = new JTextField();
		txtApellidoU.setEnabled(false);
		txtApellidoU.setColumns(10);
		txtApellidoU.setBounds(192, 75, 169, 28);
		panelRegistro.add(txtApellidoU);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmail.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblEmail.setBounds(0, 120, 182, 28);
		panelRegistro.add(lblEmail);

		JLabel lblFechaDeNacimiento = new JLabel("Fecha de nacimiento:");
		lblFechaDeNacimiento.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblFechaDeNacimiento.setBounds(371, 84, 182, 28);
		panelRegistro.add(lblFechaDeNacimiento);

		txtEmailU = new JTextField();
		txtEmailU.setEnabled(false);
		txtEmailU.setColumns(10);
		txtEmailU.setBounds(192, 120, 169, 28);
		panelRegistro.add(txtEmailU);

		lblContrasea_1 = new JLabel("Contrase\u00F1a:");
		lblContrasea_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblContrasea_1.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblContrasea_1.setBounds(0, 165, 182, 28);
		panelRegistro.add(lblContrasea_1);

		pswContra = new JPasswordField();
		pswContra.setEnabled(false);
		pswContra.setBounds(192, 165, 169, 28);
		panelRegistro.add(pswContra);

		JLabel lblContraseaConfirmar = new JLabel("Confirme contrase\u00F1a:");
		lblContraseaConfirmar.setHorizontalAlignment(SwingConstants.CENTER);
		lblContraseaConfirmar.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblContraseaConfirmar.setBounds(0, 212, 182, 28);
		panelRegistro.add(lblContraseaConfirmar);

		pswContraConf = new JPasswordField();
		pswContraConf.setEnabled(false);
		pswContraConf.setBounds(192, 212, 169, 28);
		panelRegistro.add(pswContraConf);

		cmbDia = new JComboBox();
		cmbDia.setEnabled(false);
		cmbDia.setBackground(SystemColor.textHighlightText);
		cmbDia.setModel(new DefaultComboBoxModel(new String[] { "Dia", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
				"27", "28", "29", "30", "31" }));
		cmbDia.setBounds(371, 123, 57, 28);
		panelRegistro.add(cmbDia);

		cmbMes = new JComboBox();
		cmbMes.setEnabled(false);
		cmbMes.setBackground(SystemColor.textHighlightText);
		cmbMes.setModel(new DefaultComboBoxModel(new String[] { "Mes", "Enero", "Febrero", "Marzo", "Abril", "Mayo",
				"Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
		cmbMes.setBounds(438, 123, 77, 28);
		panelRegistro.add(cmbMes);

		cmbAnio = new JComboBox();
		cmbAnio.setEnabled(false);
		cmbAnio.setBackground(SystemColor.textHighlightText);
		cmbAnio.setModel(new DefaultComboBoxModel(new String[] { "A\u00F1o", "1985", "1986", "1987", "1988", "1989",
				"1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002",
				"2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011" }));
		cmbAnio.setBounds(527, 123, 77, 28);
		panelRegistro.add(cmbAnio);

		lblregistername = new JLabel("");
		lblregistername.setForeground(Color.RED);
		lblregistername.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblregistername.setBounds(192, 59, 169, 14);
		panelRegistro.add(lblregistername);

		lblregistersurnema = new JLabel("");
		lblregistersurnema.setForeground(Color.RED);
		lblregistersurnema.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblregistersurnema.setBounds(192, 106, 169, 14);
		panelRegistro.add(lblregistersurnema);

		lblregisteremail = new JLabel("");
		lblregisteremail.setForeground(Color.RED);
		lblregisteremail.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblregisteremail.setBounds(192, 149, 169, 14);
		panelRegistro.add(lblregisteremail);

		lblregisterpass = new JLabel("");
		lblregisterpass.setForeground(Color.RED);
		lblregisterpass.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblregisterpass.setBounds(192, 197, 169, 14);
		panelRegistro.add(lblregisterpass);

		lblregisterpassconf = new JLabel("");
		lblregisterpassconf.setForeground(Color.RED);
		lblregisterpassconf.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblregisterpassconf.setBounds(192, 243, 169, 14);
		panelRegistro.add(lblregisterpassconf);

		lblregisterdate = new JLabel("");
		lblregisterdate.setForeground(Color.RED);
		lblregisterdate.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblregisterdate.setBounds(371, 155, 223, 14);
		panelRegistro.add(lblregisterdate);

		JLabel lblIngreseContraseaPara = new JLabel("Ingrese contrase\u00F1a para poder registrarse:");
		lblIngreseContraseaPara.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIngreseContraseaPara.setFont(new Font("Sitka Text", Font.BOLD, 11));
		lblIngreseContraseaPara.setBounds(10, 5, 253, 20);
		panelRegistro.add(lblIngreseContraseaPara);

		txtvalidarpass = new JTextField();
		txtvalidarpass.setBounds(273, 5, 130, 20);
		panelRegistro.add(txtvalidarpass);
		txtvalidarpass.setColumns(10);

		btnOk = new JButton("OK");
		btnOk.setBounds(413, 4, 89, 20);
		btnOk.addActionListener(this.getControlador());
		panelRegistro.add(btnOk);

		btnConfirmar = new JButton("CONFIRMAR");
		btnConfirmar.setEnabled(false);
		btnConfirmar.setBounds(460, 191, 116, 20);
		btnConfirmar.addActionListener(this.getControlador());
		panelRegistro.add(btnConfirmar);

	}

	private void inicializarLogin() {
		JLabel lblNombreU = new JLabel("Correo Electronico:");
		lblNombreU.setForeground(Color.BLACK);
		lblNombreU.setBackground(Color.BLUE);
		lblNombreU.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNombreU.setFont(new Font("Verdana", Font.PLAIN, 18));
		lblNombreU.setBounds(42, 30, 265, 41);
		panelInicioUsuario.add(lblNombreU);

		JLabel lblContrasea = new JLabel("Contrase\u00F1a:");
		lblContrasea.setForeground(Color.BLACK);
		lblContrasea.setBackground(Color.BLUE);
		lblContrasea.setHorizontalAlignment(SwingConstants.RIGHT);
		lblContrasea.setFont(new Font("Verdana", Font.PLAIN, 18));
		lblContrasea.setBounds(42, 118, 265, 41);
		panelInicioUsuario.add(lblContrasea);

		txtIngresoUsuario = new JTextField();
		txtIngresoUsuario.setBounds(317, 30, 287, 41);
		txtIngresoUsuario.setColumns(10);
		panelInicioUsuario.add(txtIngresoUsuario);

		passIngresoUsuario = new JPasswordField();
		passIngresoUsuario.setBounds(317, 118, 287, 41);
		panelInicioUsuario.add(passIngresoUsuario);

		JLabel lblFondo2 = new JLabel("New label");
		String ruta = System.getProperty("user.dir");

		Image preview = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Logo Sistema.jpg");
		lblFondo2.setBounds(10, 30, 116, 102);
		ImageIcon icon = new ImageIcon(
				preview.getScaledInstance(lblFondo2.getWidth(), lblFondo2.getHeight(), Image.SCALE_DEFAULT));
		lblFondo2.setIcon(icon);

		panelInicioUsuario.add(lblFondo2);

	}

	public ControladorPrincipalWindow getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPrincipalWindow controlador) {
		this.controlador = controlador;
	}

	public JPanel getPanelInicioUsuario() {
		return panelInicioUsuario;
	}

	public void setPanelInicioUsuario(JPanel panelInicioUsuario) {
		this.panelInicioUsuario = panelInicioUsuario;
	}

	public JButton getBtnIniciarSesion() {
		return btnIniciarSesion;
	}

	public void setBtnIniciarSesion(JButton btnIniciarSesion) {
		this.btnIniciarSesion = btnIniciarSesion;
	}

	public JButton getBtnRegistrarse() {
		return btnRegistrarse;
	}

	public void setBtnRegistrarse(JButton btnRegistrarse) {
		this.btnRegistrarse = btnRegistrarse;
	}

	public JPanel getPanelRegistro() {
		return panelRegistro;
	}

	public void setPanelRegistro(JPanel panelRegistro) {
		this.panelRegistro = panelRegistro;
	}

	public JTextField getTxtIngresoUsuario() {
		return txtIngresoUsuario;
	}

	public void setTxtIngresoUsuario(JTextField txtIngresoUsuario) {
		this.txtIngresoUsuario = txtIngresoUsuario;
	}

	public JPasswordField getPassIngresoUsuario() {
		return passIngresoUsuario;
	}

	public void setPassIngresoUsuario(JPasswordField passIngresoUsuario) {
		this.passIngresoUsuario = passIngresoUsuario;
	}

	public JTextField getTxtNombreU() {
		return txtNombreU;
	}

	public void setTxtNombreU(JTextField txtNombreU) {
		this.txtNombreU = txtNombreU;
	}

	public JTextField getTxtApellidoU() {
		return txtApellidoU;
	}

	public void setTxtApellidoU(JTextField txtApellidoU) {
		this.txtApellidoU = txtApellidoU;
	}

	public JTextField getTxtEmailU() {
		return txtEmailU;
	}

	public void setTxtEmailU(JTextField txtEmailU) {
		this.txtEmailU = txtEmailU;
	}

	public JPasswordField getPswContra() {
		return pswContra;
	}

	public void setPswContra(JPasswordField pswContra) {
		this.pswContra = pswContra;
	}

	public JPasswordField getPswContraConf() {
		return pswContraConf;
	}

	public void setPswContraConf(JPasswordField pswContraConf) {
		this.pswContraConf = pswContraConf;
	}

	public JComboBox getCmbMes() {
		return cmbMes;
	}

	public void setCmbMes(JComboBox cmbMes) {
		this.cmbMes = cmbMes;
	}

	public JComboBox getCmbAnio() {
		return cmbAnio;
	}

	public void setCmbAnio(JComboBox cmbAnio) {
		this.cmbAnio = cmbAnio;
	}

	public JComboBox getCmbDia() {
		return cmbDia;
	}

	public void setCmbDia(JComboBox cmbDia) {
		this.cmbDia = cmbDia;
	}

	public JLabel getLblregistername() {
		return lblregistername;
	}

	public void setLblregistername(JLabel lblregistername) {
		this.lblregistername = lblregistername;
	}

	public JLabel getLblregistersurnema() {
		return lblregistersurnema;
	}

	public void setLblregistersurnema(JLabel lblregistersurnema) {
		this.lblregistersurnema = lblregistersurnema;
	}

	public JLabel getLblregisteremail() {
		return lblregisteremail;
	}

	public void setLblregisteremail(JLabel lblregisteremail) {
		this.lblregisteremail = lblregisteremail;
	}

	public JLabel getLblregisterpass() {
		return lblregisterpass;
	}

	public void setLblregisterpass(JLabel lblregisterpass) {
		this.lblregisterpass = lblregisterpass;
	}

	public JLabel getLblregisterpassconf() {
		return lblregisterpassconf;
	}

	public void setLblregisterpassconf(JLabel lblregisterpassconf) {
		this.lblregisterpassconf = lblregisterpassconf;
	}

	public JLabel getLblregisterdate() {
		return lblregisterdate;
	}

	public void setLblregisterdate(JLabel lblregisterdate) {
		this.lblregisterdate = lblregisterdate;
	}

	public JTextField getTxtvalidarpass() {
		return txtvalidarpass;
	}

	public void setTxtvalidarpass(JTextField txtvalidarpass) {
		this.txtvalidarpass = txtvalidarpass;
	}

	public JButton getBtnOk() {
		return btnOk;
	}

	public void setBtnOk(JButton btnOk) {
		this.btnOk = btnOk;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public void setBtnVolver(JButton btnVolver) {
		this.btnVolver = btnVolver;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void setBtnConfirmar(JButton btnConfirmar) {
		this.btnConfirmar = btnConfirmar;
	}
}
