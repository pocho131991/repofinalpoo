package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.text.MaskFormatter;

import com.toedter.calendar.JDateChooser;

import controlador.Controlador1;
import tabla.Tabla;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Vista1 extends JFrame {

	private static final long serialVersionUID = 1L;
	private final String ruta = System.getProperty("user.dir");
	private JPanel contentPane;
	private JPanel peliculaWindow;
	private JPanel socioWindow;
	private JPanel alquilerWindow;
	private JPanel usuarioWindow;
	private JPanel multaWindow;
	private Controlador1 controlador;
	private MaskFormatter mascara;
	private JButton btnClientes;
	private JButton btnPeliculas;
	private JButton btnAlquiler;
	private JTable table;
	private JTable table_socio;
	private JButton btnModificarPelicula;
	private JButton btnEliminarPelicula;
	private JButton btnAgregarPelicula;
	private JTextField txtNumSoc;
	private JTextField txtDniSoc;
	private JTextField txt_nombre_socio;
	private JTextField txt_apellido_socio;
	private JTextField txt_direccion;
	private JTextField txt_tel;
	private JTextField txt_fecnac;
	private JButton btnAgregarSocio;
	private JButton btnModificarSocio;
	private JButton btnEliminarSocio;
	private JLabel lblBusqueda;
	private JTextField txtFiltro;
	private JButton btnBuscar;
	private JTextField txtFiltroPelicula;
	private JButton btnSinopsisDePelicula;
	private JTextField txtsocio;
	private JDateChooser dateChooser;
	private JTable tablaSocioAlquiler;
	private JButton btnCancelarAlquiler;
	private JButton btnAgregarAlquiler;
	private JTextField txtNomApeSocioAlquiler;
	private JLabel lblSocio;
	private JTextField txtDireccionSocioAlquiler;
	private JLabel lblDireccion_1;
	private JTextField txtTelefonoSocioAlquiler;
	private JLabel lblTelefono_1;
	private JTable tablaPeli;
	private JLabel lblPeliculasSeleccionadas;
	private JScrollPane scrollPane_2;
	private JTable tablaSeleccion;
	private JLabel lblPortada;
	private JButton btnActualizar;
	private JScrollPane scrollPane_3;
	private JTextArea txtASinopsis;
	private JLabel lblSinopsis;
	private JScrollPane scrollPane_4;
	private JLabel lblActores;
	private JTextArea txtAActores;
	private JButton btnUsuarios;
	private JButton btnMulta;
	private JTextField txtFiltroAlquilerSocio;
	private JLabel lblBuscarPelicula;
	private JTextField txtFiltroAlquilerPelicula;
	private JButton btnQuitarPeli;
	private JTable tablaMultas;
	private JTextField txtCod_multa;
	private JTextField txtTipo_multa;
	private JTextField txtFecha_multa;
	private JTextField txtCosto;
	private JLabel lblMulta_1;
	private JLabel lblMulta_3;
	private JLabel lblMulta_4;
	private JLabel lblMulta_6;
	private JLabel lblMulta_7;
	private JTable tablaAlquiler;
	private JTable tablaUsuario;
	private JTextField txtIDUsuario;
	private JLabel lblNewLabel;
	private JTextField txtNomUsuario;
	private JLabel lblNombre_1;
	private JLabel lblApellido_1;
	private JLabel lblEmail;
	private JTextField txtApeUsuario;
	private JTextField txtEmailUsuario;
	private JLabel lblContrasea;
	private JPasswordField passfieldContra;
	private JTextField txtFecNacUsuario;
	private JLabel lblExcepcionUsuarioNombre;
	private JButton btnModificarUsuario;
	private JButton btnEliminarUsuario;
	private JLabel lblExcepcionUsuarioApellido;
	private JLabel lblExcepcionUsuarioEmail;
	private JLabel lblExcepcionUsuarioContrasenia;
	private JLabel lblExcepcionUsuarioFecNac;
	private JButton btnBusqueda;
	private JButton btnMostrar;
	private JButton btnQuitar;
	private JTextField txtFecIniAlquiler;
	private JTextField txtCantipeliculas;
	private JButton btnListadoAlquiler;
	private JButton btnAlquilar;
	private JTextField txtPagar;
	private JTextField txtNroSocioMulta;
	private JComboBox cmbPagado;
	private JButton btnModificarMulta;
	private JButton btnAgregarMulta;
	private JLabel lblNewLabel_2;
	private JTextField textFechaDesde;


	public Vista1(Controlador1 controlador) {
		this.setControlador(controlador);
		setTitle("Vista Principal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1350, 750);
		this.setLocationRelativeTo(null);
		//this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(Color.white);
		setContentPane(contentPane);

//////////// BOTONES
		btnClientes = new JButton("Socio");
		btnClientes.addActionListener(this.getControlador());
		btnClientes.setBounds(10, 47, 154, 39);
		btnPeliculas = new JButton("Pelicula");
		btnPeliculas.addActionListener(this.getControlador());
		btnPeliculas.setBounds(174, 47, 154, 39);
		btnAlquiler = new JButton("Alquilar");
		btnAlquiler.addActionListener(this.getControlador());
		btnAlquiler.setBounds(338, 47, 154, 39);
		contentPane.add(btnClientes);
		contentPane.add(btnPeliculas);
		contentPane.add(btnAlquiler);
		btnMulta = new JButton("Multas/Sanciones");
		btnMulta.addActionListener(this.getControlador());
		btnMulta.setBounds(502, 47, 154, 39);
		contentPane.add(btnMulta);

		btnUsuarios = new JButton("Usuarios");
		btnUsuarios.addActionListener(this.getControlador());
		btnUsuarios.setBounds(666, 47, 154, 39);
		contentPane.add(btnUsuarios);
		
		multaWindow= inicializarMultaWindow();
		multaWindow.setBounds(10, 100, 1324, 605);
		multaWindow.setLayout(null);
		
		usuarioWindow = inicializarUsuarioWindow();
		usuarioWindow.setBounds(10, 100, 1324, 605);
		usuarioWindow.setLayout(null);

		peliculaWindow = inicializarPeliculasWindow();
		peliculaWindow.setBounds(10, 100, 1324, 605);
		peliculaWindow.setLayout(null);

		socioWindow = inicializarsocioWindow();
		socioWindow.setBounds(10, 100, 1324, 605);
		socioWindow.setLayout(null);

		alquilerWindow = inicializarAlquilerWindow();
		alquilerWindow.setBounds(10, 100, 1324, 605);
		alquilerWindow.setLayout(null);
		
		
		///para acceder al design de la vista comentar los paneles completos incluyendo los metodos que usen
		//o cambiar de lugares como se agregan los paneles...
		//getContentPane().add(multaWindow);
		getContentPane().add(alquilerWindow);
		//getContentPane().add(usuarioWindow);
		//getContentPane().add(socioWindow);
		//getContentPane().add(peliculaWindow);
		
		lblNewLabel_2 = new JLabel("BIENVENIDO");
		lblNewLabel_2.setForeground(Color.BLUE);
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setFont(new Font("Sitka Subheading", Font.PLAIN, 23));
		lblNewLabel_2.setBounds(261, 11, 385, 25);
		contentPane.add(lblNewLabel_2);
		
		
		socioWindow.setVisible(false);
		peliculaWindow.setVisible(false);
		alquilerWindow.setVisible(false);
		usuarioWindow.setVisible(false);
		multaWindow.setVisible(true);
		
		
	}

	public void cambiarPanelSocio() {
		socioWindow.setVisible(true);
		peliculaWindow.setVisible(false);
		alquilerWindow.setVisible(false);
		usuarioWindow.setVisible(false);
		multaWindow.setVisible(false);
	}

	public void cambiarPanelPelicula() {
		alquilerWindow.setVisible(false);
		socioWindow.setVisible(false);
		peliculaWindow.setVisible(true);
		usuarioWindow.setVisible(false);
		multaWindow.setVisible(false);
	}

	public void cambiarPanelAlquiler() {
		alquilerWindow.setVisible(true);
		socioWindow.setVisible(false);
		peliculaWindow.setVisible(false);
		usuarioWindow.setVisible(false);
		multaWindow.setVisible(false);
	}
	public void cambiarPanelUsuario() {
		alquilerWindow.setVisible(false);
		socioWindow.setVisible(false);
		peliculaWindow.setVisible(false);
		usuarioWindow.setVisible(true);
		multaWindow.setVisible(false);
	}

	public void cambiarPanelMulta() {
		alquilerWindow.setVisible(false);
		socioWindow.setVisible(false);
		peliculaWindow.setVisible(false);
		usuarioWindow.setVisible(false);
		multaWindow.setVisible(true);
	}
	public JPanel inicializarsocioWindow() {
		JPanel socioWindow = new JPanel();
		socioWindow.setBackground(Color.white);
		table_socio = new JTable();
		table_socio.addMouseListener(this.getControlador());
		table_socio.setRowHeight(60);
		table_socio.setRowSelectionAllowed(true);
		table_socio.setFont(new Font("Tahoma", Font.PLAIN, 16));
		table_socio.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		table_socio.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table_socio.doLayout();
		Tabla t = new Tabla();
		t.visualizar_TablaSocio(table_socio);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportView(table_socio);
		getContentPane().setLayout(null);
		scrollPane.setBounds(0, 50, 900, 550);
		socioWindow.add(scrollPane);

		txtNumSoc = new JTextField();
		txtNumSoc.setEnabled(false);
		txtNumSoc.setBounds(1092, 22, 222, 30);
		socioWindow.add(txtNumSoc);
		txtNumSoc.setColumns(10);

		JLabel lblCodigoSocio = new JLabel("NUMERO SOCIO:");
		lblCodigoSocio.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCodigoSocio.setBounds(874, 22, 214, 30);
		socioWindow.add(lblCodigoSocio);

		JLabel lblDni = new JLabel("DNI:");
		lblDni.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDni.setBounds(874, 63, 214, 30);
		socioWindow.add(lblDni);

		txtDniSoc = new JTextField();
		txtDniSoc.setColumns(10);
		txtDniSoc.setBounds(1092, 63, 222, 30);
		socioWindow.add(txtDniSoc);

		txt_nombre_socio = new JTextField();
		txt_nombre_socio.setColumns(10);
		txt_nombre_socio.setBounds(1092, 104, 222, 30);
		socioWindow.add(txt_nombre_socio);

		JLabel lblNombre = new JLabel("NOMBRE:");
		lblNombre.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNombre.setBounds(874, 104, 214, 30);
		socioWindow.add(lblNombre);

		JLabel lblApellido = new JLabel("APELLIDO:");
		lblApellido.setHorizontalAlignment(SwingConstants.RIGHT);
		lblApellido.setBounds(874, 145, 214, 30);
		socioWindow.add(lblApellido);

		JLabel lblDireccion = new JLabel("DIRECCION:");
		lblDireccion.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDireccion.setBounds(874, 186, 214, 30);
		socioWindow.add(lblDireccion);

		JLabel lblTelefono = new JLabel("TELEFONO:");
		lblTelefono.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTelefono.setBounds(874, 227, 214, 30);
		socioWindow.add(lblTelefono);

		txt_apellido_socio = new JTextField();
		txt_apellido_socio.setColumns(10);
		txt_apellido_socio.setBounds(1092, 145, 222, 30);
		socioWindow.add(txt_apellido_socio);

		txt_direccion = new JTextField();
		txt_direccion.setColumns(10);
		txt_direccion.setBounds(1092, 186, 222, 30);
		socioWindow.add(txt_direccion);

		txt_tel = new JTextField();
		txt_tel.setColumns(10);
		txt_tel.setBounds(1092, 227, 222, 30);
		socioWindow.add(txt_tel);

		JLabel lblFechaDeNacimiento = new JLabel("FECHA DE NACIMIENTO:");
		lblFechaDeNacimiento.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFechaDeNacimiento.setBounds(874, 268, 214, 30);
		socioWindow.add(lblFechaDeNacimiento);

		txt_fecnac = new JTextField();
		txt_fecnac.setColumns(10);
		txt_fecnac.setBounds(1092, 268, 222, 30);
		socioWindow.add(txt_fecnac);

		btnAgregarSocio = new JButton("Agregar Socio");
		btnAgregarSocio.setBounds(1092, 309, 222, 38);
		btnAgregarSocio.addActionListener(this.getControlador());
		socioWindow.add(btnAgregarSocio);

		btnModificarSocio = new JButton("Modificar Socio");
		btnModificarSocio.addActionListener(this.getControlador());
		btnModificarSocio.setBounds(1092, 358, 222, 38);
		socioWindow.add(btnModificarSocio);

		btnEliminarSocio = new JButton("Eliminar Socio");
		btnEliminarSocio.setBounds(1092, 407, 222, 38);
		btnEliminarSocio.addActionListener(this.getControlador());
		socioWindow.add(btnEliminarSocio);

		lblBusqueda = new JLabel("BUSQUEDA :");
		lblBusqueda.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBusqueda.setBounds(0, 0, 108, 30);
		socioWindow.add(lblBusqueda);

		txtFiltro = new JTextField();
		txtFiltro.addKeyListener(this.getControlador());
		txtFiltro.setBounds(118, 0, 256, 30);
		socioWindow.add(txtFiltro);
		txtFiltro.setColumns(10);

		btnBuscar = new JButton("BUSCAR");
		btnBuscar.setBounds(384, 0, 119, 26);
		btnBuscar.addActionListener(getControlador());
		socioWindow.add(btnBuscar);
		return socioWindow;
	}
	public JPanel inicializarMultaWindow() {
		JPanel mwindow = new JPanel();
		mwindow.setBackground(Color.WHITE);
		tablaMultas = new JTable();
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 50, 900, 321);
		mwindow.add(scrollPane);
		
		
		tablaMultas.addMouseListener(this.getControlador());
		tablaMultas.setRowHeight(60);
		tablaMultas.setRowSelectionAllowed(true);
		tablaMultas.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaMultas.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		tablaMultas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaMultas.doLayout();
		Tabla t= new Tabla();
		t.visualizar_TablaMultas(tablaMultas);
		scrollPane.setViewportView(tablaMultas);
		
		lblMulta_1 = new JLabel("CODIGO MULTA");
		lblMulta_1.setBounds(925, 50, 124, 30);
		mwindow.add(lblMulta_1);
		
		txtCod_multa = new JTextField();
		txtCod_multa.setEnabled(false);
		txtCod_multa.setBounds(925, 87, 184, 32);
		mwindow.add(txtCod_multa);
		txtCod_multa.setColumns(10);
		
		lblNewLabel = new JLabel("NRO SOCIO");
		lblNewLabel.setBounds(1130, 50, 124, 30);
		mwindow.add(lblNewLabel);
		
		lblMulta_3 = new JLabel("TIPO MULTA");
		lblMulta_3.setBounds(925, 130, 140, 30);
		mwindow.add(lblMulta_3);
		
		txtTipo_multa = new JTextField();
		txtTipo_multa.setEnabled(false);
		txtTipo_multa.setColumns(10);
		txtTipo_multa.setBounds(925, 171, 184, 32);
		mwindow.add(txtTipo_multa);
		
		lblMulta_4 = new JLabel("FECHA MULTA");
		lblMulta_4.setBounds(1130, 130, 124, 30);
		mwindow.add(lblMulta_4);
		
		txtFecha_multa = new JTextField();
		txtFecha_multa.setEnabled(false);
		txtFecha_multa.setColumns(10);
		txtFecha_multa.setBounds(1130, 171, 184, 32);
		mwindow.add(txtFecha_multa);
		
		lblMulta_6 = new JLabel("PAGADO");
		lblMulta_6.setBounds(1130, 214, 124, 30);
		mwindow.add(lblMulta_6);
		
		lblMulta_7= new JLabel("COSTO");
		lblMulta_7.setBounds(925, 214, 124, 30);
		mwindow.add(lblMulta_7);
		
		txtCosto = new JTextField();
		txtCosto.setEnabled(false);
		txtCosto.setColumns(10);
		txtCosto.setBounds(925, 255, 184, 32);
		mwindow.add(txtCosto);
		
		btnAgregarMulta = new JButton("Agregar Multa");
		btnAgregarMulta.addActionListener(getControlador());
		btnAgregarMulta.setBounds(925, 423, 192, 41);
		mwindow.add(btnAgregarMulta);
		
		btnModificarMulta = new JButton("Modificar Multa");
		btnModificarMulta.setBounds(1130, 423, 192, 41);
		btnModificarMulta.addActionListener(getControlador());
		mwindow.add(btnModificarMulta);
		
		txtNroSocioMulta = new JTextField();
		txtNroSocioMulta.setEnabled(false);
		txtNroSocioMulta.setBounds(1130, 87, 184, 32);
		mwindow.add(txtNroSocioMulta);
		txtNroSocioMulta.setColumns(10);
		
		cmbPagado = new JComboBox();
		cmbPagado.setEnabled(false);
		cmbPagado.setModel(new DefaultComboBoxModel(new String[] {"Pagado", "No Pagado"}));
		cmbPagado.setBounds(1130, 255, 184, 30);
		mwindow.add(cmbPagado);
		
		JLabel lblNewLabel_1 = new JLabel("Seleccione un fila haciendo doble click para poder editarla");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel_1.setBounds(0, 11, 490, 28);
		mwindow.add(lblNewLabel_1);
		
		return mwindow;
	}
	public JPanel inicializarUsuarioWindow() {
		JPanel uwindow = new JPanel();
		uwindow.setBackground(Color.WHITE);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 50, 900, 321);
		uwindow.add(scrollPane);
		
		tablaUsuario = new JTable();
		tablaUsuario.addMouseListener(this.getControlador());
		tablaUsuario.setRowHeight(60);
		tablaUsuario.setRowSelectionAllowed(true);
		tablaUsuario.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaUsuario.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		tablaUsuario.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaUsuario.doLayout();
		Tabla t= new Tabla();
		t.visualizar_TablaUsuario(tablaUsuario);
		scrollPane.setViewportView(tablaUsuario);
		
		txtIDUsuario = new JTextField();
		txtIDUsuario.setBounds(925, 87, 184, 32);
		uwindow.add(txtIDUsuario);
		txtIDUsuario.setColumns(10);
		
		lblNewLabel = new JLabel("ID USUARIO");
		lblNewLabel.setBounds(925, 50, 124, 30);
		uwindow.add(lblNewLabel);
		
		txtNomUsuario = new JTextField();
		txtNomUsuario.setColumns(10);
		txtNomUsuario.setBounds(1130, 87, 184, 32);
		uwindow.add(txtNomUsuario);
		
		lblNombre_1 = new JLabel("NOMBRE");
		lblNombre_1.setBounds(1130, 50, 124, 30);
		uwindow.add(lblNombre_1);
		
		lblApellido_1 = new JLabel("APELLIDO");
		lblApellido_1.setBounds(925, 130, 124, 30);
		uwindow.add(lblApellido_1);
		
		lblEmail = new JLabel("EMAIL");
		lblEmail.setBounds(1130, 130, 124, 30);
		uwindow.add(lblEmail);
		
		txtApeUsuario = new JTextField();
		txtApeUsuario.setColumns(10);
		txtApeUsuario.setBounds(925, 171, 184, 32);
		uwindow.add(txtApeUsuario);
		
		txtEmailUsuario = new JTextField();
		txtEmailUsuario.setColumns(10);
		txtEmailUsuario.setBounds(1130, 171, 184, 32);
		uwindow.add(txtEmailUsuario);
		
		lblContrasea = new JLabel("CONTRASE\u00D1A");
		lblContrasea.setBounds(925, 214, 124, 30);
		uwindow.add(lblContrasea);
		
		passfieldContra = new JPasswordField();
		passfieldContra.setBounds(925, 255, 184, 32);
		uwindow.add(passfieldContra);
		
		JLabel lblFechaDeNacimiento_1 = new JLabel("FECHA DE NACIMIENTO");
		lblFechaDeNacimiento_1.setBounds(1130, 214, 124, 30);
		uwindow.add(lblFechaDeNacimiento_1);
		
		txtFecNacUsuario = new JTextField();
		txtFecNacUsuario.setColumns(10);
		txtFecNacUsuario.setBounds(1130, 255, 184, 32);
		uwindow.add(txtFecNacUsuario);
		
		btnModificarUsuario = new JButton("Modificar Usuario");
		btnModificarUsuario.addActionListener(getControlador());
		btnModificarUsuario.setBounds(925, 436, 192, 41);
		uwindow.add(btnModificarUsuario);
		
		btnEliminarUsuario = new JButton("Eliminar Usuario");
		btnEliminarUsuario.addActionListener(getControlador());
		btnEliminarUsuario.setBounds(1122, 436, 192, 41);
		uwindow.add(btnEliminarUsuario);
		
		lblExcepcionUsuarioNombre = new JLabel("");
		lblExcepcionUsuarioNombre.setForeground(Color.RED);
		lblExcepcionUsuarioNombre.setBounds(925, 298, 363, 23);
		uwindow.add(lblExcepcionUsuarioNombre);
		
		lblExcepcionUsuarioApellido = new JLabel("");
		lblExcepcionUsuarioApellido.setForeground(Color.RED);
		lblExcepcionUsuarioApellido.setBounds(925, 322, 363, 23);
		uwindow.add(lblExcepcionUsuarioApellido);
		
		lblExcepcionUsuarioEmail = new JLabel("");
		lblExcepcionUsuarioEmail.setForeground(Color.RED);
		lblExcepcionUsuarioEmail.setBounds(925, 348, 363, 23);
		uwindow.add(lblExcepcionUsuarioEmail);
		
		lblExcepcionUsuarioContrasenia = new JLabel("");
		lblExcepcionUsuarioContrasenia.setForeground(Color.RED);
		lblExcepcionUsuarioContrasenia.setBounds(925, 374, 363, 23);
		uwindow.add(lblExcepcionUsuarioContrasenia);
		
		lblExcepcionUsuarioFecNac = new JLabel("");
		lblExcepcionUsuarioFecNac.setForeground(Color.RED);
		lblExcepcionUsuarioFecNac.setBounds(925, 400, 363, 23);
		uwindow.add(lblExcepcionUsuarioFecNac);
		return uwindow;
	}

	public JPanel inicializarAlquilerWindow() {
		
		JPanel awindow = new JPanel();
		awindow.setBackground(Color.WHITE);
		tablaAlquiler = new JTable();
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(567, 45, 706, 152);
		awindow.add(scrollPane);
		
		
		tablaAlquiler.addMouseListener(this.getControlador());
		tablaAlquiler.setRowHeight(60);
		tablaAlquiler.setRowSelectionAllowed(true);
		tablaAlquiler.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaAlquiler.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		tablaAlquiler.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaAlquiler.doLayout();
		Tabla t= new Tabla();
		t.visualizar_TablaAlquiler(tablaAlquiler);
		scrollPane.setViewportView(tablaAlquiler);
		
		btnBusqueda = new JButton("Busqueda");
		btnBusqueda.addActionListener(getControlador());
		btnBusqueda.setBounds(468, 121, 89, 23);
		awindow.add(btnBusqueda);
		
		btnMostrar = new JButton("Mostrar");
		btnMostrar.setEnabled(false);
		btnMostrar.setToolTipText("Se ingresara los datos que fueron seleccionados en la busqueda");
		btnMostrar.addActionListener(getControlador());
		btnMostrar.setBounds(468, 66, 89, 23);
		awindow.add(btnMostrar);
		

		txtsocio = new JTextField();
		txtsocio.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtsocio.setBounds(10, 60, 219, 31);
		awindow.add(txtsocio);
		txtsocio.setColumns(10);
		
		JLabel lblNumeroDeSocio = new JLabel("NUMERO DE SOCIO:");
		lblNumeroDeSocio.setBounds(10, 40, 219, 22);
		awindow.add(lblNumeroDeSocio);

		txtNomApeSocioAlquiler = new JTextField();
		txtNomApeSocioAlquiler.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtNomApeSocioAlquiler.setColumns(10);
		txtNomApeSocioAlquiler.setBounds(239, 60, 219, 31);
		awindow.add(txtNomApeSocioAlquiler);

		lblSocio = new JLabel("SOCIO:");
		lblSocio.setBounds(239, 40, 219, 22);
		awindow.add(lblSocio);
		
		txtDireccionSocioAlquiler = new JTextField();
		txtDireccionSocioAlquiler.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtDireccionSocioAlquiler.setColumns(10);
		txtDireccionSocioAlquiler.setBounds(10, 115, 219, 31);
		awindow.add(txtDireccionSocioAlquiler);

		lblDireccion_1 = new JLabel("DIRECCION:");
		lblDireccion_1.setBounds(10, 93, 219, 22);
		awindow.add(lblDireccion_1);

		txtTelefonoSocioAlquiler = new JTextField();
		txtTelefonoSocioAlquiler.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtTelefonoSocioAlquiler.setColumns(10);
		txtTelefonoSocioAlquiler.setBounds(239, 115, 219, 31);
		awindow.add(txtTelefonoSocioAlquiler);

		lblTelefono_1 = new JLabel("TELEFONO:");
		lblTelefono_1.setBounds(241, 93, 219, 22);
		awindow.add(lblTelefono_1);
		
		lblPeliculasSeleccionadas = new JLabel("PELICULA/S SELECCIONADA/S:");
		lblPeliculasSeleccionadas.setBounds(684, 208, 309, 22);
		awindow.add(lblPeliculasSeleccionadas);

		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(684, 232, 589, 115);
		awindow.add(scrollPane_2);

		tablaSeleccion = new JTable();
		tablaSeleccion.addMouseListener(getControlador());
		tablaSeleccion.setModel(t.visualizar_TablaSelec());
		scrollPane_2.setViewportView(tablaSeleccion);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 232, 620, 115);
		awindow.add(scrollPane_1);

		tablaPeli = new JTable();
		tablaPeli.addMouseListener(getControlador());
		t.visualizar_TablaPeliculaReducida(tablaPeli);
		tablaPeli.getColumnModel().getColumn(0).setPreferredWidth(120);
		tablaPeli.getColumnModel().getColumn(1).setPreferredWidth(450);
		tablaPeli.getColumnModel().getColumn(2).setPreferredWidth(450);
		tablaPeli.getColumnModel().getColumn(3).setPreferredWidth(80);
		tablaPeli.getColumnModel().getColumn(4).setPreferredWidth(150);
		tablaPeli.getColumnModel().getColumn(5).setPreferredWidth(150);
		tablaPeli.getColumnModel().getColumn(6).setPreferredWidth(80);
		tablaPeli.getColumnModel().getColumn(7).setPreferredWidth(80);
		
		scrollPane_1.setViewportView(tablaPeli);
		
		btnQuitar = new JButton("QUITAR PELICULA");
		btnQuitar.addActionListener(getControlador());
		btnQuitar.setBounds(1122, 208, 139, 23);
		awindow.add(btnQuitar);
		
		lblBuscarPelicula = new JLabel("BUSCAR PELICULA:");
		lblBuscarPelicula.setBounds(10, 177, 101, 22);
		awindow.add(lblBuscarPelicula);
		
		JLabel lblCodigoONombre = new JLabel("SELECCIONE LA/S PELICULA/S A ALQUILAR HACIENDO DOBLE CLICK");
		lblCodigoONombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblCodigoONombre.setBounds(10, 208, 385, 22);
		awindow.add(lblCodigoONombre);
		
		txtFiltroAlquilerPelicula = new JTextField();
		txtFiltroAlquilerPelicula.setColumns(10);
		txtFiltroAlquilerPelicula.addKeyListener(getControlador());
		txtFiltroAlquilerPelicula.setBounds(121, 177, 157, 20);
		awindow.add(txtFiltroAlquilerPelicula);
		
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd/MM/yyyy");
		dateChooser.setBounds(567, 358, 219, 31);
		awindow.add(dateChooser);

		try {
			mascara = new MaskFormatter("##/##/####");
			mascara.setPlaceholderCharacter('-');
		} catch (ParseException e) {

			e.printStackTrace();
		}

		JLabel lblFechaDeIngreso = new JLabel("FECHA DE PEDIDO DE ALQUILER:");
		lblFechaDeIngreso.setBounds(10, 358, 189, 31);
		awindow.add(lblFechaDeIngreso);

		JLabel lblFechaDeDevolucion = new JLabel("FECHA DE DEVOLUCION DE ALQUILER:");
		lblFechaDeDevolucion.setBounds(341, 358, 219, 31);
		awindow.add(lblFechaDeDevolucion);
		
		txtFecIniAlquiler = new JTextField();
		txtFecIniAlquiler.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		txtFecIniAlquiler.setBounds(209, 358, 122, 31);
		awindow.add(txtFecIniAlquiler);
		txtFecIniAlquiler.setColumns(10);
		//Para obtener la fecha actual
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		txtFecIniAlquiler.setText(dateFormat.format(date));
		
		txtCantipeliculas = new JTextField();
		txtCantipeliculas.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtCantipeliculas.setText("0");
		txtCantipeliculas.setBounds(1134, 358, 134, 31);
		awindow.add(txtCantipeliculas);
		txtCantipeliculas.setColumns(10);
		
		JLabel lblTotalPeliculas = new JLabel("TOTAL PELICULAS:");
		lblTotalPeliculas.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTotalPeliculas.setBounds(971, 358, 151, 31);
		awindow.add(lblTotalPeliculas);
		
		txtPagar = new JTextField();
		txtPagar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtPagar.setText("0.0");
		txtPagar.setColumns(10);
		txtPagar.setBounds(1134, 400, 134, 31);
		awindow.add(txtPagar);
		
		JLabel lblPagar = new JLabel("TOTAL A PAGAR:");
		lblPagar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPagar.setBounds(981, 400, 141, 31);
		awindow.add(lblPagar);
		
		btnListadoAlquiler = new JButton("VER LISTADO ALQUILER");
		btnListadoAlquiler.addActionListener(getControlador());
		btnListadoAlquiler.setBounds(1091, 495, 170, 23);
		awindow.add(btnListadoAlquiler);
		
		btnAlquilar = new JButton("ALQUILAR");
		btnAlquilar.addActionListener(getControlador());
		btnAlquilar.setBounds(1122, 453, 139, 23);
		awindow.add(btnAlquilar);
		
		textFechaDesde = new JTextField();
		textFechaDesde.setBounds(967, 496, 86, 20);
		awindow.add(textFechaDesde);
		textFechaDesde.setColumns(10);
		
		JLabel lblListarAlquileresDesde = new JLabel("Listar alquileres desde:");
		lblListarAlquileresDesde.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblListarAlquileresDesde.setBounds(811, 495, 134, 23);
		awindow.add(lblListarAlquileresDesde);
		return awindow;
		/*
		 * JPanel awindow = new JPanel();
		BufferedImage img = null;
		

		awindow.setBackground(Color.white);

		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd/MM/yyyy");
		dateChooser.setBounds(567, 358, 219, 31);
		awindow.add(dateChooser);

		txtsocio = new JTextField();
		txtsocio.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtsocio.setBounds(10, 60, 219, 31);
		awindow.add(txtsocio);
		txtsocio.setColumns(10);

		JLabel lblNumeroDeSocio = new JLabel("NUMERO DE SOCIO:");
		lblNumeroDeSocio.setBounds(10, 40, 219, 22);
		awindow.add(lblNumeroDeSocio);

		try {
			mascara = new MaskFormatter("##/##/####");
			mascara.setPlaceholderCharacter('-');
			formattedTextField = new JFormattedTextField(mascara);
			formattedTextField.setBounds(209, 358, 122, 31);
		} catch (ParseException e) {

			e.printStackTrace();
		}

		awindow.add(formattedTextField);

		JLabel lblFechaDeIngreso = new JLabel("FECHA DE PEDIDO DE ALQUILER:");
		lblFechaDeIngreso.setBounds(10, 358, 189, 31);
		awindow.add(lblFechaDeIngreso);

		JLabel lblFechaDeDevolucion = new JLabel("FECHA DE DEVOLUCION DE ALQUILER:");
		lblFechaDeDevolucion.setBounds(341, 358, 219, 31);
		awindow.add(lblFechaDeDevolucion);

		JLabel lblCodigoONombre = new JLabel("SELECCIONE LA/S PELICULA/S A ALQUILAR HACIENDO DOBLE CLICK");
		lblCodigoONombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblCodigoONombre.setBounds(10, 157, 385, 22);
		awindow.add(lblCodigoONombre);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(468, 29, 745, 123);
		awindow.add(scrollPane);

		tablaSocioAlquiler = new JTable();
		tablaSocioAlquiler.addMouseListener(this.getControlador());
		Tabla t = new Tabla();
		t.visualizar_TablaSocio(tablaSocioAlquiler);

		scrollPane.setViewportView(tablaSocioAlquiler);

		btnAgregarAlquiler = new JButton("");
		btnAgregarAlquiler.setBounds(10, 6, 189, 31);
		try {
			img = ImageIO.read(new File(ruta + "\\images\\" + "btnagregaralquiler.png"));
			ImageIcon imgi = new ImageIcon(
					img.getScaledInstance(btnAgregarAlquiler.getWidth(), btnAgregarAlquiler.getHeight(), 0));
			btnAgregarAlquiler.setIcon(imgi);
		} catch (IOException e) {
			e.printStackTrace();
		}

		awindow.add(btnAgregarAlquiler);

		btnCancelarAlquiler = new JButton("");
		btnCancelarAlquiler.setBounds(241, 6, 189, 31);
		try {
			img = ImageIO.read(new File(ruta + "\\images\\" + "btncancelaralquiler.png"));
			ImageIcon imgi = new ImageIcon(
					img.getScaledInstance(btnCancelarAlquiler.getWidth(), btnCancelarAlquiler.getHeight(), 0));
			btnCancelarAlquiler.setIcon(imgi);
		} catch (IOException e) {
			e.printStackTrace();
		}
		awindow.add(btnCancelarAlquiler);

		txtNomApeSocioAlquiler = new JTextField();
		txtNomApeSocioAlquiler.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtNomApeSocioAlquiler.setColumns(10);
		txtNomApeSocioAlquiler.setBounds(239, 60, 219, 31);
		awindow.add(txtNomApeSocioAlquiler);

		lblSocio = new JLabel("SOCIO:");
		lblSocio.setBounds(239, 40, 219, 22);
		awindow.add(lblSocio);

		txtDireccionSocioAlquiler = new JTextField();
		txtDireccionSocioAlquiler.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtDireccionSocioAlquiler.setColumns(10);
		txtDireccionSocioAlquiler.setBounds(10, 115, 219, 31);
		awindow.add(txtDireccionSocioAlquiler);

		lblDireccion_1 = new JLabel("DIRECCION:");
		lblDireccion_1.setBounds(10, 93, 219, 22);
		awindow.add(lblDireccion_1);

		txtTelefonoSocioAlquiler = new JTextField();
		txtTelefonoSocioAlquiler.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtTelefonoSocioAlquiler.setColumns(10);
		txtTelefonoSocioAlquiler.setBounds(239, 115, 219, 31);
		awindow.add(txtTelefonoSocioAlquiler);

		lblTelefono_1 = new JLabel("TELEFONO:");
		lblTelefono_1.setBounds(241, 93, 219, 22);
		awindow.add(lblTelefono_1);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 232, 589, 115);
		awindow.add(scrollPane_1);

		tablaPeli = new JTable();
		tablaPeli.addMouseListener(getControlador());
		t.visualizar_TablaPeliculaReducida(tablaPeli);
		scrollPane_1.setViewportView(tablaPeli);

		lblPeliculasSeleccionadas = new JLabel("PELICULA/S SELECCIONADA/S:");
		lblPeliculasSeleccionadas.setBounds(650, 157, 309, 22);
		awindow.add(lblPeliculasSeleccionadas);

		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(640, 232, 589, 115);
		awindow.add(scrollPane_2);

		tablaSeleccion = new JTable();
		tablaSeleccion.addMouseListener(getControlador());
		tablaSeleccion.setModel(t.visualizar_TablaSelec());
		scrollPane_2.setViewportView(tablaSeleccion);

		JLabel lblBuscarCliente = new JLabel("BUSCAR CLIENTE:");
		lblBuscarCliente.setBounds(468, 6, 101, 22);
		awindow.add(lblBuscarCliente);

		txtFiltroAlquilerSocio = new JTextField();
		txtFiltroAlquilerSocio.setBounds(579, 6, 157, 20);
		awindow.add(txtFiltroAlquilerSocio);
		txtFiltroAlquilerSocio.setColumns(10);

		lblBuscarPelicula = new JLabel("BUSCAR PELICULA:");
		lblBuscarPelicula.setBounds(10, 190, 101, 22);
		awindow.add(lblBuscarPelicula);

		txtFiltroAlquilerPelicula = new JTextField();
		txtFiltroAlquilerPelicula.setColumns(10);
		txtFiltroAlquilerPelicula.setBounds(121, 190, 157, 20);
		awindow.add(txtFiltroAlquilerPelicula);

		btnQuitarPeli = new JButton("QUITAR PELICULA");
		btnQuitarPeli.addMouseListener(getControlador());
		btnQuitarPeli.addActionListener(getControlador());
		btnQuitarPeli.setBounds(640, 190, 146, 23);
		awindow.add(btnQuitarPeli);
		return awindow;
		 */
		
	}

	

	public JPanel inicializarPeliculasWindow() {
		JPanel pwindow = new JPanel();
		pwindow.setBackground(UIManager.getColor("PasswordField.selectionBackground"));
		table = new JTable();
		table.addMouseListener(this.getControlador());
		table.setRowHeight(30);
		table.setRowSelectionAllowed(true);
		table.setFont(new Font("Tahoma", Font.PLAIN, 16));
		table.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.doLayout();

		Tabla t = new Tabla();
		t.visualizar_TablaPeliculas(table);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportView(table);
		getContentPane().setLayout(null);
		scrollPane.setBounds(0, 50, 1050, 286);
		pwindow.add(scrollPane);
		btnEliminarPelicula = new JButton("Eliminar pelicula");
		btnEliminarPelicula.setEnabled(false);
		btnEliminarPelicula.addActionListener(this.getControlador());
		btnEliminarPelicula.setBounds(887, 0, 160, 30);
		btnModificarPelicula = new JButton("Modificar pelicula");
		btnModificarPelicula.setEnabled(false);
		btnModificarPelicula.addActionListener(this.getControlador());
		contentPane.setLayout(null);
		btnModificarPelicula.setBounds(720, 0, 160, 30);

		pwindow.add(btnModificarPelicula);
		pwindow.add(btnEliminarPelicula);

		btnAgregarPelicula = new JButton("Agregar pelicula");
		btnAgregarPelicula.addActionListener(this.getControlador());
		btnAgregarPelicula.setBounds(543, 0, 160, 30);
		pwindow.add(btnAgregarPelicula);

		JLabel lblBusquedaPelicula = new JLabel("BUSQUEDA:");
		lblBusquedaPelicula.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBusquedaPelicula.setBounds(0, 0, 105, 30);
		pwindow.add(lblBusquedaPelicula);

		txtFiltroPelicula = new JTextField();
		txtFiltroPelicula.addKeyListener(this.getControlador());
		txtFiltroPelicula.setBounds(108, 0, 233, 30);
		pwindow.add(txtFiltroPelicula);
		txtFiltroPelicula.setColumns(10);

		BufferedImage img = null;
		try {
			String ruta = System.getProperty("user.dir");
			img = ImageIO.read(new File(ruta + "\\images\\portada-vacia.jpg"));
			// img =(BufferedImage) escalarImage(img, 566, 543);
		} catch (IOException e) {
			e.printStackTrace();
		}
		lblPortada = new JLabel(new ImageIcon(img));
		lblPortada.setHorizontalAlignment(SwingConstants.CENTER);
		lblPortada.setBounds(1078, 11, 236, 275);
		pwindow.add(lblPortada);

		

		btnActualizar = new JButton("Actualizar lista");
		btnActualizar.setBounds(373, 0, 160, 30);
		btnActualizar.addActionListener(getControlador());
		pwindow.add(btnActualizar);

		scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 389, 468, 177);
		pwindow.add(scrollPane_3);

		txtASinopsis = new JTextArea();
		txtASinopsis.setFont(new Font("Monospaced", Font.PLAIN, 18));
		scrollPane_3.setViewportView(txtASinopsis);
		txtASinopsis.setColumns(20);
		txtASinopsis.setRows(5);
		txtASinopsis.setWrapStyleWord(true);
		txtASinopsis.setLineWrap(true);

		lblSinopsis = new JLabel("SINOPSIS");
		lblSinopsis.setHorizontalAlignment(SwingConstants.CENTER);
		lblSinopsis.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblSinopsis.setBounds(75, 348, 341, 30);
		pwindow.add(lblSinopsis);

		scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(582, 389, 468, 177);
		pwindow.add(scrollPane_4);

		txtAActores = new JTextArea();
		txtAActores.setFont(new Font("Monospaced", Font.PLAIN, 18));
		txtAActores.setWrapStyleWord(true);
		txtAActores.setLineWrap(true);
		txtAActores.setRows(5);
		txtAActores.setColumns(20);
		scrollPane_4.setViewportView(txtAActores);

		lblActores = new JLabel("ACTORES");
		lblActores.setHorizontalAlignment(SwingConstants.CENTER);
		lblActores.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblActores.setBounds(630, 348, 341, 30);
		pwindow.add(lblActores);
		return pwindow;
	}



	//////////// GETTERS AND SETTERS
	public Controlador1 getControlador() {
		return controlador;
	}

	public void setControlador(Controlador1 controlador) {
		this.controlador = controlador;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JTable getTable_socio() {
		return table_socio;
	}

	public void setTable_socio(JTable table_socio) {
		this.table_socio = table_socio;
	}

	public JTextField getTxtNumSoc() {
		return txtNumSoc;
	}

	public void setTxtNumSoc(JTextField txtNumSoc) {
		this.txtNumSoc = txtNumSoc;
	}

	public JTextField getTxtDniSoc() {
		return txtDniSoc;
	}

	public void setTxtDniSoc(JTextField txtDniSoc) {
		this.txtDniSoc = txtDniSoc;
	}

	public JTextField getTxt_nombre_socio() {
		return txt_nombre_socio;
	}

	public void setTxt_nombre_socio(JTextField txt_nombre_socio) {
		this.txt_nombre_socio = txt_nombre_socio;
	}

	public JTextField getTxt_apellido_socio() {
		return txt_apellido_socio;
	}

	public void setTxt_apellido_socio(JTextField txt_apellido_socio) {
		this.txt_apellido_socio = txt_apellido_socio;
	}

	public JTextField getTxt_direccion() {
		return txt_direccion;
	}

	public void setTxt_direccion(JTextField txt_direccion) {
		this.txt_direccion = txt_direccion;
	}

	public JTextField getTxt_tel() {
		return txt_tel;
	}

	public void setTxt_tel(JTextField txt_tel) {
		this.txt_tel = txt_tel;
	}

	public JTextField getTxt_fecnac() {
		return txt_fecnac;
	}

	public void setTxt_fecnac(JTextField txt_fecnac) {
		this.txt_fecnac = txt_fecnac;
	}

	public JButton getBtnClientes() {
		return btnClientes;
	}

	public void setBtnClientes(JButton btnClientes) {
		this.btnClientes = btnClientes;
	}

	public JButton getBtnPeliculas() {
		return btnPeliculas;
	}

	public void setBtnPeliculas(JButton btnPeliculas) {
		this.btnPeliculas = btnPeliculas;
	}

	public JTextField getTxtFiltro() {
		return txtFiltro;
	}

	public void setTxtFiltro(JTextField txtFiltro) {
		this.txtFiltro = txtFiltro;
	}

	public JTable getTablaPeli() {
		return tablaPeli;
	}

	public void setTablaPeli(JTable tablaPeli) {
		this.tablaPeli = tablaPeli;
	}

	public JTable getTablaSeleccion() {
		return tablaSeleccion;
	}

	public void setTablaSeleccion(JTable tablaSeleccion) {
		this.tablaSeleccion = tablaSeleccion;
	}

	public JLabel getLblPortada() {
		return lblPortada;
	}

	public void setLblPortada(JLabel lblPortada) {
		this.lblPortada = lblPortada;
	}

	public JTextField getTxtFiltroPelicula() {
		return txtFiltroPelicula;
	}

	public void setTxtFiltroPelicula(JTextField txtFiltroPelicula) {
		this.txtFiltroPelicula = txtFiltroPelicula;
	}

	public JButton getBtnModificarPelicula() {
		return btnModificarPelicula;
	}

	public void setBtnModificarPelicula(JButton btnModificarPelicula) {
		this.btnModificarPelicula = btnModificarPelicula;
	}

	public JButton getBtnEliminarPelicula() {
		return btnEliminarPelicula;
	}

	public void setBtnEliminarPelicula(JButton btnEliminarPelicula) {
		this.btnEliminarPelicula = btnEliminarPelicula;
	}

	public JButton getBtnSinopsisDePelicula() {
		return btnSinopsisDePelicula;
	}

	public void setBtnSinopsisDePelicula(JButton btnSinopsisDePelicula) {
		this.btnSinopsisDePelicula = btnSinopsisDePelicula;
	}

	public JTable getTablaSocioAlquiler() {
		return tablaSocioAlquiler;
	}

	public void setTablaSocioAlquiler(JTable tablaSocioAlquiler) {
		this.tablaSocioAlquiler = tablaSocioAlquiler;
	}

	public JTextArea getTxtASinopsis() {
		return txtASinopsis;
	}

	public void setTxtASinopsis(JTextArea txtASinopsis) {
		this.txtASinopsis = txtASinopsis;
	}

	public JTextArea getTxtAActores() {
		return txtAActores;
	}

	public void setTxtAActores(JTextArea txtAActores) {
		this.txtAActores = txtAActores;
	}

	public JTextField getTxtsocio() {
		return txtsocio;
	}

	public void setTxtsocio(JTextField txtsocio) {
		this.txtsocio = txtsocio;
	}

	public JTextField getTxtNomApeSocioAlquiler() {
		return txtNomApeSocioAlquiler;
	}

	public void setTxtNomApeSocioAlquiler(JTextField txtNomApeSocioAlquiler) {
		this.txtNomApeSocioAlquiler = txtNomApeSocioAlquiler;
	}

	public JTextField getTxtDireccionSocioAlquiler() {
		return txtDireccionSocioAlquiler;
	}

	public void setTxtDireccionSocioAlquiler(JTextField txtDireccionSocioAlquiler) {
		this.txtDireccionSocioAlquiler = txtDireccionSocioAlquiler;
	}

	public JTextField getTxtTelefonoSocioAlquiler() {
		return txtTelefonoSocioAlquiler;
	}

	public void setTxtTelefonoSocioAlquiler(JTextField txtTelefonoSocioAlquiler) {
		this.txtTelefonoSocioAlquiler = txtTelefonoSocioAlquiler;
	}

	public JButton getBtnQuitarPeli() {
		return btnQuitarPeli;
	}

	public void setBtnQuitarPeli(JButton btnQuitarPeli) {
		this.btnQuitarPeli = btnQuitarPeli;
	}

	public JTextField getTxtIDUsuario() {
		return txtIDUsuario;
	}

	public void setTxtIDUsuario(JTextField txtIDUsuario) {
		this.txtIDUsuario = txtIDUsuario;
	}

	public JTextField getTxtNomUsuario() {
		return txtNomUsuario;
	}

	public void setTxtNomUsuario(JTextField txtNomUsuario) {
		this.txtNomUsuario = txtNomUsuario;
	}

	public JTextField getTxtApeUsuario() {
		return txtApeUsuario;
	}

	public void setTxtApeUsuario(JTextField txtApeUsuario) {
		this.txtApeUsuario = txtApeUsuario;
	}

	public JTextField getTxtEmailUsuario() {
		return txtEmailUsuario;
	}

	public void setTxtEmailUsuario(JTextField txtEmailUsuario) {
		this.txtEmailUsuario = txtEmailUsuario;
	}

	public JPasswordField getPassfieldContra() {
		return passfieldContra;
	}

	public void setPassfieldContra(JPasswordField passfieldContra) {
		this.passfieldContra = passfieldContra;
	}


	public JTextField getTxtFecNacUsuario() {
		return txtFecNacUsuario;
	}

	public void setTxtFecNacUsuario(JTextField txtFecNacUsuario) {
		this.txtFecNacUsuario = txtFecNacUsuario;
	}

	public JTable getTablaUsuario() {
		return tablaUsuario;
	}

	public void setTablaUsuario(JTable tablaUsuario) {
		this.tablaUsuario = tablaUsuario;
	}

	public JLabel getLblExcepcionUsuarioNombre() {
		return lblExcepcionUsuarioNombre;
	}

	public void setLblExcepcionUsuarioNombre(JLabel lblExcepcionUsuarioNombre) {
		this.lblExcepcionUsuarioNombre = lblExcepcionUsuarioNombre;
	}

	public JLabel getLblExcepcionUsuarioApellido() {
		return lblExcepcionUsuarioApellido;
	}

	public void setLblExcepcionUsuarioApellido(JLabel lblExcepcionUsuarioApellido) {
		this.lblExcepcionUsuarioApellido = lblExcepcionUsuarioApellido;
	}

	public JLabel getLblExcepcionUsuarioEmail() {
		return lblExcepcionUsuarioEmail;
	}

	public void setLblExcepcionUsuarioEmail(JLabel lblExcepcionUsuarioEmail) {
		this.lblExcepcionUsuarioEmail = lblExcepcionUsuarioEmail;
	}

	public JLabel getLblExcepcionUsuarioContrasenia() {
		return lblExcepcionUsuarioContrasenia;
	}

	public void setLblExcepcionUsuarioContrasenia(JLabel lblExcepcionUsuarioContrasenia) {
		this.lblExcepcionUsuarioContrasenia = lblExcepcionUsuarioContrasenia;
	}

	public JLabel getLblExcepcionUsuarioFecNac() {
		return lblExcepcionUsuarioFecNac;
	}

	public void setLblExcepcionUsuarioFecNac(JLabel lblExcepcionUsuarioFecNac) {
		this.lblExcepcionUsuarioFecNac = lblExcepcionUsuarioFecNac;
	}

	public JButton getBtnMostrar() {
		return btnMostrar;
	}

	public void setBtnMostrar(JButton btnMostrar) {
		this.btnMostrar = btnMostrar;
	}

	public JTextField getTxtFiltroAlquilerPelicula() {
		return txtFiltroAlquilerPelicula;
	}

	public void setTxtFiltroAlquilerPelicula(JTextField txtFiltroAlquilerPelicula) {
		this.txtFiltroAlquilerPelicula = txtFiltroAlquilerPelicula;
	}

	public JTextField getTxtCantipeliculas() {
		return txtCantipeliculas;
	}

	public void setTxtCantipeliculas(JTextField txtCantipeliculas) {
		this.txtCantipeliculas = txtCantipeliculas;
	}

	public JTextField getTxtPagar() {
		return txtPagar;
	}

	public void setTxtPagar(JTextField txtPagar) {
		this.txtPagar = txtPagar;
	}

	public JTable getTablaMultas() {
		return tablaMultas;
	}

	public void setTablaMultas(JTable tablaMultas) {
		this.tablaMultas = tablaMultas;
	}

	public JTextField getTxtCod_multa() {
		return txtCod_multa;
	}

	public void setTxtCod_multa(JTextField txtCod_multa) {
		this.txtCod_multa = txtCod_multa;
	}

	public JTextField getTxtTipo_multa() {
		return txtTipo_multa;
	}

	public void setTxtTipo_multa(JTextField txtTipo_multa) {
		this.txtTipo_multa = txtTipo_multa;
	}

	public JTextField getTxtFecha_multa() {
		return txtFecha_multa;
	}

	public void setTxtFecha_multa(JTextField txtFecha_multa) {
		this.txtFecha_multa = txtFecha_multa;
	}

	public JTextField getTxtCosto() {
		return txtCosto;
	}

	public void setTxtCosto(JTextField txtCosto) {
		this.txtCosto = txtCosto;
	}

	public JTextField getTxtNroSocioMulta() {
		return txtNroSocioMulta;
	}

	public void setTxtNroSocioMulta(JTextField txtNroSocioMulta) {
		this.txtNroSocioMulta = txtNroSocioMulta;
	}

	public JComboBox getCmbPagado() {
		return cmbPagado;
	}

	public void setCmbPagado(JComboBox cmbPagado) {
		this.cmbPagado = cmbPagado;
	}
	
	public JTextField getTxtFecIniAlquiler() {
		return txtFecIniAlquiler;
	}

	public void setTxtFecIniAlquiler(JTextField txtFecIniAlquiler) {
		this.txtFecIniAlquiler = txtFecIniAlquiler;
	}

	public JTextField getTextFechaDesde() {
		return textFechaDesde;
	}

	public void setTextFechaDesde(JTextField textFechaDesde) {
		this.textFechaDesde = textFechaDesde;
	}
}
