package vista;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.plaf.BorderUIResource;
import javax.swing.text.MaskFormatter;

import com.toedter.calendar.JDateChooser;

import controlador.ControladorVistaAlquilar;
import tabla.Tabla;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.border.BevelBorder;
import java.awt.Rectangle;
import javax.swing.border.LineBorder;

public class VistaAlquilar extends JInternalFrame {
	private final String ruta = System.getProperty("user.dir");
	private JTextField txtsocio;
	private JTextField txtNomApeSocioAlquiler;
	private JTextField txtDireccionSocioAlquiler;
	private JTextField txtTelefonoSocioAlquiler;
	private JTable tablaSeleccion;
	private Tabla t = new Tabla();
	private JTable tablaPelicula;
	private JButton btnQuitar;
	private JTextField txtFiltroAlquilerPelicula;
	private MaskFormatter mascara;
	private JTextField txtFecIniAlquiler;
	private JTextField txtCantipeliculas;
	private JDateChooser dateChooser;
	private JButton btnAlquilar;
	private JTextField txtPagar;
	private ControladorVistaAlquilar cva;
	private JLabel lblSocioMulta;
	private JButton btnBusqueda;
	private JPanel panel;

	/**
	 * Create the frame.
	 */
	public VistaAlquilar(ControladorVistaAlquilar cva) {
		getContentPane().setBounds(new Rectangle(5, 5, 5, 5));
		getContentPane().setBackground(Color.LIGHT_GRAY);
		this.setCva(cva);
		setBounds(100, 100, 1029, 621);
		setTitle("Alquiler");
		addInternalFrameListener(getCva());
		crearVista();

	}

	private void crearVista() {
		getContentPane().setLayout(null);
		setClosable(true);
		setIconifiable(true);
		JPanel panelSocio = new JPanel();
		panelSocio.setForeground(Color.WHITE);
		panelSocio.setOpaque(false);
		panelSocio.setBorder(new TitledBorder(
				new BevelBorder(BevelBorder.LOWERED, new Color(255, 255, 255), new Color(255, 255, 255),
						new Color(255, 255, 255), new Color(255, 255, 255)),
				"SOCIO", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(255, 255, 255)));
		// panelSocio.setBackground(Color.WHITE);
		panelSocio.setBounds(214, 24, 609, 175);
		getContentPane().add(panelSocio);
		panelSocio.setLayout(null);

		txtsocio = new JTextField();
		txtsocio.setForeground(Color.WHITE);
		txtsocio.setOpaque(false);
		txtsocio.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		txtsocio.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtsocio.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtsocio.setBounds(10, 53, 196, 22);

		panelSocio.add(txtsocio);
		txtsocio.setColumns(10);

		JLabel lblNumeroDeSocio = new JLabel("NUMERO DE SOCIO:");
		lblNumeroDeSocio.setForeground(Color.WHITE);
		lblNumeroDeSocio.setBounds(10, 20, 122, 22);
		panelSocio.add(lblNumeroDeSocio);

		txtNomApeSocioAlquiler = new JTextField();
		txtNomApeSocioAlquiler.setForeground(Color.WHITE);
		txtNomApeSocioAlquiler.setOpaque(false);
		txtNomApeSocioAlquiler.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtNomApeSocioAlquiler.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		txtNomApeSocioAlquiler.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtNomApeSocioAlquiler.setColumns(10);
		txtNomApeSocioAlquiler.setBounds(236, 53, 196, 22);
		panelSocio.add(txtNomApeSocioAlquiler);

		JLabel lblSocio = new JLabel("SOCIO:");
		lblSocio.setForeground(Color.WHITE);
		lblSocio.setBounds(236, 20, 101, 22);
		panelSocio.add(lblSocio);

		txtDireccionSocioAlquiler = new JTextField();
		txtDireccionSocioAlquiler.setForeground(Color.WHITE);
		txtDireccionSocioAlquiler.setOpaque(false);
		txtDireccionSocioAlquiler.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtDireccionSocioAlquiler.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtDireccionSocioAlquiler.setColumns(10);
		txtDireccionSocioAlquiler.setBounds(10, 119, 196, 22);
		panelSocio.add(txtDireccionSocioAlquiler);

		JLabel lblDireccion_1 = new JLabel("DIRECCION:");
		lblDireccion_1.setForeground(Color.WHITE);
		lblDireccion_1.setBounds(10, 86, 101, 22);
		panelSocio.add(lblDireccion_1);

		txtTelefonoSocioAlquiler = new JTextField();
		txtTelefonoSocioAlquiler.setForeground(Color.WHITE);
		txtTelefonoSocioAlquiler.setOpaque(false);
		txtTelefonoSocioAlquiler.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtTelefonoSocioAlquiler.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtTelefonoSocioAlquiler.setColumns(10);
		txtTelefonoSocioAlquiler.setBounds(236, 119, 196, 22);
		panelSocio.add(txtTelefonoSocioAlquiler);

		JLabel lblTelefono_1 = new JLabel("TELEFONO:");
		lblTelefono_1.setForeground(Color.WHITE);
		lblTelefono_1.setBounds(236, 86, 122, 22);
		panelSocio.add(lblTelefono_1);

		JLabel lblPeliculasSeleccionadas = new JLabel("PELICULA/S SELECCIONADA/S:");
		lblPeliculasSeleccionadas.setForeground(Color.WHITE);
		lblPeliculasSeleccionadas.setBounds(0, 414, 309, 22);
		getContentPane().add(lblPeliculasSeleccionadas);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(0, 447, 642, 115);
		getContentPane().add(scrollPane_2);

		tablaSeleccion = new JTable();
		tablaSeleccion.addMouseListener(getCva());
		tablaSeleccion.setModel(t.visualizar_TablaSelec());
		scrollPane_2.setViewportView(tablaSeleccion);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(0, 288, 642, 115);
		getContentPane().add(scrollPane_1);

		tablaPelicula = new JTable();
		tablaPelicula.addMouseListener(getCva());
		t.visualizar_TablaPeliculaReducida(tablaPelicula);

		scrollPane_1.setViewportView(tablaPelicula);

		btnQuitar = new JButton("QUITAR PELICULA");
		btnQuitar.setEnabled(false);
		btnQuitar.addActionListener(getCva());
		btnQuitar.setBounds(319, 414, 139, 23);
		getContentPane().add(btnQuitar);

		JLabel lblBuscarPelicula = new JLabel("BUSCAR PELICULA:");
		lblBuscarPelicula.setForeground(Color.WHITE);
		lblBuscarPelicula.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBuscarPelicula.setBounds(0, 255, 122, 22);
		getContentPane().add(lblBuscarPelicula);

		JLabel lblCodigoONombre = new JLabel("SELECCIONE LA/S PELICULA/S A ALQUILAR HACIENDO DOBLE CLICK");
		lblCodigoONombre.setForeground(Color.WHITE);
		lblCodigoONombre.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblCodigoONombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblCodigoONombre.setBounds(290, 255, 352, 22);
		getContentPane().add(lblCodigoONombre);

		txtFiltroAlquilerPelicula = new JTextField();
		txtFiltroAlquilerPelicula.setColumns(10);
		txtFiltroAlquilerPelicula.addKeyListener(getCva());
		txtFiltroAlquilerPelicula.setBounds(132, 257, 157, 20);
		getContentPane().add(txtFiltroAlquilerPelicula);
		// Para obtener la fecha actual
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		txtCantipeliculas = new JTextField();
		txtCantipeliculas.setEnabled(false);
		txtCantipeliculas.setEditable(false);
		txtCantipeliculas.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtCantipeliculas.setText("0");
		txtCantipeliculas.setBounds(879, 342, 134, 31);
		getContentPane().add(txtCantipeliculas);
		txtCantipeliculas.setColumns(10);

		JLabel lblTotalPeliculas = new JLabel("TOTAL PELICULAS:");
		lblTotalPeliculas.setForeground(Color.WHITE);
		lblTotalPeliculas.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTotalPeliculas.setBounds(729, 342, 151, 31);
		getContentPane().add(lblTotalPeliculas);

		txtPagar = new JTextField();
		txtPagar.setEnabled(false);
		txtPagar.setEditable(false);
		txtPagar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtPagar.setText("0.0");
		txtPagar.setColumns(10);
		txtPagar.setBounds(879, 384, 134, 31);
		getContentPane().add(txtPagar);

		JLabel lblPagar = new JLabel("TOTAL A PAGAR:");
		lblPagar.setForeground(Color.WHITE);
		lblPagar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPagar.setBounds(739, 384, 141, 31);
		getContentPane().add(lblPagar);

		btnAlquilar = new JButton("ALQUILAR");/// Solo almacenara en la base de datos el nuevo alquiler
		btnAlquilar.setEnabled(false);
		btnAlquilar.addActionListener(getCva());
		btnAlquilar.setBounds(835, 447, 178, 23);
		getContentPane().add(btnAlquilar);

		btnBusqueda = new JButton("CLIENTES");
		
		btnBusqueda.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnBusqueda.addActionListener(getCva());
		btnBusqueda.setBounds(461, 86, 95, 23);
		panelSocio.add(btnBusqueda);
		btnBusqueda.setBackground(new Color(0, 153, 51));
		btnBusqueda.setForeground(Color.white);
		btnBusqueda.setBorder(null);
		btnBusqueda.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnBusqueda.setOpaque(true);

		Image preview = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\search1.png");
		btnBusqueda.setIcon(new ImageIcon(preview.getScaledInstance(23, 23, Image.SCALE_DEFAULT)));

		lblSocioMulta = new JLabel("");
		lblSocioMulta.setBounds(10, 144, 342, 23);
		panelSocio.add(lblSocioMulta);
		lblSocioMulta.setForeground(Color.RED);

		panel = new JPanel();
		panel.setBorder(new TitledBorder(new MatteBorder(0, 2, 2, 2, (Color) new Color(255, 255, 255)), "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setOpaque(false);
		panel.setBounds(214, 201, 609, 43);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblFechaDeIngreso = new JLabel("FECHA DE PEDIDO DE ALQUILER:");
		lblFechaDeIngreso.setBounds(10, 5, 175, 24);
		panel.add(lblFechaDeIngreso);
		lblFechaDeIngreso.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFechaDeIngreso.setForeground(Color.WHITE);

		txtFecIniAlquiler = new JTextField();
		txtFecIniAlquiler.setBounds(195, 5, 116, 24);
		panel.add(txtFecIniAlquiler);
		txtFecIniAlquiler.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		txtFecIniAlquiler.setColumns(10);
		txtFecIniAlquiler.setText(dateFormat.format(date));

		JLabel lblFechaDeDevolucion = new JLabel("FECHA DE DEVOLUCION:");
		lblFechaDeDevolucion.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFechaDeDevolucion.setBounds(321, 5, 143, 24);
		panel.add(lblFechaDeDevolucion);
		lblFechaDeDevolucion.setForeground(Color.WHITE);

		dateChooser = new JDateChooser();
		dateChooser.setBounds(474, 5, 116, 24);
		panel.add(dateChooser);
		dateChooser.setDateFormatString("dd/MM/yyyy");
		
		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 1020, 591);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance(1020, 591, Image.SCALE_DEFAULT)));
		
		

	}

	public void completarCamposSocio(String nroSocio, String nomApe, String direccion, String telefono) {
		this.getTxtsocio().setText(nroSocio);
		this.getTxtNomApeSocioAlquiler().setText(nomApe);
		this.getTxtDireccionSocioAlquiler().setText(direccion);
		this.getTxtTelefonoSocioAlquiler().setText(telefono);
	}

	public JTextField getTxtsocio() {
		return txtsocio;
	}

	public void setTxtsocio(JTextField txtsocio) {
		this.txtsocio = txtsocio;
	}

	public JTextField getTxtNomApeSocioAlquiler() {
		return txtNomApeSocioAlquiler;
	}

	public void setTxtNomApeSocioAlquiler(JTextField txtNomApeSocioAlquiler) {
		this.txtNomApeSocioAlquiler = txtNomApeSocioAlquiler;
	}

	public JTextField getTxtDireccionSocioAlquiler() {
		return txtDireccionSocioAlquiler;
	}

	public void setTxtDireccionSocioAlquiler(JTextField txtDireccionSocioAlquiler) {
		this.txtDireccionSocioAlquiler = txtDireccionSocioAlquiler;
	}

	public JTextField getTxtTelefonoSocioAlquiler() {
		return txtTelefonoSocioAlquiler;
	}

	public void setTxtTelefonoSocioAlquiler(JTextField txtTelefonoSocioAlquiler) {
		this.txtTelefonoSocioAlquiler = txtTelefonoSocioAlquiler;
	}

	public JTable getTablaSeleccion() {
		return tablaSeleccion;
	}

	public void setTablaSeleccion(JTable tablaSeleccion) {
		this.tablaSeleccion = tablaSeleccion;
	}

	public JTable getTablaPelicula() {
		return tablaPelicula;
	}

	public void setTablaPelicula(JTable tablaPelicula) {
		this.tablaPelicula = tablaPelicula;
	}

	public JButton getBtnQuitar() {
		return btnQuitar;
	}

	public void setBtnQuitar(JButton btnQuitar) {
		this.btnQuitar = btnQuitar;
	}

	public JTextField getTxtFiltroAlquilerPelicula() {
		return txtFiltroAlquilerPelicula;
	}

	public void setTxtFiltroAlquilerPelicula(JTextField txtFiltroAlquilerPelicula) {
		this.txtFiltroAlquilerPelicula = txtFiltroAlquilerPelicula;
	}

	public JTextField getTxtFecIniAlquiler() {
		return txtFecIniAlquiler;
	}

	public void setTxtFecIniAlquiler(JTextField txtFecIniAlquiler) {
		this.txtFecIniAlquiler = txtFecIniAlquiler;
	}

	public JTextField getTxtCantipeliculas() {
		return txtCantipeliculas;
	}

	public void setTxtCantipeliculas(JTextField txtCantipeliculas) {
		this.txtCantipeliculas = txtCantipeliculas;
	}

	public JDateChooser getDateChooser() {
		return dateChooser;
	}

	public void setDateChooser(JDateChooser dateChooser) {
		this.dateChooser = dateChooser;
	}

	public JButton getBtnAlquilar() {
		return btnAlquilar;
	}

	public void setBtnAlquilar(JButton btnAlquilar) {
		this.btnAlquilar = btnAlquilar;
	}

	public JTextField getTxtPagar() {
		return txtPagar;
	}

	public void setTxtPagar(JTextField txtPagar) {
		this.txtPagar = txtPagar;
	}

	public ControladorVistaAlquilar getCva() {
		return cva;
	}

	public void setCva(ControladorVistaAlquilar cva) {
		this.cva = cva;
	}

	public JLabel getLblSocioMulta() {
		return lblSocioMulta;
	}

	public void setLblSocioMulta(JLabel lblSocioMulta) {
		this.lblSocioMulta = lblSocioMulta;
	}

	public JButton getBtnBusqueda() {
		return btnBusqueda;
	}

	public void setBtnBusqueda(JButton btnBusqueda) {
		this.btnBusqueda = btnBusqueda;
	}


}
