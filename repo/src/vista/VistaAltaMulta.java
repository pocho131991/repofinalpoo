package vista;

import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.ParseException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import controlador.ControladorAltaMulta;
import java.awt.Color;
import javax.swing.border.MatteBorder;

public class VistaAltaMulta extends JDialog {
	private JTextField txtNroSocio;
	private JButton btnBusqueda;
	
	private JFormattedTextField ftxtFecha;
	private ControladorAltaMulta cam;
	private JButton btnAgregarMulta;
	private JComboBox cmbTipo;
	private final String ruta = System.getProperty("user.dir");


	public VistaAltaMulta(ControladorAltaMulta cam) {
		this.setCam(cam);
		setBounds(100, 100, 450, 200);
		getContentPane().setLayout(null);

		JLabel lblNroSocio = new JLabel("Nro Socio:");
		lblNroSocio.setForeground(Color.WHITE);
		lblNroSocio.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNroSocio.setBounds(10, 38, 99, 31);
		getContentPane().add(lblNroSocio);

		txtNroSocio = new JTextField();
		txtNroSocio.setBorder(new MatteBorder(0, 0, 2, 1, (Color) Color.WHITE));
		txtNroSocio.setForeground(Color.WHITE);
		txtNroSocio.setOpaque(false);
		txtNroSocio.setBounds(119, 38, 99, 31);
		getContentPane().add(txtNroSocio);
		txtNroSocio.setColumns(10);

		JLabel lblTipoMulta = new JLabel("Tipo Multa:");
		lblTipoMulta.setForeground(Color.WHITE);
		lblTipoMulta.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblTipoMulta.setBounds(10, 80, 99, 31);
		getContentPane().add(lblTipoMulta);

		JLabel lblFechaMulta = new JLabel("Fecha Multa:");
		lblFechaMulta.setForeground(Color.WHITE);
		lblFechaMulta.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblFechaMulta.setBounds(10, 122, 99, 31);
		getContentPane().add(lblFechaMulta);

		cmbTipo = new JComboBox();
		cmbTipo.setModel(new DefaultComboBoxModel(new String[] { "TIPO 1", "TIPO 2", "TIPO 3" }));
		cmbTipo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		cmbTipo.setBounds(119, 80, 99, 31);
		getContentPane().add(cmbTipo);

		MaskFormatter mascara;
		try {
			mascara = new MaskFormatter("##/##/####");
			mascara.setPlaceholderCharacter('-');
			ftxtFecha = new JFormattedTextField(mascara);
			ftxtFecha.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
			ftxtFecha.setForeground(Color.WHITE);
			ftxtFecha.setOpaque(false);
			ftxtFecha.setBounds(119, 122, 99, 28);
		} catch (ParseException e) {

			e.printStackTrace();
		}

		getContentPane().add(ftxtFecha);


		btnBusqueda = new JButton("Busqueda");
		btnBusqueda.setOpaque(true);
		btnBusqueda.setForeground(Color.WHITE);
		btnBusqueda.setFont(new Font("Sitka Small", Font.PLAIN, 15));
		btnBusqueda.setBorder(null);
		btnBusqueda.setBackground(new Color(0, 153, 51));
		btnBusqueda.addActionListener(getCam());
		btnBusqueda.setBounds(323, 38, 85, 31);
		getContentPane().add(btnBusqueda);
		
		btnAgregarMulta = new JButton("Agregar Multa");
		btnAgregarMulta.setBounds(289, 125, 119, 23);
		btnAgregarMulta.setOpaque(true);
		btnAgregarMulta.setForeground(Color.WHITE);
		btnAgregarMulta.setFont(new Font("Sitka Small", Font.PLAIN, 15));
		btnAgregarMulta.setBorder(null);
		btnAgregarMulta.setBackground(new Color(0, 153, 51));
		getContentPane().add(btnAgregarMulta);
		
		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0,  450, 200);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance( 450, 200, Image.SCALE_DEFAULT)));

	}

	public ControladorAltaMulta getCam() {
		return cam;
	}

	public void setCam(ControladorAltaMulta cam) {
		this.cam = cam;
	}

	public JTextField getTxtNroSocio() {
		return txtNroSocio;
	}

	public void setTxtNroSocio(JTextField txtNroSocio) {
		this.txtNroSocio = txtNroSocio;
	}

	public JButton getBtnBusqueda() {
		return btnBusqueda;
	}

	public void setBtnBusqueda(JButton btnBusqueda) {
		this.btnBusqueda = btnBusqueda;
	}

	public JFormattedTextField getFtxtFecha() {
		return ftxtFecha;
	}

	public void setFtxtFecha(JFormattedTextField ftxtFecha) {
		this.ftxtFecha = ftxtFecha;
	}

	public JButton getBtnAgregarMulta() {
		return btnAgregarMulta;
	}

	public void setBtnAgregarMulta(JButton btnAgregarMulta) {
		this.btnAgregarMulta = btnAgregarMulta;
	}

	public JComboBox getCmbTipo() {
		return cmbTipo;
	}

	public void setCmbTipo(JComboBox cmbTipo) {
		this.cmbTipo = cmbTipo;
	}
}
