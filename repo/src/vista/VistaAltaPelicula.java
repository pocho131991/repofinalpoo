package vista;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controlador.ControladorAltaPelicula;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;

public class VistaAltaPelicula extends JDialog {

	public static void main(String[] args) {
		try {
			VistaAltaPelicula dialog = new VistaAltaPelicula();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private final String ruta = System.getProperty("user.dir");
	private JTextField txttitulo;
	private JTextField txttitulolat;
	private JTextField txtduracion;
	private JTextField txtanio;
	private JTextField txtdirector;
	private JTextArea txtAActoresP;
	private JTextArea txtSinopsis;
	private JButton btnGuardar;
	private JButton btnVolver;
	private JButton btnLimpiar;
	private JButton btnImaPeli;
	private JScrollPane scrollPane;
	private JTextField txt_ruta;
	private ControladorAltaPelicula cap;
	private JLabel lblExcepcionTitulo;
	private JLabel lblExcepcionTituloA;
	private JLabel lblExcepcionDuracion;
	private JLabel lblExcepcionAnio;
	private JLabel lblExcepcionDirector;
	private JTextField txtgenero;
	private JComboBox cmbFormato;
	private JLabel lblExcepcionGenero;
	private JLabel lblExcepcionActores;
	private JLabel lblExcepcionSinopsis;
	private JLabel lblExcepcionFormato;
	private JTextField txtCantidad;
	private JTextField txtPrecio;
	private JLabel lblPrecio;
	private JLabel lblExcepcionPrecio;
	private JLabel lblExcepcionCantidad;

	/**
	 * Create the dialog.
	 */
	public VistaAltaPelicula() {

		crearVista();
	}

	public VistaAltaPelicula(ControladorAltaPelicula cap) {
		this.setCap(cap);
		crearVista();
	}

	private void crearVista() {
		setTitle("Agregar pelicula");
		setBounds(50, 50, 900, 500);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		getContentPane().setBackground(UIManager.getColor("ProgressBar.foreground"));
		setModal(true);
		///////
		JLabel lblTitulo = new JLabel("TITULO:");
		lblTitulo.setForeground(Color.WHITE);
		lblTitulo.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblTitulo.setBounds(10, 47, 133, 29);
		getContentPane().add(lblTitulo);

		JLabel lblTituloLatino = new JLabel("TITULO LATINO:");
		lblTituloLatino.setForeground(Color.WHITE);
		lblTituloLatino.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblTituloLatino.setBounds(10, 105, 133, 29);
		getContentPane().add(lblTituloLatino);

		JLabel lblDuracion = new JLabel("DURACION:");
		lblDuracion.setForeground(Color.WHITE);
		lblDuracion.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblDuracion.setBounds(10, 161, 133, 29);
		getContentPane().add(lblDuracion);

		JLabel lblAo = new JLabel("A\u00D1O:");
		lblAo.setForeground(Color.WHITE);
		lblAo.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblAo.setBounds(10, 216, 133, 29);
		getContentPane().add(lblAo);

		JLabel lblDirector = new JLabel("DIRECTOR:");
		lblDirector.setForeground(Color.WHITE);
		lblDirector.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblDirector.setBounds(10, 273, 133, 29);
		getContentPane().add(lblDirector);

		JLabel lblActoresPrincipales = new JLabel("ACTORES:");
		lblActoresPrincipales.setForeground(Color.WHITE);
		lblActoresPrincipales.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblActoresPrincipales.setBounds(462, 47, 88, 29);
		getContentPane().add(lblActoresPrincipales);
		//////
		txttitulo = new JTextField();
		txttitulo.setOpaque(false);
		txttitulo.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txttitulo.setForeground(Color.WHITE);
		txttitulo.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		txttitulo.setBounds(153, 47, 243, 29);
		getContentPane().add(txttitulo);
		txttitulo.setColumns(10);

		txttitulolat = new JTextField();
		txttitulolat.setOpaque(false);
		txttitulolat.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txttitulolat.setForeground(Color.WHITE);
		txttitulolat.setFont(new Font("Sitka Subheading", Font.PLAIN, 11));
		txttitulolat.setColumns(10);
		txttitulolat.setBounds(153, 105, 243, 29);
		getContentPane().add(txttitulolat);

		txtduracion = new JTextField();
		txtduracion.setOpaque(false);
		txtduracion.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtduracion.setForeground(Color.WHITE);
		txtduracion.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		txtduracion.setColumns(10);
		txtduracion.setBounds(153, 161, 243, 29);
		getContentPane().add(txtduracion);

		txtanio = new JTextField();
		txtanio.setOpaque(false);
		txtanio.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtanio.setForeground(Color.WHITE);
		txtanio.setFont(new Font("Sylfaen", Font.PLAIN, 16));
		txtanio.setColumns(10);
		txtanio.setBounds(153, 217, 243, 29);
		getContentPane().add(txtanio);

		txtdirector = new JTextField();
		txtdirector.setOpaque(false);
		txtdirector.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtdirector.setForeground(Color.WHITE);
		txtdirector.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		txtdirector.setColumns(10);
		txtdirector.setBounds(153, 273, 243, 29);
		getContentPane().add(txtdirector);

		txt_ruta = new JTextField();
		txt_ruta.setColumns(10);
		txt_ruta.setBounds(545, 161, 222, 29);
		getContentPane().add(txt_ruta);

		txtgenero = new JTextField();
		txtgenero.setOpaque(false);
		txtgenero.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtgenero.setForeground(Color.WHITE);
		txtgenero.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		txtgenero.setColumns(10);
		txtgenero.setBounds(153, 329, 243, 29);
		getContentPane().add(txtgenero);
		JScrollPane laminaBarras = new JScrollPane();
		laminaBarras.setLocation(545, 47);
		laminaBarras.setSize(332, 84);
		getContentPane().add(laminaBarras);

		txtAActoresP = new JTextArea(4, 10);
		txtAActoresP.setForeground(Color.black);
		txtAActoresP.setFont(new Font("Monospaced", Font.PLAIN, 16));
		txtAActoresP.setTabSize(4);
		laminaBarras.setViewportView(txtAActoresP);
		txtAActoresP.setLineWrap(true);

		///////////////////
		btnGuardar = new JButton("GUARDAR");
		btnGuardar.setBackground(new Color(0, 153, 51));
		btnGuardar.setForeground(Color.white);
		btnGuardar.setBorder(null);
		btnGuardar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnGuardar.setOpaque(true);
		btnGuardar.addActionListener(this.getCap());
		btnGuardar.setBounds(462, 418, 133, 38);
		getContentPane().add(btnGuardar);

		btnVolver = new JButton("VOLVER");
		btnVolver.setBackground(new Color(0, 153, 51));
		btnVolver.setForeground(Color.white);
		btnVolver.setBorder(null);
		btnVolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnVolver.setOpaque(true);
		btnVolver.addActionListener(this.getCap());
		btnVolver.setBounds(605, 418, 133, 38);
		getContentPane().add(btnVolver);

		btnLimpiar = new JButton("LIMPIAR");
		btnLimpiar.setBackground(new Color(0, 153, 51));
		btnLimpiar.setForeground(Color.white);
		btnLimpiar.setBorder(null);
		btnLimpiar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnLimpiar.setOpaque(true);
		btnLimpiar.addActionListener(this.getCap());
		btnLimpiar.setBounds(748, 418, 133, 38);
		getContentPane().add(btnLimpiar);

		btnImaPeli = new JButton("IMAGEN");
		btnImaPeli.setBackground(new Color(0, 153, 51));
		btnImaPeli.setForeground(Color.white);
		btnImaPeli.setBorder(null);
		btnImaPeli.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnImaPeli.setOpaque(true);
		btnImaPeli.addActionListener(this.getCap());
		btnImaPeli.setBounds(777, 162, 103, 29);
		getContentPane().add(btnImaPeli);

		JLabel lblRuta = new JLabel("RUTA:");
		lblRuta.setForeground(Color.WHITE);
		lblRuta.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblRuta.setBounds(462, 161, 88, 29);
		getContentPane().add(lblRuta);

		JLabel lblGenero = new JLabel("GENERO:");
		lblGenero.setForeground(Color.WHITE);
		lblGenero.setHorizontalAlignment(SwingConstants.LEFT);
		lblGenero.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblGenero.setBounds(10, 329, 133, 29);
		getContentPane().add(lblGenero);

		JLabel lblSinopsis = new JLabel("SINOPSIS:");
		lblSinopsis.setForeground(Color.WHITE);
		lblSinopsis.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblSinopsis.setBounds(462, 217, 88, 29);
		getContentPane().add(lblSinopsis);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(545, 217, 332, 80);
		getContentPane().add(scrollPane);

		txtSinopsis = new JTextArea(5, 10);
		txtSinopsis.setForeground(Color.WHITE);
		txtSinopsis.setTabSize(5);
		txtSinopsis.setLineWrap(true);
		scrollPane.setViewportView(txtSinopsis);

		JLabel lblDatosDePelicula = new JLabel("Datos de pelicula");
		lblDatosDePelicula.setForeground(Color.WHITE);
		lblDatosDePelicula.setFont(new Font("Sitka Subheading", Font.PLAIN, 22));
		lblDatosDePelicula.setBounds(10, 11, 204, 38);
		getContentPane().add(lblDatosDePelicula);

		lblExcepcionTitulo = new JLabel("");
		lblExcepcionTitulo.setForeground(Color.RED);
		lblExcepcionTitulo.setBounds(153, 78, 164, 14);
		getContentPane().add(lblExcepcionTitulo);

		lblExcepcionTituloA = new JLabel("");
		lblExcepcionTituloA.setForeground(Color.RED);
		lblExcepcionTituloA.setBounds(153, 136, 164, 14);
		getContentPane().add(lblExcepcionTituloA);

		lblExcepcionDuracion = new JLabel("");
		lblExcepcionDuracion.setForeground(Color.RED);
		lblExcepcionDuracion.setBounds(153, 191, 164, 14);
		getContentPane().add(lblExcepcionDuracion);

		lblExcepcionAnio = new JLabel("");
		lblExcepcionAnio.setForeground(Color.RED);
		lblExcepcionAnio.setBounds(153, 248, 164, 14);
		getContentPane().add(lblExcepcionAnio);

		lblExcepcionDirector = new JLabel("");
		lblExcepcionDirector.setForeground(Color.RED);
		lblExcepcionDirector.setBounds(153, 304, 164, 14);
		getContentPane().add(lblExcepcionDirector);

		lblExcepcionGenero = new JLabel("");
		lblExcepcionGenero.setForeground(Color.RED);
		lblExcepcionGenero.setBounds(153, 360, 164, 14);
		getContentPane().add(lblExcepcionGenero);

		JLabel lblFormato = new JLabel("FORMATO:");
		lblFormato.setForeground(Color.WHITE);
		lblFormato.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblFormato.setBounds(463, 329, 87, 29);
		getContentPane().add(lblFormato);

		cmbFormato = new JComboBox();
		cmbFormato.setFont(new Font("Sitka Text", Font.PLAIN, 16));
		cmbFormato.setModel(new DefaultComboBoxModel(new String[] { "", "DVD", "BLU-RAY" }));
		cmbFormato.setBounds(545, 329, 96, 29);
		getContentPane().add(cmbFormato);

		lblExcepcionActores = new JLabel("");
		lblExcepcionActores.setForeground(Color.RED);
		lblExcepcionActores.setBounds(545, 132, 164, 14);
		getContentPane().add(lblExcepcionActores);

		lblExcepcionSinopsis = new JLabel("");
		lblExcepcionSinopsis.setForeground(Color.RED);
		lblExcepcionSinopsis.setBounds(545, 298, 164, 14);
		getContentPane().add(lblExcepcionSinopsis);

		lblExcepcionFormato = new JLabel("");
		lblExcepcionFormato.setForeground(Color.RED);
		lblExcepcionFormato.setBounds(545, 360, 164, 14);
		getContentPane().add(lblExcepcionFormato);

		JLabel lblCantidad = new JLabel("CANTIDAD:");
		lblCantidad.setForeground(Color.WHITE);
		lblCantidad.setHorizontalAlignment(SwingConstants.LEFT);
		lblCantidad.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblCantidad.setBounds(10, 386, 133, 29);
		getContentPane().add(lblCantidad);

		txtCantidad = new JTextField();
		txtCantidad.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtCantidad.setOpaque(false);
		txtCantidad.setForeground(Color.WHITE);
		txtCantidad.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		txtCantidad.setColumns(10);
		txtCantidad.setBounds(153, 385, 243, 29);
		getContentPane().add(txtCantidad);

		txtPrecio = new JTextField();
		txtPrecio.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtPrecio.setForeground(Color.WHITE);
		txtPrecio.setOpaque(false);
		txtPrecio.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(545, 385, 125, 29);
		getContentPane().add(txtPrecio);

		lblPrecio = new JLabel("PRECIO:");
		lblPrecio.setForeground(Color.WHITE);
		lblPrecio.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPrecio.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblPrecio.setBounds(451, 386, 88, 29);
		getContentPane().add(lblPrecio);

		lblExcepcionPrecio = new JLabel("");
		lblExcepcionPrecio.setForeground(Color.RED);
		lblExcepcionPrecio.setBounds(674, 393, 164, 14);
		getContentPane().add(lblExcepcionPrecio);

		lblExcepcionCantidad = new JLabel("");
		lblExcepcionCantidad.setForeground(Color.RED);
		lblExcepcionCantidad.setBounds(153, 418, 164, 14);
		getContentPane().add(lblExcepcionCantidad);
		
		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 900, 500);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance(900, 500, Image.SCALE_DEFAULT)));
	}

	public JTextField getTxttitulo() {
		return txttitulo;
	}

	public void setTxttitulo(JTextField txttitulo) {
		this.txttitulo = txttitulo;
	}

	public JTextField getTxttitulolat() {
		return txttitulolat;
	}

	public void setTxttitulolat(JTextField txttitulolat) {
		this.txttitulolat = txttitulolat;
	}

	public JTextField getTxtduracion() {
		return txtduracion;
	}

	public void setTxtduracion(JTextField txtduracion) {
		this.txtduracion = txtduracion;
	}

	public JTextField getTxtanio() {
		return txtanio;
	}

	public void setTxtanio(JTextField txtanio) {
		this.txtanio = txtanio;
	}

	public JTextField getTxtdirector() {
		return txtdirector;
	}

	public void setTxtdirector(JTextField txtdirector) {
		this.txtdirector = txtdirector;
	}

	public JTextArea getTxtAActoresP() {
		return txtAActoresP;
	}

	public void setTxtAActoresP(JTextArea txtAActoresP) {
		this.txtAActoresP = txtAActoresP;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public void setBtnVolver(JButton btnVolver) {
		this.btnVolver = btnVolver;
	}

	public JButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(JButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public JButton getBtnImaPeli() {
		return btnImaPeli;
	}

	public void setBtnImaPeli(JButton btnImaPeli) {
		this.btnImaPeli = btnImaPeli;
	}

	public ControladorAltaPelicula getCap() {
		return cap;
	}

	public void setCap(ControladorAltaPelicula cap) {
		this.cap = cap;
	}

	public JTextArea getTxtSinopsis() {
		return txtSinopsis;
	}

	public void setTxtSinopsis(JTextArea txtSinopsis) {
		this.txtSinopsis = txtSinopsis;
	}

	public JTextField getTxt_ruta() {
		return txt_ruta;
	}

	public void setTxt_ruta(JTextField txt_ruta) {
		this.txt_ruta = txt_ruta;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JTextField getTxtgenero() {
		return txtgenero;
	}

	public void setTxtgenero(JTextField txtgenero) {
		this.txtgenero = txtgenero;
	}

	public JComboBox getCmbFormato() {
		return cmbFormato;
	}

	public void setCmbFormato(JComboBox cmbFormato) {
		this.cmbFormato = cmbFormato;
	}

	public JLabel getLblExcepcionTitulo() {
		return lblExcepcionTitulo;
	}

	public void setLblExcepcionTitulo(JLabel lblExcepcionTitulo) {
		this.lblExcepcionTitulo = lblExcepcionTitulo;
	}

	public JLabel getLblExcepcionTituloA() {
		return lblExcepcionTituloA;
	}

	public void setLblExcepcionTituloA(JLabel lblExcepcionTituloA) {
		this.lblExcepcionTituloA = lblExcepcionTituloA;
	}

	public JLabel getLblExcepcionDuracion() {
		return lblExcepcionDuracion;
	}

	public void setLblExcepcionDuracion(JLabel lblExcepcionDuracion) {
		this.lblExcepcionDuracion = lblExcepcionDuracion;
	}

	public JLabel getLblExcepcionAnio() {
		return lblExcepcionAnio;
	}

	public void setLblExcepcionAnio(JLabel lblExcepcionAnio) {
		this.lblExcepcionAnio = lblExcepcionAnio;
	}

	public JLabel getLblExcepcionDirector() {
		return lblExcepcionDirector;
	}

	public void setLblExcepcionDirector(JLabel lblExcepcionDirector) {
		this.lblExcepcionDirector = lblExcepcionDirector;
	}

	public JLabel getLblExcepcionGenero() {
		return lblExcepcionGenero;
	}

	public void setLblExcepcionGenero(JLabel lblExcepcionGenero) {
		this.lblExcepcionGenero = lblExcepcionGenero;
	}

	public JLabel getLblExcepcionActores() {
		return lblExcepcionActores;
	}

	public void setLblExcepcionActores(JLabel lblExcepcionActores) {
		this.lblExcepcionActores = lblExcepcionActores;
	}

	public JLabel getLblExcepcionSinopsis() {
		return lblExcepcionSinopsis;
	}

	public void setLblExcepcionSinopsis(JLabel lblExcepcionSinopsis) {
		this.lblExcepcionSinopsis = lblExcepcionSinopsis;
	}

	public JLabel getLblExcepcionFormato() {
		return lblExcepcionFormato;
	}

	public void setLblExcepcionFormato(JLabel lblExcepcionFormato) {
		this.lblExcepcionFormato = lblExcepcionFormato;
	}

	public JTextField getTxtCantidad() {
		return txtCantidad;
	}

	public void setTxtCantidad(JTextField txtCantidad) {
		this.txtCantidad = txtCantidad;
	}

	public JTextField getTxtPrecio() {
		return txtPrecio;
	}

	public void setTxtPrecio(JTextField txtPrecio) {
		this.txtPrecio = txtPrecio;
	}

	public JLabel getLblPrecio() {
		return lblPrecio;
	}

	public void setLblPrecio(JLabel lblPrecio) {
		this.lblPrecio = lblPrecio;
	}

	public JLabel getLblExcepcionPrecio() {
		return lblExcepcionPrecio;
	}

	public void setLblExcepcionPrecio(JLabel lblExcepcionPrecio) {
		this.lblExcepcionPrecio = lblExcepcionPrecio;
	}

	public JLabel getLblExcepcionCantidad() {
		return lblExcepcionCantidad;
	}

	public void setLblExcepcionCantidad(JLabel lblExcepcionCantidad) {
		this.lblExcepcionCantidad = lblExcepcionCantidad;
	}
}
