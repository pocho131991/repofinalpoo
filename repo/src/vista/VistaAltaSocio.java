package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;
import javax.swing.text.MaskFormatter;

import controlador.ControladorAltaSocio;

public class VistaAltaSocio extends JDialog {

	private static final long serialVersionUID = 1L;
	private final String ruta = System.getProperty("user.dir");
	private JTextField txt_nombre;
	private JTextField txt_apellido;
	private JTextField txt_calle;
	private JTextField txt_dni;
	private JTextField txt_num_tel;
	private JComboBox cmb_dia;
	private JComboBox cmb_anio;
	private JComboBox cbm_mes;
	private JButton btnGuardar;
	private JButton btnVolver;
	private JButton btnLimpiarCampos;
	private ControladorAltaSocio cas;
	private JLabel lblIngreseNombreDel;
	private JLabel lblIngreseApellidosDel;
	private JLabel lblCalle;
	private JLabel lbl_dni;
	private JLabel lblIngreseNumeroTelefonico;
	private JLabel lblIngresoDeFecha;

	public VistaAltaSocio() {

		crearVista();

	}

	public VistaAltaSocio(ControladorAltaSocio cas) {
		this.setCas(cas);
		crearVista();

	}

	private void crearVista() {
		setTitle("Agregar Socio");
		setBounds(100, 100, 850, 550);
		getContentPane().setLayout(null);
		getContentPane().setBackground(new Color(153, 153, 102));
		setModal(true);
		///// JLabel
		JLabel lbl_datos = new JLabel("DATOS PERSONALES");
		lbl_datos.setForeground(Color.WHITE);
		lbl_datos.setFont(new Font("Sitka Subheading", Font.BOLD, 24));
		lbl_datos.setBounds(10, 11, 244, 38);
		getContentPane().add(lbl_datos);

		JLabel lblApe_Nom = new JLabel("* NOMBRE/S:");
		lblApe_Nom.setForeground(Color.WHITE);
		lblApe_Nom.setVerticalAlignment(SwingConstants.BOTTOM);
		lblApe_Nom.setFont(new Font("Sitka Subheading", Font.PLAIN, 22));
		lblApe_Nom.setBounds(10, 60, 188, 38);
		getContentPane().add(lblApe_Nom);

		JLabel lblApellidos = new JLabel("* APELLIDO/S:");
		lblApellidos.setForeground(Color.WHITE);
		lblApellidos.setVerticalAlignment(SwingConstants.BOTTOM);
		lblApellidos.setFont(new Font("Sitka Subheading", Font.PLAIN, 22));
		lblApellidos.setBounds(10, 128, 188, 38);
		getContentPane().add(lblApellidos);

		JLabel lblDni_1 = new JLabel("* DNI:");
		lblDni_1.setForeground(Color.WHITE);
		lblDni_1.setVerticalAlignment(SwingConstants.BOTTOM);
		lblDni_1.setFont(new Font("Sitka Subheading", Font.PLAIN, 22));
		lblDni_1.setBounds(10, 190, 188, 38);
		getContentPane().add(lblDni_1);

		JLabel lblCalle_1 = new JLabel("DIRECCION:");
		lblCalle_1.setForeground(Color.WHITE);
		lblCalle_1.setVerticalAlignment(SwingConstants.BOTTOM);
		lblCalle_1.setFont(new Font("Sitka Subheading", Font.PLAIN, 22));
		lblCalle_1.setBounds(10, 304, 188, 38);
		getContentPane().add(lblCalle_1);

		JLabel lblTelefono_1 = new JLabel("TELEFONO:");
		lblTelefono_1.setForeground(Color.WHITE);
		lblTelefono_1.setVerticalAlignment(SwingConstants.BOTTOM);
		lblTelefono_1.setFont(new Font("Sitka Subheading", Font.PLAIN, 22));
		lblTelefono_1.setBounds(473, 304, 125, 38);
		getContentPane().add(lblTelefono_1);

		JLabel lblNombre = new JLabel("FECHA DE NACIMIENTO");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setFont(new Font("Sitka Subheading", Font.BOLD, 24));
		lblNombre.setBounds(497, 11, 477, 38);
		getContentPane().add(lblNombre);

		JLabel lblDireccion = new JLabel("DIRECCION");
		lblDireccion.setForeground(Color.WHITE);
		lblDireccion.setFont(new Font("Sitka Subheading", Font.BOLD, 24));
		lblDireccion.setBounds(10, 255, 162, 38);
		getContentPane().add(lblDireccion);

		JLabel lblDia = new JLabel("DIA");
		lblDia.setForeground(Color.WHITE);
		lblDia.setBounds(507, 49, 80, 14);
		getContentPane().add(lblDia);

		JLabel lblMes = new JLabel("MES");
		lblMes.setForeground(Color.WHITE);
		lblMes.setBounds(597, 49, 80, 14);
		getContentPane().add(lblMes);

		JLabel lblAnio = new JLabel("A\u00D1O");
		lblAnio.setForeground(Color.WHITE);
		lblAnio.setBounds(687, 49, 80, 14);
		getContentPane().add(lblAnio);

		lblIngreseNumeroTelefonico = new JLabel("Ingrese numero telefonico del socio");
		lblIngreseNumeroTelefonico.setForeground(Color.WHITE);
		lblIngreseNumeroTelefonico.setBounds(597, 340, 218, 14);
		getContentPane().add(lblIngreseNumeroTelefonico);

		lblIngreseNombreDel = new JLabel("Ingrese nombre/s del socio");
		lblIngreseNombreDel.setForeground(Color.WHITE);
		lblIngreseNombreDel.setBounds(208, 98, 218, 14);
		getContentPane().add(lblIngreseNombreDel);

		lblIngreseApellidosDel = new JLabel("Ingrese apellido/s del socio");
		lblIngreseApellidosDel.setForeground(Color.WHITE);
		lblIngreseApellidosDel.setBounds(208, 164, 218, 14);
		getContentPane().add(lblIngreseApellidosDel);

		lblCalle = new JLabel("Ingrese calle ");
		lblCalle.setForeground(Color.WHITE);
		lblCalle.setBounds(208, 340, 195, 14);
		getContentPane().add(lblCalle);

		lbl_dni = new JLabel("Ingrese DNI del socio");
		lbl_dni.setForeground(Color.WHITE);
		lbl_dni.setBounds(208, 230, 218, 14);
		getContentPane().add(lbl_dni);

		lblIngresoDeFecha = new JLabel("Ingreso de fecha incorrecta");
		lblIngresoDeFecha.setVisible(false);
		lblIngresoDeFecha.setForeground(Color.RED);
		lblIngresoDeFecha.setBounds(507, 115, 195, 14);
		getContentPane().add(lblIngresoDeFecha);
		///// JTextField
		txt_nombre = new JTextField();
		txt_nombre.setBounds(208, 60, 220, 37);
		getContentPane().add(txt_nombre);
		txt_nombre.setColumns(10);

		txt_apellido = new JTextField();
		txt_apellido.setColumns(10);
		txt_apellido.setBounds(208, 123, 218, 37);
		getContentPane().add(txt_apellido);

		txt_calle = new JTextField();
		txt_calle.setColumns(10);
		txt_calle.setBounds(208, 303, 218, 37);
		getContentPane().add(txt_calle);

		txt_dni = new JTextField();
		txt_dni.setColumns(10);
		txt_dni.setBounds(208, 189, 218, 37);
		getContentPane().add(txt_dni);

		txt_num_tel = new JTextField();
		txt_num_tel.setColumns(10);
		txt_num_tel.setBounds(597, 304, 218, 37);
		getContentPane().add(txt_num_tel);

		try {
			MaskFormatter formatter = new MaskFormatter("####");
		} catch (ParseException e1) {

			e1.printStackTrace();
		}

		///// JComboBox
		cmb_dia = new JComboBox();
		cmb_dia.setModel(new DefaultComboBoxModel(
				new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
						"17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
		cmb_dia.setBounds(507, 67, 80, 37);
		getContentPane().add(cmb_dia);

		cbm_mes = new JComboBox();
		cbm_mes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		cbm_mes.setModel(new DefaultComboBoxModel(
				new String[] { "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" }));
		cbm_mes.setBounds(597, 67, 80, 37);
		getContentPane().add(cbm_mes);

		cmb_anio = new JComboBox();
		cmb_anio.setModel(new DefaultComboBoxModel(new String[] { "1990", "1991", "1992", "1993", "1994", "1995",
				"1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008",
				"2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019" }));
		cmb_anio.setBounds(687, 67, 80, 37);
		getContentPane().add(cmb_anio);

		/// JButton
		btnGuardar = new JButton("GUARDAR");
		btnGuardar.addActionListener(this.getCas());

		btnGuardar.setBounds(91, 427, 141, 66);
		getContentPane().add(btnGuardar);

		btnVolver = new JButton("VOLVER");
		btnVolver.addActionListener(this.getCas());
		btnVolver.setBounds(339, 427, 141, 66);
		getContentPane().add(btnVolver);

		btnLimpiarCampos = new JButton("LIMPIAR CAMPOS");
		btnLimpiarCampos.addActionListener(this.getCas());
		btnLimpiarCampos.setBounds(564, 427, 141, 66);
		getContentPane().add(btnLimpiarCampos);
		
		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 850, 550);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance(850, 550, Image.SCALE_DEFAULT)));
	}

	public JTextField getTxt_nombre() {
		return txt_nombre;
	}

	public void setTxt_nombre(JTextField txt_nombre) {
		this.txt_nombre = txt_nombre;
	}

	public JTextField getTxt_apellido() {
		return txt_apellido;
	}

	public void setTxt_apellido(JTextField txt_apellido) {
		this.txt_apellido = txt_apellido;
	}

	public JTextField getTxt_calle() {
		return txt_calle;
	}

	public void setTxt_calle(JTextField txt_calle) {
		this.txt_calle = txt_calle;
	}

	public JTextField getTxt_dni() {
		return txt_dni;
	}

	public void setTxt_dni(JTextField txt_dni) {
		this.txt_dni = txt_dni;
	}

	public JTextField getTxt_num_tel() {
		return txt_num_tel;
	}

	public void setTxt_num_tel(JTextField txt_num_tel) {
		this.txt_num_tel = txt_num_tel;
	}



	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public void setBtnVolver(JButton btnVolver) {
		this.btnVolver = btnVolver;
	}

	public JButton getBtnLimpiarCampos() {
		return btnLimpiarCampos;
	}

	public void setBtnLimpiarCampos(JButton btnLimpiarCampos) {
		this.btnLimpiarCampos = btnLimpiarCampos;
	}

	public ControladorAltaSocio getCas() {
		return cas;
	}

	public void setCas(ControladorAltaSocio cas) {
		this.cas = cas;
	}

	public JLabel getLblIngreseNombreDel() {
		return lblIngreseNombreDel;
	}

	public void setLblIngreseNombreDel(JLabel lblIngreseNombreDel) {
		this.lblIngreseNombreDel = lblIngreseNombreDel;
	}

	public JLabel getLblIngreseApellidosDel() {
		return lblIngreseApellidosDel;
	}

	public void setLblIngreseApellidosDel(JLabel lblIngreseApellidosDel) {
		this.lblIngreseApellidosDel = lblIngreseApellidosDel;
	}

	public JLabel getLblCalle() {
		return lblCalle;
	}

	public void setLblCalle(JLabel lblCalle) {
		this.lblCalle = lblCalle;
	}


	public JLabel getLbl_dni() {
		return lbl_dni;
	}

	public void setLbl_dni(JLabel lbl_dni) {
		this.lbl_dni = lbl_dni;
	}

	public JLabel getLblIngreseNumeroTelefonico() {
		return lblIngreseNumeroTelefonico;
	}

	public void setLblIngreseNumeroTelefonico(JLabel lblIngreseNumeroTelefonico) {
		this.lblIngreseNumeroTelefonico = lblIngreseNumeroTelefonico;
	}

	public JLabel getLblIngresoDeFecha() {
		return lblIngresoDeFecha;
	}

	public void setLblIngresoDeFecha(JLabel lblIngresoDeFecha) {
		this.lblIngresoDeFecha = lblIngresoDeFecha;
	}

	public JComboBox getCmb_dia() {
		return cmb_dia;
	}

	public void setCmb_dia(JComboBox cmb_dia) {
		this.cmb_dia = cmb_dia;
	}

	public JComboBox getCmb_anio() {
		return cmb_anio;
	}

	public void setCmb_anio(JComboBox cmb_anio) {
		this.cmb_anio = cmb_anio;
	}

	public JComboBox getCbm_mes() {
		return cbm_mes;
	}

	public void setCbm_mes(JComboBox cbm_mes) {
		this.cbm_mes = cbm_mes;
	}
}
