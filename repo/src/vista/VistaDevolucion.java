package vista;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;

import controlador.ControladorVistaDevolucion;

public class VistaDevolucion extends JInternalFrame {
	private final String ruta = System.getProperty("user.dir");
	private JTextField txtNroSocio;
	private JTextField txtApeNomSocio;
	private JTextField txtCodPelicula;
	private JTextField txtTituloPelicula;
	private JTextField txtFecInicio;
	private JTextField txtFecDev;
	private JButton btnRealizarDevolucion;
	private JButton btnAlquileres;
	private ControladorVistaDevolucion cvd;
	public VistaDevolucion(ControladorVistaDevolucion cvd) {
		this.setCvd(cvd);
		setClosable(true);
		setIconifiable(true);
		addInternalFrameListener(getCvd());
		setName("Peliculas");
		setBounds(100, 100, 1029, 619);
		getContentPane().setLayout(null);
				crearVista();
	}

	private void crearVista() {

		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, new Color(255, 255, 255), new Color(255, 255, 255)), "Socio", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 255, 255)));
		panel.setBounds(150, 36, 726, 96);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNroSocio = new JLabel("NRO SOCIO:");
		lblNroSocio.setForeground(Color.WHITE);
		lblNroSocio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNroSocio.setBounds(10, 22, 104, 29);
		panel.add(lblNroSocio);
		
		txtNroSocio = new JTextField();
		txtNroSocio.setBounds(124, 22, 127, 29);
		panel.add(txtNroSocio);
		txtNroSocio.setColumns(10);
		
		JLabel lblApellidoYNombre = new JLabel("APELLIDO Y NOMBRE:");
		lblApellidoYNombre.setForeground(Color.WHITE);
		lblApellidoYNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblApellidoYNombre.setBounds(261, 22, 149, 29);
		panel.add(lblApellidoYNombre);
		
		txtApeNomSocio = new JTextField();
		txtApeNomSocio.setColumns(10);
		txtApeNomSocio.setBounds(420, 22, 259, 29);
		panel.add(txtApeNomSocio);
		
		JPanel panelPelicula = new JPanel();
		panelPelicula.setOpaque(false);
		panelPelicula.setLayout(null);
		panelPelicula.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, new Color(255, 255, 255), new Color(255, 255, 255)), "Datos Pelicula", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 255, 255)));
		panelPelicula.setBounds(150, 154, 726, 96);
		getContentPane().add(panelPelicula);
		
		JLabel lblCodPelicula = new JLabel("COD PELICULA:");
		lblCodPelicula.setForeground(Color.WHITE);
		lblCodPelicula.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCodPelicula.setBounds(10, 22, 104, 29);
		panelPelicula.add(lblCodPelicula);
		
		txtCodPelicula = new JTextField();
		txtCodPelicula.setColumns(10);
		txtCodPelicula.setBounds(124, 22, 127, 29);
		panelPelicula.add(txtCodPelicula);
		
		JLabel lblPelicula = new JLabel("PELICULA:");
		lblPelicula.setForeground(Color.WHITE);
		lblPelicula.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPelicula.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPelicula.setBounds(261, 22, 149, 29);
		panelPelicula.add(lblPelicula);
		
		txtTituloPelicula = new JTextField();
		txtTituloPelicula.setColumns(10);
		txtTituloPelicula.setBounds(420, 22, 259, 29);
		panelPelicula.add(txtTituloPelicula);
		
		JPanel panelFechas = new JPanel();
		panelFechas.setOpaque(false);
		panelFechas.setLayout(null);
		panelFechas.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.RAISED, new Color(255, 255, 255), new Color(255, 255, 255)), "Datos Pelicula", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 255, 255)));
		panelFechas.setBounds(150, 273, 726, 96);
		getContentPane().add(panelFechas);
		
		JLabel lblFechaInicio = new JLabel("FECHA INICIO:");
		lblFechaInicio.setForeground(Color.WHITE);
		lblFechaInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFechaInicio.setBounds(10, 22, 104, 29);
		panelFechas.add(lblFechaInicio);
		
		txtFecInicio = new JTextField();
		txtFecInicio.setColumns(10);
		txtFecInicio.setBounds(124, 22, 127, 29);
		panelFechas.add(txtFecInicio);
		
		JLabel lblFechaDevolucion = new JLabel("FECHA DEVOLUCION:");
		lblFechaDevolucion.setForeground(Color.WHITE);
		lblFechaDevolucion.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFechaDevolucion.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFechaDevolucion.setBounds(261, 22, 149, 29);
		panelFechas.add(lblFechaDevolucion);
		
		txtFecDev = new JTextField();
		txtFecDev.setColumns(10);
		txtFecDev.setBounds(420, 22, 259, 29);
		panelFechas.add(txtFecDev);
		
		btnAlquileres = new JButton("ALQUILERES");
		btnAlquileres.setBackground(new Color(0, 153, 51));
		btnAlquileres.setForeground(Color.white);
		btnAlquileres.setBorder(null);
		btnAlquileres.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnAlquileres.setOpaque(true);
		btnAlquileres.addActionListener(getCvd());
		btnAlquileres.setBounds(150, 380, 160, 30);
		getContentPane().add(btnAlquileres);
		
		btnRealizarDevolucion = new JButton("REALIZAR DEVOLUCION");
		btnRealizarDevolucion.setVisible(false);
		btnRealizarDevolucion.setBackground(new Color(0, 153, 51));
		btnRealizarDevolucion.setForeground(Color.white);
		btnRealizarDevolucion.setBorder(null);
		btnRealizarDevolucion.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnRealizarDevolucion.setOpaque(true);
		btnRealizarDevolucion.addActionListener(getCvd());
		btnRealizarDevolucion.setBounds(716, 380, 160, 30);
		getContentPane().add(btnRealizarDevolucion);
		
		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 1029, 619);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance(1029, 619, Image.SCALE_DEFAULT)));
	
	}
	public void completarCampos(ArrayList<String> listado){
		this.getTxtNroSocio().setText(listado.get(0));
		this.getTxtApeNomSocio().setText(listado.get(1));
		this.getTxtCodPelicula().setText(listado.get(2));
		this.getTxtTituloPelicula().setText(listado.get(3));
		this.getTxtFecInicio().setText(listado.get(4));
		this.getTxtFecDev().setText(listado.get(5));
		
	}
	public JTextField getTxtNroSocio() {
		return txtNroSocio;
	}

	public void setTxtNroSocio(JTextField txtNroSocio) {
		this.txtNroSocio = txtNroSocio;
	}

	public JTextField getTxtApeNomSocio() {
		return txtApeNomSocio;
	}

	public void setTxtApeNomSocio(JTextField txtApeNomSocio) {
		this.txtApeNomSocio = txtApeNomSocio;
	}

	public JTextField getTxtCodPelicula() {
		return txtCodPelicula;
	}

	public void setTxtCodPelicula(JTextField txtCodPelicula) {
		this.txtCodPelicula = txtCodPelicula;
	}

	public JTextField getTxtTituloPelicula() {
		return txtTituloPelicula;
	}

	public void setTxtTituloPelicula(JTextField txtTituloPelicula) {
		this.txtTituloPelicula = txtTituloPelicula;
	}

	public JTextField getTxtFecInicio() {
		return txtFecInicio;
	}

	public void setTxtFecInicio(JTextField txtFecInicio) {
		this.txtFecInicio = txtFecInicio;
	}

	public JTextField getTxtFecDev() {
		return txtFecDev;
	}

	public void setTxtFecDev(JTextField txtFecDev) {
		this.txtFecDev = txtFecDev;
	}

	public JButton getBtnRealizarDevolucion() {
		return btnRealizarDevolucion;
	}

	public void setBtnRealizarDevolucion(JButton btnRealizarDevolucion) {
		this.btnRealizarDevolucion = btnRealizarDevolucion;
	}

	public JButton getBtnAlquileres() {
		return btnAlquileres;
	}

	public void setBtnAlquileres(JButton btnAlquileres) {
		this.btnAlquileres = btnAlquileres;
	}

	public ControladorVistaDevolucion getCvd() {
		return cvd;
	}

	public void setCvd(ControladorVistaDevolucion cvd) {
		this.cvd = cvd;
	}
}
