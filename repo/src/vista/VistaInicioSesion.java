package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class VistaInicioSesion extends JFrame {

	private JPanel contentPane;
	private JLabel lblFondo = new JLabel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaInicioSesion frame = new VistaInicioSesion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaInicioSesion() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(350, 150, 650, 400);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		String ruta = System.getProperty("user.dir");
		Image preview = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		lblFondo.setBounds(0, 0, 634, 361);
		ImageIcon icon = new ImageIcon(
				preview.getScaledInstance(lblFondo.getWidth(), lblFondo.getHeight(), Image.SCALE_DEFAULT));
		lblFondo.setIcon(icon);
		contentPane.add(lblFondo);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 51, 614, 283);
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblCorreoElectronico = new JLabel("CORREO ELECTRONICO");
		lblCorreoElectronico.setBounds(59, 54, 290, 50);
		panel.add(lblCorreoElectronico);
	}
	
	
	
	
}
