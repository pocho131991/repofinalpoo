package vista;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jdesktop.swingx.border.DropShadowBorder;

import controlador.ControladorVistaMenu;

import javax.swing.JButton;
import java.awt.Font;

public class VistaMenu extends JFrame {

	private JPanel contentPane;
	private final String ruta = System.getProperty("user.dir");
	private JPanel panelMenu;
	private JButton btnMenu;
	private JButton btnPelicula;
	private JButton btnSocio;
	private JButton btnAlquiler;
	private JButton btnMultas;
	private JPanel panelContenedor;
	private ControladorVistaMenu cvm;
	private JButton btnUsuario;
	private JButton btnReportes;
	private JButton btnDevolucion;

	
	public VistaMenu(ControladorVistaMenu cvm) {
		this.setCvm(cvm);
		
		
		setTitle("Vista Principal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1350, 730);
		this.setLocationRelativeTo(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(Color.white);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panelMenu = new JPanel();
		panelMenu.setBackground(Color.WHITE);
		panelMenu.setBounds(0, 0, 204, 711);
		contentPane.add(panelMenu);
		panelMenu.setLayout(null);
        DropShadowBorder dropShadowBorder1 = new DropShadowBorder();
        dropShadowBorder1.setShowBottomShadow(false);
        panelMenu.setBorder(dropShadowBorder1);

		
		
		JLabel lblLogoUsuario = new JLabel("");
		lblLogoUsuario.setBounds(43, 29, 100, 100);
		panelMenu.add(lblLogoUsuario);
        Image preview = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\icono-user.png");
        lblLogoUsuario.setIcon(new ImageIcon(preview.getScaledInstance(lblLogoUsuario.getWidth(), lblLogoUsuario.getHeight(), Image.SCALE_DEFAULT)));
        
        btnPelicula = new JButton("PELICULA");
        btnPelicula.addActionListener(getCvm());
        btnPelicula.setFont(new Font("Sitka Small", Font.PLAIN, 22));
        btnPelicula.setBounds(10, 140, 173, 43);
        panelMenu.add(btnPelicula);
        btnPelicula.setBackground(new Color(0, 153, 204));
        btnPelicula.setForeground(Color.white);
        btnPelicula.setBorder(null);
        btnPelicula.setCursor(new Cursor(Cursor.HAND_CURSOR));
        btnPelicula.setOpaque(true);
        
        btnSocio = new JButton("SOCIO");
        btnSocio.addActionListener(getCvm());
        btnSocio.setOpaque(true);
        btnSocio.setForeground(Color.WHITE);
        btnSocio.setFont(new Font("Sitka Small", Font.PLAIN, 22));
        btnSocio.setBorder(null);
        btnSocio.setBackground(new Color(0, 153, 204));
        btnSocio.setBounds(10, 194, 173, 43);
        panelMenu.add(btnSocio);
        
        btnAlquiler = new JButton("ALQUILAR");
        btnAlquiler.addActionListener(getCvm());
        btnAlquiler.setOpaque(true);
        btnAlquiler.setForeground(Color.WHITE);
        btnAlquiler.setFont(new Font("Sitka Small", Font.PLAIN, 22));
        btnAlquiler.setBorder(null);
        btnAlquiler.setBackground(new Color(0, 153, 204));
        btnAlquiler.setBounds(10, 248, 173, 43);
        panelMenu.add(btnAlquiler);
        
        btnMultas = new JButton("MULTAS");
        btnMultas.addActionListener(getCvm());
        btnMultas.setOpaque(true);
        btnMultas.setForeground(Color.WHITE);
        btnMultas.setFont(new Font("Sitka Small", Font.PLAIN, 22));
        btnMultas.setBorder(null);
        btnMultas.setBackground(new Color(0, 153, 204));
        btnMultas.setBounds(10, 302, 173, 43);
        panelMenu.add(btnMultas);
        
        btnUsuario = new JButton("USUARIO");
        btnUsuario.addActionListener(getCvm());
        btnUsuario.setOpaque(true);
        btnUsuario.setForeground(Color.WHITE);
        btnUsuario.setFont(new Font("Sitka Small", Font.PLAIN, 22));
        btnUsuario.setBorder(null);
        btnUsuario.setBackground(new Color(0, 153, 204));
        btnUsuario.setBounds(10, 356, 173, 43);
        panelMenu.add(btnUsuario);
        
        btnReportes = new JButton("REPORTES");
        btnReportes.addActionListener(getCvm());
        btnReportes.setOpaque(true);
        btnReportes.setForeground(Color.WHITE);
        btnReportes.setFont(new Font("Sitka Small", Font.PLAIN, 22));
        btnReportes.setBorder(null);
        btnReportes.setBackground(new Color(0, 153, 204));
        btnReportes.setBounds(10, 410, 173, 43);
        panelMenu.add(btnReportes);
        
        btnDevolucion = new JButton("DEVOLUCION");
        btnDevolucion.addActionListener(getCvm());
        btnDevolucion.setOpaque(true);
        btnDevolucion.setForeground(Color.WHITE);
        btnDevolucion.setFont(new Font("Sitka Small", Font.PLAIN, 22));
        btnDevolucion.setBorder(null);
        btnDevolucion.setBackground(new Color(0, 153, 204));
        btnDevolucion.setBounds(10, 464, 173, 43);
        panelMenu.add(btnDevolucion);
        
        btnMenu = new JButton("");
        btnMenu.setBounds(232, 0, 50, 50);
        btnMenu.addActionListener(getCvm());
        contentPane.add(btnMenu);
        btnMenu.setBorder(null);
        btnMenu.setContentAreaFilled(false);
        btnMenu.setCursor(new Cursor(Cursor.HAND_CURSOR));
        Image previewBtnMenu = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\icono-menu.png");
        btnMenu.setIcon(new ImageIcon(previewBtnMenu.getScaledInstance(50,50, Image.SCALE_DEFAULT)));
        Image previewBtnMenu1 = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\icono-menu1.png");
        btnMenu.setRolloverIcon(new ImageIcon(previewBtnMenu1.getScaledInstance(50,50, Image.SCALE_DEFAULT)));
        
        panelContenedor = new JPanel();
        panelContenedor.setBackground(Color.WHITE);
        panelContenedor.setBounds(295, 70, 1029, 621);
        contentPane.add(panelContenedor);
        panelContenedor.setLayout(null);
	}


	public JPanel getPanelMenu() {
		return panelMenu;
	}


	public void setPanelMenu(JPanel panelMenu) {
		this.panelMenu = panelMenu;
	}


	public JButton getBtnMenu() {
		return btnMenu;
	}


	public void setBtnMenu(JButton btnMenu) {
		this.btnMenu = btnMenu;
	}


	public JButton getBtnPelicula() {
		return btnPelicula;
	}


	public void setBtnPelicula(JButton btnPelicula) {
		this.btnPelicula = btnPelicula;
	}


	public JButton getBtnSocio() {
		return btnSocio;
	}


	public void setBtnSocio(JButton btnSocio) {
		this.btnSocio = btnSocio;
	}


	public JButton getBtnAlquiler() {
		return btnAlquiler;
	}


	public void setBtnAlquiler(JButton btnAlquiler) {
		this.btnAlquiler = btnAlquiler;
	}


	public JButton getBtnMultas() {
		return btnMultas;
	}


	public void setBtnMultas(JButton btnMultas) {
		this.btnMultas = btnMultas;
	}


	public ControladorVistaMenu getCvm() {
		return cvm;
	}


	public void setCvm(ControladorVistaMenu cvm) {
		this.cvm = cvm;
	}


	public JPanel getPanelContenedor() {
		return panelContenedor;
	}


	public void setPanelContenedor(JPanel panelContenedor) {
		this.panelContenedor = panelContenedor;
	}


	public JButton getBtnUsuario() {
		return btnUsuario;
	}


	public void setBtnUsuario(JButton btnUsuario) {
		this.btnUsuario = btnUsuario;
	}


	public JButton getBtnReportes() {
		return btnReportes;
	}


	public void setBtnReportes(JButton btnReportes) {
		this.btnReportes = btnReportes;
	}


	public JButton getBtnDevolucion() {
		return btnDevolucion;
	}


	public void setBtnDevolucion(JButton btnDevolucion) {
		this.btnDevolucion = btnDevolucion;
	}
}
