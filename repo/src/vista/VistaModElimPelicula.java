package vista;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controlador.ControladorModElimPelicula;
import java.awt.Color;

public class VistaModElimPelicula extends VistaAltaPelicula {

	private static final long serialVersionUID = 1L;
	private JTextField txtCodPeli;
	private ControladorModElimPelicula cmep;

	public VistaModElimPelicula(ControladorModElimPelicula cmep) {
		
		super();
		this.setSize(900, 520);
		this.setCmep(cmep);
		setTitle("Modificar pelicula");
		getBtnGuardar().setLocation(451, 432);
		getBtnVolver().setLocation(597, 432);
		getBtnVolver().addActionListener(getCmep());
		getBtnLimpiar().addActionListener(getCmep());
		getBtnGuardar().addActionListener(getCmep());
		getBtnImaPeli().addActionListener(getCmep());
		getScrollPane().setBounds(545, 217, 332, 85);
		getBtnGuardar().setText("MODIFICAR");
		getBtnLimpiar().setLocation(740, 432);
		JLabel lblNewLabel = new JLabel("CODIGO:");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Sitka Subheading", Font.PLAIN, 16));
		lblNewLabel.setBounds(655, 329, 72, 29);
		getContentPane().add(lblNewLabel);

		txtCodPeli = new JTextField();
		txtCodPeli.setForeground(Color.WHITE);
		txtCodPeli.setEnabled(false);
		txtCodPeli.setBounds(726, 329, 148, 29);
		getContentPane().add(txtCodPeli);
		txtCodPeli.setColumns(10);
	}
	public void completarCampos(ArrayList<String> seleccion) {
			
		this.getTxttitulo().setText(seleccion.get(0));
		this.getTxttitulolat().setText(seleccion.get(1));
		this.getTxtduracion().setText(seleccion.get(2));
		this.getTxtanio().setText(seleccion.get(3));
		this.getTxtdirector().setText(seleccion.get(4));
		this.getTxtAActoresP().setText(seleccion.get(5));
		this.getTxtCodPeli().setText(seleccion.get(6));
		this.getTxtgenero().setText(seleccion.get(7));
		this.getTxtSinopsis().setText(seleccion.get(8));
		this.getTxtCantidad().setText(seleccion.get(9));
		this.getTxtPrecio().setText(seleccion.get(10));
	}
	


	public JTextField getTxtCodPeli() {
		return txtCodPeli;
	}

	public void setTxtCodPeli(JTextField txtCodPeli) {
		this.txtCodPeli = txtCodPeli;
	}

	public ControladorModElimPelicula getCmep() {
		return cmep;
	}

	public void setCmep(ControladorModElimPelicula cmep) {
		this.cmep = cmep;
	}


}
