package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.regex.Pattern;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorVistaMulta;
import modelo.DAOMulta;
import modelo.Multa;
import tabla.Tabla;

public class VistaMulta extends JInternalFrame {

	private final String ruta = System.getProperty("user.dir");
	private static final long serialVersionUID = 1L;
	private JTable tablaMultas;
	private JTextField txtCod_multa;
	private JTextField txtTipo_multa;
	private JTextField txtFecha_multa;
	private JTextField txtCosto;
	private JButton btnAgregarMulta;
	private JButton btnModificarMulta;
	private JTextField txtNroSocioMulta;
	private JComboBox cmbPagado;
	private ControladorVistaMulta cvm;
	private Tabla t = new Tabla();
	private JTextField txtFiltro;

	public VistaMulta(ControladorVistaMulta cvm) {
		this.setCvm(cvm);
		addInternalFrameListener(this.getCvm());
		setClosable(true);
		setIconifiable(true);
		setName("Peliculas");
		setBounds(100, 100, 1029, 619);
		crearVista();
	}

	public void busqueda(String valor) {
		DefaultTableModel model = (DefaultTableModel) this.getTablaMultas().getModel();
		model.setRowCount(0);
		DAOMulta daom= new DAOMulta();
		String pag="";
		String nroSocio="";
		if (!valor.equals("")) {
		for (Multa m : daom.obtenerTodasLasMultasArray()) {
			nroSocio= m.getNro_socio().toString();
			if (m.getPagado()) {
				pag="Pagado";
			} else {
				pag="No pagado";
			}
			
			if (Pattern.compile(".*" + valor + ".*", Pattern.CASE_INSENSITIVE).matcher(m.getApellido()).matches()||
					Pattern.compile(".*" + valor + ".*", Pattern.CASE_INSENSITIVE).matcher(m.getNombre()).matches()||Pattern.compile(".*" + valor + ".*", Pattern.CASE_INSENSITIVE).matcher(nroSocio).matches())
			{
				model.addRow(new Object[]{m.getCod_multa(), m.getNro_socio(), m.getApellido(), m.getNombre(),m.getTipo_multa(),m.getDetalle(),m.getFecha_multa(),pag,m.getCosto()});
				//m.getPagado()
			}
			
		}
		this.getTablaMultas().setModel(model);
		}else {
			t.visualizar_TablaMultas(getTablaMultas());
		}
	}

	private void crearVista() {
		getContentPane().setLayout(null);
		tablaMultas = new JTable();

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 50, 671, 421);
		getContentPane().add(scrollPane);

		tablaMultas.addMouseListener(this.getCvm());
		tablaMultas.setRowHeight(60);
		tablaMultas.setRowSelectionAllowed(true);
		tablaMultas.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaMultas.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		tablaMultas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaMultas.doLayout();
		
		t.visualizar_TablaMultas(tablaMultas);
		scrollPane.setViewportView(tablaMultas);

		JLabel lblMulta_1 = new JLabel("CODIGO MULTA");
		lblMulta_1.setForeground(Color.WHITE);
		lblMulta_1.setBounds(681, 50, 124, 30);
		getContentPane().add(lblMulta_1);

		txtCod_multa = new JTextField();
		txtCod_multa.setEnabled(false);
		txtCod_multa.setBounds(815, 50, 184, 32);
		getContentPane().add(txtCod_multa);
		txtCod_multa.setColumns(10);

		JLabel lblNroSocio = new JLabel("NRO SOCIO");
		lblNroSocio.setForeground(Color.WHITE);
		lblNroSocio.setBounds(681, 91, 124, 30);
		getContentPane().add(lblNroSocio);

		JLabel lblMulta_3 = new JLabel("TIPO MULTA");
		lblMulta_3.setForeground(Color.WHITE);
		lblMulta_3.setBounds(681, 132, 124, 30);
		getContentPane().add(lblMulta_3);

		txtTipo_multa = new JTextField();
		txtTipo_multa.setEnabled(false);
		txtTipo_multa.setColumns(10);
		txtTipo_multa.setBounds(815, 132, 184, 32);
		getContentPane().add(txtTipo_multa);

		JLabel lblMulta_4 = new JLabel("FECHA MULTA");
		lblMulta_4.setForeground(Color.WHITE);
		lblMulta_4.setBounds(681, 173, 124, 30);
		getContentPane().add(lblMulta_4);

		txtFecha_multa = new JTextField();
		txtFecha_multa.setEnabled(false);
		txtFecha_multa.setColumns(10);
		txtFecha_multa.setBounds(815, 173, 184, 32);
		getContentPane().add(txtFecha_multa);

		JLabel lblMulta_6 = new JLabel("PAGADO");
		lblMulta_6.setForeground(Color.WHITE);
		lblMulta_6.setBounds(681, 214, 124, 30);
		getContentPane().add(lblMulta_6);

		JLabel lblMulta_7 = new JLabel("COSTO");
		lblMulta_7.setForeground(Color.WHITE);
		lblMulta_7.setBounds(681, 255, 124, 30);
		getContentPane().add(lblMulta_7);

		txtCosto = new JTextField();
		txtCosto.setEnabled(false);
		txtCosto.setColumns(10);
		txtCosto.setBounds(815, 254, 184, 32);
		getContentPane().add(txtCosto);

		btnAgregarMulta = new JButton("Agregar Multa");
		btnAgregarMulta.addActionListener(getCvm());
		btnAgregarMulta.setBounds(815, 328, 184, 41);
		getContentPane().add(btnAgregarMulta);

		btnModificarMulta = new JButton("Modificar Multa");
		btnModificarMulta.setBounds(815, 380, 184, 41);
		btnModificarMulta.setEnabled(false);
		btnModificarMulta.addActionListener(getCvm());
		getContentPane().add(btnModificarMulta);

		txtNroSocioMulta = new JTextField();
		txtNroSocioMulta.setEnabled(false);
		txtNroSocioMulta.setBounds(815, 93, 184, 32);
		getContentPane().add(txtNroSocioMulta);
		txtNroSocioMulta.setColumns(10);

		cmbPagado = new JComboBox();
		cmbPagado.setEnabled(false);
		cmbPagado.setModel(new DefaultComboBoxModel(new String[] { "Pagado", "No Pagado" }));
		cmbPagado.setBounds(815, 214, 184, 30);
		getContentPane().add(cmbPagado);

		JLabel lblNewLabel_1 = new JLabel("Seleccione un fila haciendo doble click para poder editarla");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(250, 18, 425, 30);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblBuscar = new JLabel("Buscar:");
		lblBuscar.setForeground(Color.WHITE);
		lblBuscar.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBuscar.setBounds(0, 25, 78, 22);
		getContentPane().add(lblBuscar);
		
		txtFiltro = new JTextField();
		txtFiltro.setOpaque(false);
		txtFiltro.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtFiltro.setForeground(Color.WHITE);
		txtFiltro.addKeyListener(getCvm());
		txtFiltro.setBounds(88, 26, 152, 22);
		getContentPane().add(txtFiltro);
		txtFiltro.setColumns(10);
		
		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 1029, 619);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance(1029, 619, Image.SCALE_DEFAULT)));

	}

	public JTable getTablaMultas() {
		return tablaMultas;
	}

	public void setTablaMultas(JTable tablaMultas) {
		this.tablaMultas = tablaMultas;
	}

	public JTextField getTxtCod_multa() {
		return txtCod_multa;
	}

	public void setTxtCod_multa(JTextField txtCod_multa) {
		this.txtCod_multa = txtCod_multa;
	}

	public JTextField getTxtTipo_multa() {
		return txtTipo_multa;
	}

	public void setTxtTipo_multa(JTextField txtTipo_multa) {
		this.txtTipo_multa = txtTipo_multa;
	}

	public JTextField getTxtFecha_multa() {
		return txtFecha_multa;
	}

	public void setTxtFecha_multa(JTextField txtFecha_multa) {
		this.txtFecha_multa = txtFecha_multa;
	}

	public JTextField getTxtCosto() {
		return txtCosto;
	}

	public void setTxtCosto(JTextField txtCosto) {
		this.txtCosto = txtCosto;
	}

	public JButton getBtnAgregarMulta() {
		return btnAgregarMulta;
	}

	public void setBtnAgregarMulta(JButton btnAgregarMulta) {
		this.btnAgregarMulta = btnAgregarMulta;
	}

	public JButton getBtnModificarMulta() {
		return btnModificarMulta;
	}

	public void setBtnModificarMulta(JButton btnModificarMulta) {
		this.btnModificarMulta = btnModificarMulta;
	}

	public JTextField getTxtNroSocioMulta() {
		return txtNroSocioMulta;
	}

	public void setTxtNroSocioMulta(JTextField txtNroSocioMulta) {
		this.txtNroSocioMulta = txtNroSocioMulta;
	}

	public JComboBox getCmbPagado() {
		return cmbPagado;
	}

	public void setCmbPagado(JComboBox cmbPagado) {
		this.cmbPagado = cmbPagado;
	}

	public ControladorVistaMulta getCvm() {
		return cvm;
	}

	public void setCvm(ControladorVistaMulta cvm) {
		this.cvm = cvm;
	}

	public JTextField getTxtFiltro() {
		return txtFiltro;
	}

	public void setTxtFiltro(JTextField txtFiltro) {
		this.txtFiltro = txtFiltro;
	}
}
