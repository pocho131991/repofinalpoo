package vista;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorVistaPelicula;
import modelo.DAOPeliculasImpl;
import modelo.Pelicula;
import tabla.Tabla;

public class VistaPelicula extends JInternalFrame {

	private JTable tablaPelicula;

	private Tabla t = new Tabla();
	private JButton btnEliminarPelicula;
	private JButton btnModificarPelicula;
	private JButton btnAgregarPelicula;
	private JTextField txtFiltroPelicula;
	private JLabel lblPortada;
	private JButton btnActualizar;
	private JTextArea txtASinopsis;
	private JTextArea txtAActores;
	private ControladorVistaPelicula cvp;
	static String x;
	private final String ruta = System.getProperty("user.dir");

	public VistaPelicula(ControladorVistaPelicula cvp) {
		this.setCvp(cvp);
		
		addInternalFrameListener(getCvp());
		setClosable(true);
		setIconifiable(true);
		setName("Peliculas");
		setBounds(100, 100, 1029, 619);
		crearVista();
	}

	private void crearVista() {
		tablaPelicula = new JTable();
		tablaPelicula.addMouseListener(this.getCvp());
		tablaPelicula.setRowHeight(30);
		tablaPelicula.setRowSelectionAllowed(true);
		tablaPelicula.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaPelicula.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		tablaPelicula.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaPelicula.doLayout();

		getT().visualizar_TablaReducidaPelicula(tablaPelicula);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportView(tablaPelicula);
		getContentPane().setLayout(null);
		scrollPane.setBounds(0, 50, 757, 286);
		getContentPane().add(scrollPane);
		btnEliminarPelicula = new JButton("Eliminar pelicula");
		btnEliminarPelicula.setBackground(new Color(0, 153, 51));
		btnEliminarPelicula.setForeground(Color.white);
		btnEliminarPelicula.setBorder(null);
		btnEliminarPelicula.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnEliminarPelicula.setOpaque(true);
		btnEliminarPelicula.setEnabled(false);
		btnEliminarPelicula.addActionListener(this.getCvp());
		btnEliminarPelicula.setBounds(784, 0, 127, 30);
		
		btnModificarPelicula = new JButton("Modificar pelicula");
		btnModificarPelicula.setBackground(new Color(0, 153, 51));
		btnModificarPelicula.setForeground(Color.white);
		btnModificarPelicula.setBorder(null);
		btnModificarPelicula.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnModificarPelicula.setOpaque(true);
		btnModificarPelicula.setEnabled(false);
		btnModificarPelicula.addActionListener(this.getCvp());
		btnModificarPelicula.setBounds(647, 0, 127, 30);

		getContentPane().add(btnModificarPelicula);
		getContentPane().add(btnEliminarPelicula);

		btnAgregarPelicula = new JButton("Agregar pelicula");
		btnAgregarPelicula.setBackground(new Color(0, 153, 51));
		btnAgregarPelicula.setForeground(Color.white);
		btnAgregarPelicula.setBorder(null);
		btnAgregarPelicula.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnAgregarPelicula.setOpaque(true);
		btnAgregarPelicula.addActionListener(this.getCvp());
		btnAgregarPelicula.setBounds(510, 0, 127, 30);
		getContentPane().add(btnAgregarPelicula);

		JLabel lblBusquedaPelicula = new JLabel("BUSQUEDA:");
		lblBusquedaPelicula.setForeground(Color.WHITE);
		lblBusquedaPelicula.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBusquedaPelicula.setBounds(0, 0, 105, 30);
		getContentPane().add(lblBusquedaPelicula);

		txtFiltroPelicula = new JTextField();
		txtFiltroPelicula.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtFiltroPelicula.setOpaque(false);
		txtFiltroPelicula.setForeground(Color.WHITE);
		txtFiltroPelicula.addKeyListener(this.getCvp());
		txtFiltroPelicula.setBounds(108, 0, 233, 30);
		getContentPane().add(txtFiltroPelicula);
		txtFiltroPelicula.setColumns(10);

		BufferedImage img = null;
		try {
			String ruta = System.getProperty("user.dir");
			img = ImageIO.read(new File(ruta + "\\images\\portada-vacia.jpg"));
			// img =(BufferedImage) escalarImage(img, 566, 543);
		} catch (IOException e) {
			e.printStackTrace();
		}
		lblPortada = new JLabel(new ImageIcon(img));
		lblPortada.setHorizontalAlignment(SwingConstants.CENTER);
		lblPortada.setBounds(767, 50, 236, 286);
		getContentPane().add(lblPortada);

		btnActualizar = new JButton("Actualizar lista");
		btnActualizar.setBounds(373, 0, 127, 30);
		// btnActualizar.addActionListener(getControlador());
		getContentPane().add(btnActualizar);

		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 389, 396, 177);
		getContentPane().add(scrollPane_3);

		txtASinopsis = new JTextArea();
		txtASinopsis.setFont(new Font("Monospaced", Font.PLAIN, 18));
		scrollPane_3.setViewportView(txtASinopsis);
		txtASinopsis.setColumns(20);
		txtASinopsis.setRows(5);
		txtASinopsis.setWrapStyleWord(true);
		txtASinopsis.setLineWrap(true);

		JLabel lblSinopsis = new JLabel("SINOPSIS");
		lblSinopsis.setForeground(Color.WHITE);
		lblSinopsis.setHorizontalAlignment(SwingConstants.CENTER);
		lblSinopsis.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblSinopsis.setBounds(10, 348, 396, 30);
		getContentPane().add(lblSinopsis);

		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(484, 389, 396, 177);
		getContentPane().add(scrollPane_4);

		txtAActores = new JTextArea();
		scrollPane_4.setViewportView(txtAActores);
		txtAActores.setFont(new Font("Monospaced", Font.PLAIN, 18));
		txtAActores.setWrapStyleWord(true);
		txtAActores.setLineWrap(true);
		txtAActores.setRows(5);
		txtAActores.setColumns(20);

		JLabel lblActores = new JLabel("ACTORES");
		lblActores.setForeground(Color.WHITE);
		lblActores.setHorizontalAlignment(SwingConstants.CENTER);
		lblActores.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblActores.setBounds(484, 347, 396, 30);
		getContentPane().add(lblActores);
		
		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 1020, 591);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance(1020, 591, Image.SCALE_DEFAULT)));

	}

	public void busqueda(String valor) {
		DefaultTableModel model = (DefaultTableModel) this.getTablaPelicula().getModel();
		model.setRowCount(0);
		DAOPeliculasImpl daop = new DAOPeliculasImpl();
		String codPelicula="";
		if (!valor.equals("")) {
			for (Pelicula p : daop.buscarPeliculas()) {
				codPelicula=codPelicula+p.getCod_pelicula().toString();
				if (Pattern.compile(".*" + valor + ".*", Pattern.CASE_INSENSITIVE).matcher(p.getTitulo()).matches()||
						Pattern.compile(".*" + valor + ".*", Pattern.CASE_INSENSITIVE).matcher(p.getGenero()).matches()||
						Pattern.compile(".*" + valor + ".*", Pattern.CASE_INSENSITIVE).matcher(p.getTitulo_alterno()).matches()||Pattern.compile(".*" + valor + ".*", Pattern.CASE_INSENSITIVE).matcher(codPelicula).matches())
				{
					model.addRow(new Object[]{
							p.getCod_pelicula(),p.getTitulo(),p.getTitulo_alterno(),p.getAnio(),p.getGenero()
					});
				}
				
			}
			this.getTablaPelicula().setModel(model);
		}else {
			this.getT().visualizar_TablaReducidaPelicula(getTablaPelicula());
		}
		
	}
	
	public JTable getTablaPelicula() {
		return tablaPelicula;
	}

	public void setTablaPelicula(JTable tablaPelicula) {
		this.tablaPelicula = tablaPelicula;
	}

	public JButton getBtnEliminarPelicula() {
		return btnEliminarPelicula;
	}

	public void setBtnEliminarPelicula(JButton btnEliminarPelicula) {
		this.btnEliminarPelicula = btnEliminarPelicula;
	}

	public JButton getBtnModificarPelicula() {
		return btnModificarPelicula;
	}

	public void setBtnModificarPelicula(JButton btnModificarPelicula) {
		this.btnModificarPelicula = btnModificarPelicula;
	}

	public JButton getBtnAgregarPelicula() {
		return btnAgregarPelicula;
	}

	public void setBtnAgregarPelicula(JButton btnAgregarPelicula) {
		this.btnAgregarPelicula = btnAgregarPelicula;
	}

	public JTextField getTxtFiltroPelicula() {
		return txtFiltroPelicula;
	}

	public void setTxtFiltroPelicula(JTextField txtFiltroPelicula) {
		this.txtFiltroPelicula = txtFiltroPelicula;
	}

	public JLabel getLblPortada() {
		return lblPortada;
	}

	public void setLblPortada(JLabel lblPortada) {
		this.lblPortada = lblPortada;
	}

	public JButton getBtnActualizar() {
		return btnActualizar;
	}

	public void setBtnActualizar(JButton btnActualizar) {
		this.btnActualizar = btnActualizar;
	}

	public JTextArea getTxtASinopsis() {
		return txtASinopsis;
	}

	public void setTxtASinopsis(JTextArea txtASinopsis) {
		this.txtASinopsis = txtASinopsis;
	}

	public JTextArea getTxtAActores() {
		return txtAActores;
	}

	public void setTxtAActores(JTextArea txtAActores) {
		this.txtAActores = txtAActores;
	}

	public ControladorVistaPelicula getCvp() {
		return cvp;
	}

	public void setCvp(ControladorVistaPelicula cvp) {
		this.cvp = cvp;
	}

	public Tabla getT() {
		return t;
	}

	public void setT(Tabla t) {
		this.t = t;
	}

}
