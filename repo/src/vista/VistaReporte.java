package vista;

import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import controlador.ControladorVistaReporte;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaReporte extends JInternalFrame {
	private final String ruta = System.getProperty("user.dir");
	private ControladorVistaReporte cvr;
	private JButton btnListadoAlquiler;
	private JFormattedTextField ftxtFechaDesde;
	private JButton btnListadoSociosMultas;

	public VistaReporte(ControladorVistaReporte cvr) {
		this.setCvr(cvr);
		addInternalFrameListener(getCvr());
		setClosable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblFechaDesde = new JLabel("Ingrese fecha desde:");
		lblFechaDesde.setForeground(Color.WHITE);
		lblFechaDesde.setHorizontalAlignment(SwingConstants.TRAILING);
		lblFechaDesde.setBounds(0, 79, 131, 14);
		getContentPane().add(lblFechaDesde);
		
	    btnListadoAlquiler = new JButton("LISTAR ALQUILERES");
	    btnListadoAlquiler.setBackground(new Color(0, 153, 51));
		btnListadoAlquiler.setForeground(Color.white);
		btnListadoAlquiler.setBorder(null);
		btnListadoAlquiler.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnListadoAlquiler.setOpaque(true);
		btnListadoAlquiler.addActionListener(getCvr());
		btnListadoAlquiler.setBounds(236, 75, 188, 23);
		getContentPane().add(btnListadoAlquiler);
		
		try {
			MaskFormatter formatter = new MaskFormatter("##/##/####");
			ftxtFechaDesde = new JFormattedTextField(formatter);
			ftxtFechaDesde.setBounds(132, 76, 86, 20);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getContentPane().add(ftxtFechaDesde);
		
		btnListadoSociosMultas = new JButton("LISTAR SOCIOS C/ MULTA IMPAGA");
		btnListadoSociosMultas.setBackground(new Color(0, 153, 51));
		btnListadoSociosMultas.setForeground(Color.white);
		btnListadoSociosMultas.setBorder(null);
		btnListadoSociosMultas.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnListadoSociosMultas.setOpaque(true);
		btnListadoSociosMultas.addActionListener(getCvr());
		btnListadoSociosMultas.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnListadoSociosMultas.setBounds(236, 106, 188, 23);
		getContentPane().add(btnListadoSociosMultas);
		
		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 444, 270);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance(450, 300, Image.SCALE_DEFAULT)));

	}

	public ControladorVistaReporte getCvr() {
		return cvr;
	}

	public void setCvr(ControladorVistaReporte cvr) {
		this.cvr = cvr;
	}


	public JButton getBtnListadoAlquiler() {
		return btnListadoAlquiler;
	}

	public void setBtnListadoAlquiler(JButton btnListadoAlquiler) {
		this.btnListadoAlquiler = btnListadoAlquiler;
	}

	public JFormattedTextField getFtxtFechaDesde() {
		return ftxtFechaDesde;
	}

	public void setFtxtFechaDesde(JFormattedTextField ftxtFechaDesde) {
		this.ftxtFechaDesde = ftxtFechaDesde;
	}

	public JButton getBtnListadoSociosMultas() {
		return btnListadoSociosMultas;
	}

	public void setBtnListadoSociosMultas(JButton btnListadoSociosMultas) {
		this.btnListadoSociosMultas = btnListadoSociosMultas;
	}
}
