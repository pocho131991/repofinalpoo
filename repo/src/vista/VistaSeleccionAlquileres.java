package vista;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controlador.ControladorVistaSeleccionAlquileres;
import tabla.Tabla;

public class VistaSeleccionAlquileres extends JDialog {

	private static final long serialVersionUID = 1L;
	private JTable tablaSeleccionAlquileres;
	private JTextField txtFiltro;
	private JButton btnSeleccionPelicula;
	private ControladorVistaSeleccionAlquileres cvsa;

	public VistaSeleccionAlquileres(ControladorVistaSeleccionAlquileres cvsa) {
		this.setCvsa(cvsa);
		this.setModal(true);
		setBounds(100, 100, 650, 400);
		getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 49, 614, 285);
		getContentPane().add(scrollPane);

		tablaSeleccionAlquileres = new JTable();
		tablaSeleccionAlquileres.addMouseListener(getCvsa());
		Tabla t = new Tabla();
		t.visualizar_TablaAlquiler(tablaSeleccionAlquileres);
		scrollPane.setViewportView(tablaSeleccionAlquileres);

		JLabel lblBuscar = new JLabel("BUSCAR:");
		lblBuscar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblBuscar.setBounds(10, 10, 88, 30);
		getContentPane().add(lblBuscar);

		txtFiltro = new JTextField();

		txtFiltro.setBounds(108, 8, 121, 30);
		txtFiltro.addKeyListener(getCvsa());
		getContentPane().add(txtFiltro);
		txtFiltro.setColumns(10);

		btnSeleccionPelicula = new JButton("Seleccionar ");
		btnSeleccionPelicula.setEnabled(false);
		btnSeleccionPelicula.setBounds(239, 11, 152, 27);
		btnSeleccionPelicula.addActionListener(this.getCvsa());
		getContentPane().add(btnSeleccionPelicula);

	}

	public JTable getTablaSeleccionAlquileres() {
		return tablaSeleccionAlquileres;
	}

	public void setTablaSeleccionAlquileres(JTable tablaSeleccionAlquileres) {
		this.tablaSeleccionAlquileres = tablaSeleccionAlquileres;
	}

	public JTextField getTxtFiltro() {
		return txtFiltro;
	}

	public void setTxtFiltro(JTextField txtFiltro) {
		this.txtFiltro = txtFiltro;
	}

	public JButton getBtnSeleccionPelicula() {
		return btnSeleccionPelicula;
	}

	public void setBtnSeleccionPelicula(JButton btnSeleccionPelicula) {
		this.btnSeleccionPelicula = btnSeleccionPelicula;
	}

	public ControladorVistaSeleccionAlquileres getCvsa() {
		return cvsa;
	}

	public void setCvsa(ControladorVistaSeleccionAlquileres cvsa) {
		this.cvsa = cvsa;
	}

}
