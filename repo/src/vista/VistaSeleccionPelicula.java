package vista;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controlador.ControladorVistaSeleccionPelicula;
import tabla.Tabla;
import java.awt.Font;
import javax.swing.JButton;

public class VistaSeleccionPelicula extends JDialog {

	private ControladorVistaSeleccionPelicula cvsp;
	private JTable tablaSeleccionPelicula;
	private JTextField txtFiltro;
	private JButton btnSeleccionPelicula;

	public VistaSeleccionPelicula(ControladorVistaSeleccionPelicula cvsp) {
		this.setCvsp(cvsp);
		setBounds(100, 100, 650, 400);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 49, 614, 285);
		getContentPane().add(scrollPane);
		
		tablaSeleccionPelicula = new JTable();
		tablaSeleccionPelicula.addMouseListener(getCvsp());
		Tabla t = new Tabla();
		t.visualizar_TablaPeliculaReducida(tablaSeleccionPelicula);
		scrollPane.setViewportView(tablaSeleccionPelicula);
		
		JLabel lblBuscar = new JLabel("BUSCAR:");
		lblBuscar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblBuscar.setBounds(10, 10, 88, 30);
		getContentPane().add(lblBuscar);
		
		txtFiltro = new JTextField();
		
		txtFiltro.setBounds(108, 8, 121, 30);
		txtFiltro.addKeyListener(getCvsp());
		getContentPane().add(txtFiltro);
		txtFiltro.setColumns(10);
		
		btnSeleccionPelicula = new JButton("Seleccionar ");
		btnSeleccionPelicula.setEnabled(false);
		btnSeleccionPelicula.setBounds(239, 11, 152, 27);
		btnSeleccionPelicula.addActionListener(this.getCvsp());
		getContentPane().add(btnSeleccionPelicula);
		
	}

	public ControladorVistaSeleccionPelicula getCvsp() {
		return cvsp;
	}

	public void setCvsp(ControladorVistaSeleccionPelicula cvsp) {
		this.cvsp = cvsp;
	}

	public JTable getTablaSeleccionPelicula() {
		return tablaSeleccionPelicula;
	}

	public void setTablaSeleccionPelicula(JTable tablaSeleccionPelicula) {
		this.tablaSeleccionPelicula = tablaSeleccionPelicula;
	}

	public JTextField getTxtFiltro() {
		return txtFiltro;
	}

	public void setTxtFiltro(JTextField txtFiltro) {
		this.txtFiltro = txtFiltro;
	}

	public JButton getBtnSeleccionPelicula() {
		return btnSeleccionPelicula;
	}

	public void setBtnSeleccionPelicula(JButton btnSeleccionPelicula) {
		this.btnSeleccionPelicula = btnSeleccionPelicula;
	}
}
