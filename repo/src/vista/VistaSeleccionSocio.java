package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

import controlador.ControladorVistaSeleccionSocio;
import tabla.Tabla;

public class VistaSeleccionSocio extends JDialog {
	
	private ControladorVistaSeleccionSocio cvss;
	private JTable tablaSeleccionSocio;
	private JTextField txtFiltro;
	private JButton btnSeleccionSocio;
	private final String ruta = System.getProperty("user.dir");
	
	public VistaSeleccionSocio(ControladorVistaSeleccionSocio cvss) {
		this.setCvss(cvss);
		setBounds(100, 100, 650, 400);
		getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 49, 557, 285);
		getContentPane().add(scrollPane);
		
		tablaSeleccionSocio = new JTable();
		tablaSeleccionSocio.addMouseListener(getCvss());
		Tabla t = new Tabla();
		t.visualizar_TablaSocio(tablaSeleccionSocio);
		scrollPane.setViewportView(tablaSeleccionSocio);
		
		JLabel lblBuscar = new JLabel("BUSCAR:");
		lblBuscar.setForeground(Color.WHITE);
		lblBuscar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblBuscar.setBounds(10, 10, 88, 30);
		getContentPane().add(lblBuscar);
		
		txtFiltro = new JTextField();
		txtFiltro.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		txtFiltro.setOpaque(false);
		txtFiltro.setForeground(Color.WHITE);
		
		txtFiltro.setBounds(108, 8, 121, 30);
		txtFiltro.addKeyListener(getCvss());
		getContentPane().add(txtFiltro);
		txtFiltro.setColumns(10);
		
		btnSeleccionSocio = new JButton("Seleccionar Socio");
		btnSeleccionSocio.setOpaque(true);
		btnSeleccionSocio.setForeground(Color.WHITE);
		btnSeleccionSocio.setFont(new Font("Sitka Small", Font.PLAIN, 15));
		btnSeleccionSocio.setBorder(null);
		btnSeleccionSocio.setBackground(new Color(0, 153, 51));
		btnSeleccionSocio.setEnabled(false);
		btnSeleccionSocio.setBounds(239, 11, 170, 27);
		btnSeleccionSocio.addActionListener(this.getCvss());
		getContentPane().add(btnSeleccionSocio);
		
		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0,  634, 361);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance( 634, 361, Image.SCALE_DEFAULT)));

		}
	public ControladorVistaSeleccionSocio getCvss() {
		return cvss;
	}
	public void setCvss(ControladorVistaSeleccionSocio cvss) {
		this.cvss = cvss;
	}
	public JTable getTablaSeleccionSocio() {
		return tablaSeleccionSocio;
	}
	public void setTablaSeleccionSocio(JTable tablaSeleccionSocio) {
		this.tablaSeleccionSocio = tablaSeleccionSocio;
	}
	public JTextField getTxtFiltro() {
		return txtFiltro;
	}
	public void setTxtFiltro(JTextField txtFiltro) {
		this.txtFiltro = txtFiltro;
	}
	public JButton getBtnSeleccionSocio() {
		return btnSeleccionSocio;
	}
	public void setBtnSeleccionSocio(JButton btnSeleccionSocio) {
		this.btnSeleccionSocio = btnSeleccionSocio;
	}
	}


