package vista;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

import controlador.ControladorVistaSocio;
import tabla.Tabla;

public class VistaSocio extends JInternalFrame {

	private final String ruta = System.getProperty("user.dir");
	private JTable tablaSocio;
	private JTextField txtNumSoc;
	private JTextField txtDniSoc;
	private JTextField txt_nombre_socio;
	private JTextField txt_apellido_socio;
	private JTextField txt_direccion;
	private JTextField txt_tel;
	private JButton btnAgregarSocio;
	private JButton btnModificarSocio;
	private JButton btnEliminarSocio;
	private JTextField txtFiltro;
	private JTextField txt_fecnac;
	private ControladorVistaSocio cvs;

	public VistaSocio(ControladorVistaSocio cvs) {
		this.setCvs(cvs);
		setClosable(true);
		setIconifiable(true);
		addInternalFrameListener(getCvs());
		setName("Socio");
		setBounds(100, 100, 1029, 619);
		getContentPane().setLayout(null);
		crearVista();
	}

	private void crearVista() {
		tablaSocio = new JTable();
		tablaSocio.addMouseListener(this.getCvs());
		tablaSocio.setRowHeight(60);
		tablaSocio.setRowSelectionAllowed(true);
		tablaSocio.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaSocio.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		tablaSocio.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaSocio.doLayout();
		Tabla t = new Tabla();
		t.visualizar_TablaSocio(tablaSocio);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setViewportView(tablaSocio);
		
		scrollPane.setBounds(0, 50, 630, 500);
		getContentPane().add(scrollPane);

		txtNumSoc = new JTextField();
		txtNumSoc.setEnabled(false);
		txtNumSoc.setBounds(853, 50, 150, 30);
		getContentPane().add(txtNumSoc);
		txtNumSoc.setColumns(10);

		JLabel lblCodigoSocio = new JLabel("NUMERO SOCIO:");
		lblCodigoSocio.setForeground(Color.WHITE);
		lblCodigoSocio.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCodigoSocio.setBounds(681, 50, 162, 30);
		getContentPane().add(lblCodigoSocio);

		JLabel lblDni = new JLabel("DNI:");
		lblDni.setForeground(Color.WHITE);
		lblDni.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDni.setBounds(723, 91, 120, 30);
		getContentPane().add(lblDni);

		txtDniSoc = new JTextField();
		txtDniSoc.setColumns(10);
		txtDniSoc.setBounds(853, 91, 151, 30);
		getContentPane().add(txtDniSoc);

		txt_nombre_socio = new JTextField();
		txt_nombre_socio.setColumns(10);
		txt_nombre_socio.setBounds(852, 132, 151, 30);
		getContentPane().add(txt_nombre_socio);

		JLabel lblNombre = new JLabel("NOMBRE:");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNombre.setBounds(703, 132, 140, 30);
		getContentPane().add(lblNombre);

		JLabel lblApellido = new JLabel("APELLIDO:");
		lblApellido.setForeground(Color.WHITE);
		lblApellido.setHorizontalAlignment(SwingConstants.RIGHT);
		lblApellido.setBounds(703, 173, 140, 30);
		getContentPane().add(lblApellido);

		JLabel lblDireccion = new JLabel("DIRECCION:");
		lblDireccion.setForeground(Color.WHITE);
		lblDireccion.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDireccion.setBounds(714, 214, 129, 30);
		getContentPane().add(lblDireccion);

		JLabel lblTelefono = new JLabel("TELEFONO:");
		lblTelefono.setForeground(Color.WHITE);
		lblTelefono.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTelefono.setBounds(693, 296, 150, 30);
		getContentPane().add(lblTelefono);

		txt_apellido_socio = new JTextField();
		txt_apellido_socio.setColumns(10);
		txt_apellido_socio.setBounds(853, 173, 150, 30);
		getContentPane().add(txt_apellido_socio);

		txt_direccion = new JTextField();
		txt_direccion.setColumns(10);
		txt_direccion.setBounds(853, 214, 150, 30);
		getContentPane().add(txt_direccion);

		txt_tel = new JTextField();
		txt_tel.setColumns(10);
		txt_tel.setBounds(853, 296, 150, 30);
		getContentPane().add(txt_tel);

		JLabel lblFechaDeNacimiento = new JLabel("FECHA DE NACIMIENTO:");
		lblFechaDeNacimiento.setForeground(Color.WHITE);
		lblFechaDeNacimiento.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFechaDeNacimiento.setBounds(671, 255, 172, 30);
		getContentPane().add(lblFechaDeNacimiento);

		txt_fecnac = new JTextField();
		txt_fecnac.setColumns(10);
		txt_fecnac.setBounds(853, 255, 150, 30);
		getContentPane().add(txt_fecnac);

		btnAgregarSocio = new JButton("Agregar Socio");
		btnAgregarSocio.setBackground(new Color(0, 153, 51));
		btnAgregarSocio.setForeground(Color.white);
		btnAgregarSocio.setBorder(null);
		btnAgregarSocio.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnAgregarSocio.setOpaque(true);
		btnAgregarSocio.setBounds(781, 462, 222, 22);
		btnAgregarSocio.addActionListener(this.getCvs());
		getContentPane().add(btnAgregarSocio);

		btnModificarSocio = new JButton("Modificar Socio");
		btnModificarSocio.setBackground(new Color(0, 153, 51));
		btnModificarSocio.setForeground(Color.white);
		btnModificarSocio.setBorder(null);
		btnModificarSocio.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnModificarSocio.setOpaque(true);
		btnModificarSocio.addActionListener(this.getCvs());
		btnModificarSocio.setBounds(781, 495, 222, 22);
		getContentPane().add(btnModificarSocio);

		btnEliminarSocio = new JButton("Eliminar Socio");
		btnEliminarSocio.setBackground(new Color(0, 153, 51));
		btnEliminarSocio.setForeground(Color.white);
		btnEliminarSocio.setBorder(null);
		btnEliminarSocio.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnEliminarSocio.setOpaque(true);
		btnEliminarSocio.setBounds(781, 528, 222, 22);
		btnEliminarSocio.addActionListener(this.getCvs());
		getContentPane().add(btnEliminarSocio);

		JLabel lblBusqueda = new JLabel("BUSQUEDA :");
		lblBusqueda.setForeground(Color.WHITE);
		lblBusqueda.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBusqueda.setBounds(0, 0, 108, 30);
		getContentPane().add(lblBusqueda);

		txtFiltro = new JTextField();
		txtFiltro.addKeyListener(this.getCvs());
		txtFiltro.setBounds(118, 0, 256, 30);
		getContentPane().add(txtFiltro);
		txtFiltro.setColumns(10);
		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 1020, 591);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance(1020, 591, Image.SCALE_DEFAULT)));

	}

	public JTable getTablaSocio() {
		return tablaSocio;
	}

	public void setTablaSocio(JTable tablaSocio) {
		this.tablaSocio = tablaSocio;
	}

	public JTextField getTxtNumSoc() {
		return txtNumSoc;
	}

	public void setTxtNumSoc(JTextField txtNumSoc) {
		this.txtNumSoc = txtNumSoc;
	}

	public JTextField getTxtDniSoc() {
		return txtDniSoc;
	}

	public void setTxtDniSoc(JTextField txtDniSoc) {
		this.txtDniSoc = txtDniSoc;
	}

	public JTextField getTxt_nombre_socio() {
		return txt_nombre_socio;
	}

	public void setTxt_nombre_socio(JTextField txt_nombre_socio) {
		this.txt_nombre_socio = txt_nombre_socio;
	}

	public JTextField getTxt_apellido_socio() {
		return txt_apellido_socio;
	}

	public void setTxt_apellido_socio(JTextField txt_apellido_socio) {
		this.txt_apellido_socio = txt_apellido_socio;
	}

	public JTextField getTxt_direccion() {
		return txt_direccion;
	}

	public void setTxt_direccion(JTextField txt_direccion) {
		this.txt_direccion = txt_direccion;
	}

	public JTextField getTxt_tel() {
		return txt_tel;
	}

	public void setTxt_tel(JTextField txt_tel) {
		this.txt_tel = txt_tel;
	}

	public JButton getBtnAgregarSocio() {
		return btnAgregarSocio;
	}

	public void setBtnAgregarSocio(JButton btnAgregarSocio) {
		this.btnAgregarSocio = btnAgregarSocio;
	}

	public JButton getBtnModificarSocio() {
		return btnModificarSocio;
	}

	public void setBtnModificarSocio(JButton btnModificarSocio) {
		this.btnModificarSocio = btnModificarSocio;
	}

	public JButton getBtnEliminarSocio() {
		return btnEliminarSocio;
	}

	public void setBtnEliminarSocio(JButton btnEliminarSocio) {
		this.btnEliminarSocio = btnEliminarSocio;
	}

	public JTextField getTxtFiltro() {
		return txtFiltro;
	}

	public void setTxtFiltro(JTextField txtFiltro) {
		this.txtFiltro = txtFiltro;
	}

	public JTextField getTxt_fecnac() {
		return txt_fecnac;
	}

	public void setTxt_fecnac(JTextField txt_fecnac) {
		this.txt_fecnac = txt_fecnac;
	}

	public ControladorVistaSocio getCvs() {
		return cvs;
	}

	public void setCvs(ControladorVistaSocio cvs) {
		this.cvs = cvs;
	}

}
