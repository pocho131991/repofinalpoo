package vista;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;

import controlador.ControladorVistaUsuario;
import tabla.Tabla;

public class VistaUsuario extends JInternalFrame {
	private final String ruta = System.getProperty("user.dir");
	private JTable tablaUsuario;
	private JTextField txtIDUsuario;
	private JTextField txtNomUsuario;
	private JTextField txtApeUsuario;
	private JTextField txtEmailUsuario;
	private JPasswordField passfieldContra;
	private JTextField txtFecNacUsuario;
	private JButton btnModificarUsuario;
	private JButton btnEliminarUsuario;
	private JLabel lblExcepcionUsuarioNombre;
	private JLabel lblExcepcionUsuarioApellido;
	private JLabel lblExcepcionUsuarioEmail;
	private JLabel lblExcepcionUsuarioContrasenia;
	private JLabel lblExcepcionUsuarioFecNac;
	private ControladorVistaUsuario cvu;

	public VistaUsuario(ControladorVistaUsuario cvu) {
		this.setCvu(cvu);
		setClosable(true);
		addInternalFrameListener(getCvu());
		setBounds(100, 100, 1029, 621);

		crearVista();
	}

	private void crearVista() {
		getContentPane().setLayout(null);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 50, 653, 321);
		getContentPane().add(scrollPane);

		tablaUsuario = new JTable();
		tablaUsuario.addMouseListener(this.getCvu());
		tablaUsuario.setRowHeight(60);
		tablaUsuario.setRowSelectionAllowed(true);
		tablaUsuario.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaUsuario.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		tablaUsuario.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaUsuario.doLayout();
		Tabla t = new Tabla();
		t.visualizar_TablaUsuario(tablaUsuario);
		scrollPane.setViewportView(tablaUsuario);

		txtIDUsuario = new JTextField();
		txtIDUsuario.setBounds(663, 87, 158, 32);
		getContentPane().add(txtIDUsuario);
		txtIDUsuario.setColumns(10);

		JLabel lblNewLabel = new JLabel("ID USUARIO");
		lblNewLabel.setBounds(663, 50, 124, 30);
		getContentPane().add(lblNewLabel);

		txtNomUsuario = new JTextField();
		txtNomUsuario.setColumns(10);
		txtNomUsuario.setBounds(845, 87, 158, 32);
		getContentPane().add(txtNomUsuario);

		JLabel lblNombre_1 = new JLabel("NOMBRE");
		lblNombre_1.setBounds(845, 50, 124, 30);
		getContentPane().add(lblNombre_1);

		JLabel lblApellido_1 = new JLabel("APELLIDO");
		lblApellido_1.setBounds(663, 130, 124, 30);
		getContentPane().add(lblApellido_1);

		JLabel lblEmail = new JLabel("EMAIL");
		lblEmail.setBounds(845, 130, 124, 30);
		getContentPane().add(lblEmail);

		txtApeUsuario = new JTextField();
		txtApeUsuario.setColumns(10);
		txtApeUsuario.setBounds(663, 171, 158, 32);
		getContentPane().add(txtApeUsuario);

		txtEmailUsuario = new JTextField();
		txtEmailUsuario.setColumns(10);
		txtEmailUsuario.setBounds(845, 171, 159, 32);
		getContentPane().add(txtEmailUsuario);

		JLabel lblContrasea = new JLabel("CONTRASE\u00D1A");
		lblContrasea.setBounds(663, 214, 124, 30);
		getContentPane().add(lblContrasea);

		passfieldContra = new JPasswordField();
		passfieldContra.setBounds(663, 255, 158, 32);
		getContentPane().add(passfieldContra);

		JLabel lblFechaDeNacimiento_1 = new JLabel("FECHA DE NACIMIENTO");
		lblFechaDeNacimiento_1.setBounds(845, 214, 124, 30);
		getContentPane().add(lblFechaDeNacimiento_1);

		txtFecNacUsuario = new JTextField();
		txtFecNacUsuario.setColumns(10);
		txtFecNacUsuario.setBounds(845, 255, 159, 32);
		getContentPane().add(txtFecNacUsuario);

		btnModificarUsuario = new JButton("Modificar Usuario");
		btnModificarUsuario.addActionListener(getCvu());
		btnModificarUsuario.setBounds(663, 474, 158, 41);
		getContentPane().add(btnModificarUsuario);

		btnEliminarUsuario = new JButton("Eliminar Usuario");
        btnEliminarUsuario.addActionListener(getCvu());
		btnEliminarUsuario.setBounds(845, 474, 158, 41);
		getContentPane().add(btnEliminarUsuario);

		lblExcepcionUsuarioNombre = new JLabel("");
		lblExcepcionUsuarioNombre.setForeground(Color.RED);
		lblExcepcionUsuarioNombre.setBounds(663, 298, 340, 23);
		getContentPane().add(lblExcepcionUsuarioNombre);

		lblExcepcionUsuarioApellido = new JLabel("");
		lblExcepcionUsuarioApellido.setForeground(Color.RED);
		lblExcepcionUsuarioApellido.setBounds(663, 322, 340, 23);
		getContentPane().add(lblExcepcionUsuarioApellido);

		lblExcepcionUsuarioEmail = new JLabel("");
		lblExcepcionUsuarioEmail.setForeground(Color.RED);
		lblExcepcionUsuarioEmail.setBounds(663, 348, 340, 23);
		getContentPane().add(lblExcepcionUsuarioEmail);

		lblExcepcionUsuarioContrasenia = new JLabel("");
		lblExcepcionUsuarioContrasenia.setForeground(Color.RED);
		lblExcepcionUsuarioContrasenia.setBounds(663, 374, 340, 23);
		getContentPane().add(lblExcepcionUsuarioContrasenia);

		lblExcepcionUsuarioFecNac = new JLabel("");
		lblExcepcionUsuarioFecNac.setForeground(Color.RED);
		lblExcepcionUsuarioFecNac.setBounds(663, 408, 340, 23);
		getContentPane().add(lblExcepcionUsuarioFecNac);

		Image previewFondo = Toolkit.getDefaultToolkit().getImage(ruta + "\\images\\Fondo Azul.jpg");
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 1029, 621);
		getContentPane().add(lblFondo);
		lblFondo.setBorder(new MatteBorder(0, 2, 2, 2, (Color) Color.WHITE));
		lblFondo.setIcon(new ImageIcon(previewFondo.getScaledInstance(1029, 621, Image.SCALE_DEFAULT)));
	}

	public JTable getTablaUsuario() {
		return tablaUsuario;
	}

	public void setTablaUsuario(JTable tablaUsuario) {
		this.tablaUsuario = tablaUsuario;
	}

	public JTextField getTxtIDUsuario() {
		return txtIDUsuario;
	}

	public void setTxtIDUsuario(JTextField txtIDUsuario) {
		this.txtIDUsuario = txtIDUsuario;
	}

	public JTextField getTxtNomUsuario() {
		return txtNomUsuario;
	}

	public void setTxtNomUsuario(JTextField txtNomUsuario) {
		this.txtNomUsuario = txtNomUsuario;
	}

	public JTextField getTxtApeUsuario() {
		return txtApeUsuario;
	}

	public void setTxtApeUsuario(JTextField txtApeUsuario) {
		this.txtApeUsuario = txtApeUsuario;
	}

	public JTextField getTxtEmailUsuario() {
		return txtEmailUsuario;
	}

	public void setTxtEmailUsuario(JTextField txtEmailUsuario) {
		this.txtEmailUsuario = txtEmailUsuario;
	}

	public JPasswordField getPassfieldContra() {
		return passfieldContra;
	}

	public void setPassfieldContra(JPasswordField passfieldContra) {
		this.passfieldContra = passfieldContra;
	}

	public JTextField getTxtFecNacUsuario() {
		return txtFecNacUsuario;
	}

	public void setTxtFecNacUsuario(JTextField txtFecNacUsuario) {
		this.txtFecNacUsuario = txtFecNacUsuario;
	}

	public JButton getBtnModificarUsuario() {
		return btnModificarUsuario;
	}

	public void setBtnModificarUsuario(JButton btnModificarUsuario) {
		this.btnModificarUsuario = btnModificarUsuario;
	}

	public JButton getBtnEliminarUsuario() {
		return btnEliminarUsuario;
	}

	public void setBtnEliminarUsuario(JButton btnEliminarUsuario) {
		this.btnEliminarUsuario = btnEliminarUsuario;
	}

	public JLabel getLblExcepcionUsuarioNombre() {
		return lblExcepcionUsuarioNombre;
	}

	public void setLblExcepcionUsuarioNombre(JLabel lblExcepcionUsuarioNombre) {
		this.lblExcepcionUsuarioNombre = lblExcepcionUsuarioNombre;
	}

	public JLabel getLblExcepcionUsuarioApellido() {
		return lblExcepcionUsuarioApellido;
	}

	public void setLblExcepcionUsuarioApellido(JLabel lblExcepcionUsuarioApellido) {
		this.lblExcepcionUsuarioApellido = lblExcepcionUsuarioApellido;
	}

	public JLabel getLblExcepcionUsuarioEmail() {
		return lblExcepcionUsuarioEmail;
	}

	public void setLblExcepcionUsuarioEmail(JLabel lblExcepcionUsuarioEmail) {
		this.lblExcepcionUsuarioEmail = lblExcepcionUsuarioEmail;
	}

	public JLabel getLblExcepcionUsuarioContrasenia() {
		return lblExcepcionUsuarioContrasenia;
	}

	public void setLblExcepcionUsuarioContrasenia(JLabel lblExcepcionUsuarioContrasenia) {
		this.lblExcepcionUsuarioContrasenia = lblExcepcionUsuarioContrasenia;
	}

	public JLabel getLblExcepcionUsuarioFecNac() {
		return lblExcepcionUsuarioFecNac;
	}

	public void setLblExcepcionUsuarioFecNac(JLabel lblExcepcionUsuarioFecNac) {
		this.lblExcepcionUsuarioFecNac = lblExcepcionUsuarioFecNac;
	}

	public ControladorVistaUsuario getCvu() {
		return cvu;
	}

	public void setCvu(ControladorVistaUsuario cvu) {
		this.cvu = cvu;
	}

}
